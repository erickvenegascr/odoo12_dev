# -*- coding: utf-8 -*-

import json
import requests
import logging
import re
from odoo import SUPERUSER_ID
from odoo import models, tools, fields, api, _
from odoo.exceptions import UserError
from odoo.tools.safe_eval import safe_eval
from datetime import datetime
import pytz
import base64
import xml.etree.ElementTree as ET
from dateutil.parser import parse

from odoo.addons.cr_electronic_invoice.models import api_factura_e

_logger = logging.getLogger(__name__)


class IdentificationType(models.Model):
    _name = "identification.type"

    code = fields.Char(string="Código", required=False, )
    name = fields.Char(string="Nombre", required=False, )
    notes = fields.Text(string="Notas", required=False, )

    # def name_get(self):  #    result = []  #    for record in self:  #        #name = '[%s%]' % (
    # record.percentage_exoneration)  #        result.append((record.id, record.percentage_exoneration))  #    return
    # result


class CompanyElectronic(models.Model):
    _name = 'res.company'
    # _inherit = ['res.company', 'mail.thread', 'ir.needaction_mixin']
    _inherit = ['res.company', 'mail.thread']

    commercial_name = fields.Char(string="Nombre comercial", required=False, )
    phone_code = fields.Char(string="Código de teléfono", required=False, size=3, default="506")
    fax_code = fields.Char(string="Código de Fax", required=False, )
    signature = fields.Binary(string="Llave Criptográfica", )
    identification_id = fields.Many2one(comodel_name="identification.type", string="Tipo de identificacion",
                                        required=False, )
    district_id = fields.Many2one(comodel_name="res.country.district", string="Distrito", required=False, )
    county_id = fields.Many2one(comodel_name="res.country.county", string="Cantón", required=False, )
    neighborhood_id = fields.Many2one(comodel_name="res.country.neighborhood", string="Barrios", required=False, )
    frm_ws_identificador = fields.Char(string="Usuario de Factura Electrónica", required=False, )
    frm_ws_password = fields.Char(string="Password de Factura Electrónica", required=False, )
    security_code = fields.Char(string="Código de seguridad para Factura Electrónica", size=8, required=False, )
    frm_ws_ambiente = fields.Selection(
        selection=[('disabled', 'Deshabilitado'), ('stag', 'Pruebas'), ('prod', 'Producción'), ], string="Ambiente",
        required=True, default='disabled',
        help='Es el ambiente en al cual se le está actualizando el certificado. Para el ambiente de calidad (stag) '
             'c3RhZw==, '
             'para el ambiente de producción (prod) '
             'cHJvZA==. Requerido.')
    frm_pin = fields.Char(string="Pin", required=False, help='Es el pin correspondiente al certificado. Requerido')
    frm_callback_url = fields.Char(string="Callback Url", required=False, default="https://url_callback/repuesta.php?",
                                   help='Es la URL en a la cual se reenviarán las respuestas de Hacienda.')
    url_api = fields.Char(string="URL API FE", required=False,
                          help='Direccion API firma. http://localhost:8080/decr/api')
    from_email_fe = fields.Char(string="Correo envio FE", required=False, help='FE <fe@correo.com>')
    codigo_actividad = fields.Char(string="Codigo actividad", required=True, help='Codigo de actividad en hacienda')


    @api.onchange('email')
    def _onchange_email(self):
        pass


class PartnerElectronic(models.Model):
    _inherit = "res.partner"

    commercial_name = fields.Char(string="Nombre comercial", required=False, )
    phone_code = fields.Char(string="Código de teléfono", required=False, default="506")
    fax_code = fields.Char(string="Código de Fax", required=False, )
    state_id = fields.Many2one(comodel_name="res.country.state", string="Provincia", required=False, )
    district_id = fields.Many2one(comodel_name="res.country.district", string="Distrito", required=False, )
    county_id = fields.Many2one(comodel_name="res.country.county", string="Cantón", required=False, )
    neighborhood_id = fields.Many2one(comodel_name="res.country.neighborhood", string="Barrios", required=False, )
    identification_id = fields.Many2one(comodel_name="identification.type", string="Tipo de identificacion",
                                        required=False, )
    payment_methods_id = fields.Many2one(comodel_name="payment.methods", string="Métodos de Pago", required=False, )

    _sql_constraints = [('vat_uniq', 'unique (vat)', "La cédula debe ser única"), ]

    @api.onchange('phone')
    def _onchange_phone(self):
        numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        if self.phone:
            for p in str(self.phone):
                if p not in numbers:
                    alert = {'title': 'Atención',
                        'message': 'Favor no introducir letras, espacios ni guiones en los números telefónicos.'}
                    return {'value': {'phone': ''}, 'warning': alert}

    @api.onchange('mobile')
    def _onchange_mobile(self):
        numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        if self.mobile:
            for p in str(self.mobile):
                if p not in numbers:
                    alert = {'title': 'Atención',
                        'message': 'Favor no introducir letras, espacios ni guiones en los números telefónicos.'}
                    return {'value': {'mobile': ''}, 'warning': alert}

    @api.onchange('fax')
    def _onchange_mobile(self):
        numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        if self.fax:
            for p in str(self.fax):
                if p not in numbers:
                    alert = {'title': 'Atención',
                        'message': 'Favor no introducir letras, espacios ni guiones en el fax.'}
                    return {'value': {'fax': ''}, 'warning': alert}

    @api.onchange('email')
    def _onchange_email(self):
        if self.email:
            if not re.match('^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,15}$', self.email.lower()):
                vals = {'email': False}
                alerta = {'title': 'Atención',
                    'message': 'El correo electrónico no cumple con una estructura válida. ' + str(self.email)}
                return {'value': vals, 'warning': alerta}

    """
        	@api.onchange('vat')
        def _onchange_vat(self):
            if self.identification_id and self.identification_id.code == '01':
                if len(self.vat) != 9:
                    raise UserError('La identificación tipo Cédula física debe de contener 9 dígitos, sin cero al 
                    inicio y sin guiones.')
            if self.identification_id and self.identification_id.code == '02':
                if len(self.vat) != 10:
                    raise UserError('La identificación tipo Cédula jurídica debe contener 10 dígitos, sin cero al 
                    inicio y sin guiones.')
            if self.identification_id and self.identification_id.code == '03':
                if len(self.vat) < 11 or len(self.vat) > 12:
                    raise UserError('La identificación tipo DIMEX debe contener 11 o 12 dígitos, sin ceros al inicio 
                    y sin guiones.')
            if self.identification_id and self.identification_id.code == '04':
                if len(self.vat) != 9:
                    raise UserError('La identificación tipo NITE debe contener 10 dígitos, sin ceros al inicio y sin 
                    guiones.')
            if self.identification_id and self.identification_id.code == '05':
                if len(self.vat) < 10 or len(self.vat) > 20:
                    raise UserError('La identificación tipo Extrangero debe ser de 10 dígitos.')
    """


class CodeTypeProduct(models.Model):
    _name = "code.type.product"

    code = fields.Char(string="Código", required=False, )
    name = fields.Char(string="Nombre", required=False, )


class ProductElectronic(models.Model):
    _inherit = "product.template"

    @api.model
    def _default_code_type_id(self):
        code_type_id = self.env['code.type.product'].search([('code', '=', '04')], limit=1)
        return code_type_id or False

    commercial_measurement = fields.Char(string="Unidad de Medida Comercial", required=True, )
    code_type_id = fields.Many2one(comodel_name="code.type.product", string="Tipo de código", required=False,
                                   default=_default_code_type_id)
    cabys_code = fields.Char(string="Código CAByS", help='Código CAByS Ministerio de Hacienda')

class InvoiceTaxElectronic(models.Model):
    _inherit = "account.tax"

    tax_code = fields.Char(string="Código de impuesto", required=False, )
    codigo_tarifa = fields.Char(string="Código de Tarifa", required=False, )


class PaymentMethods(models.Model):
    _name = "payment.methods"

    active = fields.Boolean(string="Activo", required=False, default=True)
    sequence = fields.Char(string="Secuencia", required=False, )
    name = fields.Char(string="Nombre", required=False, )
    notes = fields.Text(string="Notas", required=False, )


class SaleConditions(models.Model):
    _name = "sale.conditions"

    active = fields.Boolean(string="Activo", required=False, default=True)
    sequence = fields.Char(string="Secuencia", required=False, )
    name = fields.Char(string="Nombre", required=False, )
    notes = fields.Text(string="Notas", required=False, )


class AccountPaymentTerm(models.Model):
    _inherit = "account.payment.term"
    sale_conditions_id = fields.Many2one(comodel_name="sale.conditions", string="Condiciones de venta")


class ReferenceDocument(models.Model):
    _name = "reference.document"

    active = fields.Boolean(string="Activo", required=False, default=True)
    code = fields.Char(string="Código", required=False, )
    name = fields.Char(string="Nombre", required=False, )


class ReferenceCode(models.Model):
    _name = "reference.code"

    active = fields.Boolean(string="Activo", required=False, default=True)
    code = fields.Char(string="Código", required=False, )
    name = fields.Char(string="Nombre", required=False, )


class Resolution(models.Model):
    _name = "resolution"

    active = fields.Boolean(string="Activo", required=False, default=True)
    name = fields.Char(string="Nombre", required=False, )
    date_resolution = fields.Date(string="Fecha de resolución", required=False, )


class ProductUom(models.Model):
    _inherit = "uom.uom"
    code = fields.Char(string="Código", required=False, )


class AccountJournal(models.Model):
    _inherit = "account.journal"
    nd = fields.Boolean(string="Nota de Débito", required=False, )


class AccountInvoiceRefund(models.TransientModel):
    _inherit = "account.invoice.refund"

    @api.model
    def _get_invoice_id(self):
        context = dict(self._context or {})
        active_id = context.get('active_id', False)
        if active_id:
            return active_id
        return ''

    reference_code_id = fields.Many2one(comodel_name="reference.code", string="Código de referencia", required=True, )
    invoice_id = fields.Many2one(comodel_name="account.invoice", string="Documento de referencia",
                                 default=_get_invoice_id, required=False, )

    @api.multi
    def compute_refund(self, mode='refund'):
        if self.env.user.company_id.frm_ws_ambiente == 'disabled':
            result = super(AccountInvoiceRefund, self).compute_refund()
            return result
        else:
            inv_obj = self.env['account.invoice']
            inv_tax_obj = self.env['account.invoice.tax']
            inv_line_obj = self.env['account.invoice.line']
            context = dict(self._context or {})
            xml_id = False

            for form in self:
                created_inv = []
                date = False
                description = False
                for inv in inv_obj.browse(context.get('active_ids')):
                    if inv.state in ['draft', 'proforma2', 'cancel']:
                        raise UserError(_('Cannot refund draft/proforma/cancelled invoice.'))
                    if inv.reconciled and mode in ('cancel', 'modify'):
                        raise UserError(_(
                            'Cannot refund invoice which is already reconciled, invoice should be unreconciled first. '
                            'You can only refund this invoice.'))

                    date = form.date or False
                    description = form.description or inv.name
                    refund = inv.refund(form.date_invoice, date, description, inv.journal_id.id, form.invoice_id.id,
                                        form.reference_code_id.id)

                    created_inv.append(refund.id)
                    if mode in ('cancel', 'modify'):
                        movelines = inv.move_id.line_ids
                        to_reconcile_ids = {}
                        to_reconcile_lines = self.env['account.move.line']
                        for line in movelines:
                            if line.account_id.id == inv.account_id.id:
                                to_reconcile_lines += line
                                to_reconcile_ids.setdefault(line.account_id.id, []).append(line.id)
                            if line.reconciled:
                                line.remove_move_reconcile()
                        refund.action_invoice_open()
                        for tmpline in refund.move_id.line_ids:
                            if tmpline.account_id.id == inv.account_id.id:
                                to_reconcile_lines += tmpline
                        to_reconcile_lines.filtered(lambda l: l.reconciled == False).reconcile()
                        if mode == 'modify':
                            invoice = inv.read(inv_obj._get_refund_modify_read_fields())
                            invoice = invoice[0]
                            del invoice['id']
                            invoice_lines = inv_line_obj.browse(invoice['invoice_line_ids'])
                            invoice_lines = inv_obj.with_context(mode='modify')._refund_cleanup_lines(invoice_lines)
                            tax_lines = inv_tax_obj.browse(invoice['tax_line_ids'])
                            tax_lines = inv_obj._refund_cleanup_lines(tax_lines)
                            invoice.update(
                                {'type': inv.type, 'date_invoice': form.date_invoice, 'state': 'draft', 'number': False,
                                    'invoice_line_ids': invoice_lines, 'tax_line_ids': tax_lines, 'date': date,
                                    'origin': inv.origin, 'fiscal_position_id': inv.fiscal_position_id.id,
                                    'invoice_id': inv.id,  # agregado
                                    'reference_code_id': form.reference_code_id.id,  # agregado
                                })
                            for field in inv_obj._get_refund_common_fields():
                                if inv_obj._fields[field].type == 'many2one':
                                    invoice[field] = invoice[field] and invoice[field][0]
                                else:
                                    invoice[field] = invoice[field] or False
                            inv_refund = inv_obj.create(invoice)
                            if inv_refund.payment_term_id.id:
                                inv_refund._onchange_payment_term_date_invoice()
                            created_inv.append(inv_refund.id)
                    xml_id = (inv.type in ['out_refund', 'out_invoice']) and 'action_invoice_tree1' or (
                                inv.type in ['in_refund', 'in_invoice']) and 'action_invoice_tree2'
                    # Put the reason in the chatter
                    subject = _("Invoice refund")
                    body = description
                    refund.message_post(body=body, subject=subject)
            if type in ('out_refund','out_invoice'):
                if xml_id:
                    result = self.env.ref('account.%s' % (xml_id)).read()[0]
                    invoice_domain = safe_eval(result['domain'])
                    invoice_domain.append(('id', 'in', created_inv))
                    result['domain'] = invoice_domain
                    return result
            return True


class InvoiceLineElectronic(models.Model):
    _inherit = "account.invoice.line"

    total_amount = fields.Float(string="Monto total", required=False, )
    total_discount = fields.Float(string="Total descuento", required=False, )
    discount_note = fields.Char(string="Nota de descuento", required=False, )
    total_tax = fields.Float(string="Total impuesto", required=False, )
    impuesto_exonerado = fields.Float(string="Monto Impuesto Exonerado", required=False, )
    exoneration_id = fields.Many2one(comodel_name='account.exoneration', string='Exoneracion')

    # exoneration_total = fields.Float(string="Exoneración total", required=False, )
    # total_line_exoneration = fields.Float(string="Exoneración total de la línea", required=False, )
    # exoneration_id = fields.Many2one(comodel_name="exoneration", string="Exoneración", required=False, )

    @api.onchange('discount')
    def _onchange_email(self):
        if self.discount > 0.0:
            self.discount_note = 'Condición de mercado'


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    email = fields.Char(string="Correo", required=False, copy=False, compute="_email")

    @api.onchange('partner_id')
    def _email(self):
        for c in self:
            if (not c.partner_id == False):
                c.email = c.partner_id.email
            else:
                c.email = ''


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    @api.multi
    def _prepare_invoice_line(self, qty):
        """
        Prepare the dict of values to create the new invoice line for a sales order line.

        :param qty: float quantity to invoice
        """
        self.ensure_one()
        res = {}
        account = self.product_id.property_account_income_id or \
                  self.product_id.categ_id.property_account_income_categ_id
        if not account:
            raise UserError(
                _('Please define income account for this product: "%s" (id:%d) - or for its category: "%s".') % (
                self.product_id.name, self.product_id.id, self.product_id.categ_id.name))

        fpos = self.order_id.fiscal_position_id or self.order_id.partner_id.property_account_position_id
        if fpos:
            account = fpos.map_account(account)
        nota_descuento = ''
        if self.discount > 0.0:
            nota_descuento = 'Condición de mercado'
        res = {'name': self.name, 'sequence': self.sequence, 'origin': self.order_id.name, 'account_id': account.id,
            'price_unit': self.price_unit, 'quantity': qty, 'discount': self.discount, 'uom_id': self.product_uom.id,
            'product_id': self.product_id.id or False,
            # 'layout_category_id': self.layout_category_id and self.layout_category_id.id or False,
            'invoice_line_tax_ids': [(6, 0, self.tax_id.ids)],  # 'account_analytic_id': self.order_id.project_id.id,
            'analytic_tag_ids': [(6, 0, self.analytic_tag_ids.ids)], 'discount_note': nota_descuento, }
        return res


class AccountInvoiceElectronic(models.Model):
    _inherit = "account.invoice"

    number_electronic = fields.Char(string="Número electrónico", required=False, copy=False, index=True)
    date_issuance = fields.Char(string="Fecha de emisión", required=False, copy=False)
    state_send_invoice = fields.Selection([('aceptado', 'Aceptado'), ('rechazado', 'Rechazado'), ],
                                          'Estado FE Proveedor')
    state_tributacion = fields.Selection(
        [('aceptado', 'Aceptado'), ('rechazado', 'Rechazado'), ('no encontrado', 'No encontrado')], 'Estado FE',
        copy=False)
    state_invoice_partner = fields.Selection([('1', 'Aceptado'), ('3', 'Rechazado'), ('2', 'Aceptacion parcial')],
                                             'Respuesta del Cliente')
    reference_code_id = fields.Many2one(comodel_name="reference.code", string="Código de referencia", required=False, )
    payment_methods_id = fields.Many2one(comodel_name="payment.methods", string="Métodos de Pago", required=False, )
    invoice_id = fields.Many2one(comodel_name="account.invoice", string="Documento de referencia", required=False,
                                 copy=False)
    xml_respuesta_tributacion = fields.Binary(string="Respuesta Tributación XML", required=False, copy=False,
                                              attachment=True)
    fname_xml_respuesta_tributacion = fields.Char(string="Nombre de archivo XML Respuesta Tributación", required=False,
                                                  copy=False)
    xml_comprobante = fields.Binary(string="Comprobante XML", required=False, copy=False, attachment=True)
    fname_xml_comprobante = fields.Char(string="Nombre de archivo Comprobante XML", required=False, copy=False,
                                        attachment=True)
    xml_supplier_approval = fields.Binary(string="XML Proveedor", required=False, copy=False, attachment=True)
    fname_xml_supplier_approval = fields.Char(string="Nombre de archivo Comprobante XML proveedor", required=False,
                                              copy=False, attachment=True)
    amount_tax_electronic_invoice = fields.Monetary(string='Total de impuestos FE', readonly=True, )
    amount_total_electronic_invoice = fields.Monetary(string='Total FE', readonly=True, )
    documento_enviado = fields.Boolean('Enviado cliente', default=False)
    email = fields.Char(string="Correo", required=False, copy=False, compute="_email")
    fec = fields.Boolean('Factura Electrónica de Compra ', default=False)

    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'tax_line_ids.amount_rounding',
                 'currency_id', 'company_id', 'date_invoice', 'type', 'invoice_line_ids.exoneration_id')
    def _compute_amount(self):
        round_curr = self.currency_id.round
        self.amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids)
        if self.type in ('out_invoice'):
            impuesto = 0.0
            impuesto += sum(round_curr(line.amount_total) for line in self.tax_line_ids)
            for i in self.invoice_line_ids:
                subtotal_linea = round(i.price_unit * i.quantity, 4)
                subtotal_linea_descontado = (subtotal_linea - round((subtotal_linea * (i.discount / 100)), 4))
                imp = subtotal_linea_descontado * (
                            sum(round_curr(line.amount) for line in i.invoice_line_tax_ids) / 100)
                i.impuesto_exonerado = round(imp * i.exoneration_id.percentage_exoneration / 100, 2)
            impuesto -= sum(round_curr(line.impuesto_exonerado) for line in self.invoice_line_ids)
            self.amount_tax = impuesto
        else:
            self.amount_tax = sum(round_curr(line.amount_total) for line in self.tax_line_ids)
        self.amount_total = self.amount_untaxed + self.amount_tax
        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id
            amount_total_company_signed = currency_id._convert(self.amount_total, self.company_id.currency_id,
                                                               self.company_id,
                                                               self.date_invoice or fields.Date.today())
            amount_untaxed_signed = currency_id._convert(self.amount_untaxed, self.company_id.currency_id,
                                                         self.company_id, self.date_invoice or fields.Date.today())
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = self.amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign

    # mensaje_recepor = fields.Char(string="Mensaje receptor", required=False, copy=False, index=True)
    _sql_constraints = [
        ('number_electronic_uniq', 'unique (number_electronic)', "La clave de comprobante debe ser única"), ]

    @api.multi
    def load_xml_data(self):
        account = False
        analytic_account = False
        product = False
        load_lines = bool(self.env['ir.config_parameter'].sudo().get_param('load_lines'))

        default_account_id = self.env['ir.config_parameter'].sudo().get_param('expense_account_id')
        if default_account_id:
            account = self.env['account.account'].search([('id', '=', default_account_id)], limit=1)

        analytic_account_id = self.env['ir.config_parameter'].sudo().get_param('expense_analytic_account_id')
        if analytic_account_id:
            analytic_account = self.env['account.analytic.account'].search([('id', '=', analytic_account_id)], limit=1)

        product_id = self.env['ir.config_parameter'].sudo().get_param('expense_product_id')
        default_iva_ventas = self.env['ir.config_parameter'].sudo().get_param('default_iva_ventas')
        if product_id:
            product = self.env['product.product'].search([('id', '=', product_id)], limit=1)

        api_factura_e.load_xml_data(self, load_lines, account, product, analytic_account,default_iva_ventas)

    @api.model
    def tax_line_move_line_get(self):
        res = []
        # keep track of taxes already processed
        done_taxes = []
        tiene_exoneracion = False
        for i in self.invoice_line_ids:
            if i.exoneration_id:
                tiene_exoneracion = True
                break
        if tiene_exoneracion == False:
            # loop the invoice.tax.line in reversal sequence
            for tax_line in sorted(self.tax_line_ids, key=lambda x: -x.sequence):
                if tax_line.amount_total:
                    tax = tax_line.tax_id
                    if tax.amount_type == "group":
                        for child_tax in tax.children_tax_ids:
                            done_taxes.append(child_tax.id)
                    analytic_tag_ids = [(4, analytic_tag.id, None) for analytic_tag in tax_line.analytic_tag_ids]
                    res.append({'invoice_tax_line_id': tax_line.id, 'tax_line_id': tax_line.tax_id.id, 'type': 'tax',
                                'name': tax_line.name, 'price_unit': tax_line.amount_total, 'quantity': 1,
                                'price': tax_line.amount_total, 'account_id': tax_line.account_id.id,
                                'account_analytic_id': tax_line.account_analytic_id.id,
                                'analytic_tag_ids': analytic_tag_ids, 'invoice_id': self.id,
                                'tax_ids': [(6, 0, list(done_taxes))] if tax_line.tax_id.include_base_amount else []})
                    done_taxes.append(tax.id)
        return res

    @api.onchange('partner_id')
    def _email(self):
        for documento in self:
            if (not documento.partner_id == False):
                documento.email = documento.partner_id.email

    #@api.onchange('xml_supplier_approval')
    def _onchange_xml_supplier_approval(self):
        if self.xml_supplier_approval:
            root = ET.fromstring(re.sub(' xmlns="[^"]+"', '', base64.b64decode(self.xml_supplier_approval).decode("utf-8"),
                                        count=1))  # quita el namespace de los elementos
            if not root.findall('Clave'):
                return {'value': {'xml_supplier_approval': False}, 'warning': {'title': 'Atención',
                                                                               'message': 'El archivo xml no contiene '
                                                                                          'el nodo Clave. Por favor '
                                                                                          'cargue un archivo con el '
                                                                                          'formato correcto.'}}
            if not root.findall('FechaEmision'):
                return {'value': {'xml_supplier_approval': False}, 'warning': {'title': 'Atención',
                                                                               'message': 'El archivo xml no contiene '
                                                                                          'el nodo FechaEmision. Por '
                                                                                          'favor cargue un archivo '
                                                                                          'con el formato correcto.'}}
            if not root.findall('Emisor'):
                return {'value': {'xml_supplier_approval': False}, 'warning': {'title': 'Atención',
                                                                               'message': 'El archivo xml no contiene '
                                                                                          'el nodo Emisor. Por favor '
                                                                                          'cargue un archivo con el '
                                                                                          'formato correcto.'}}
            if not root.findall('Emisor')[0].findall('Identificacion'):
                return {'value': {'xml_supplier_approval': False}, 'warning': {'title': 'Atención',
                                                                               'message': 'El archivo xml no contiene '
                                                                                          'el nodo Identificacion. '
                                                                                          'Por favor cargue un '
                                                                                          'archivo con el formato '
                                                                                          'correcto.'}}
            if not root.findall('Emisor')[0].findall('Identificacion')[0].findall('Tipo'):
                return {'value': {'xml_supplier_approval': False}, 'warning': {'title': 'Atención',
                                                                               'message': 'El archivo xml no contiene '
                                                                                          'el nodo Tipo. Por favor '
                                                                                          'cargue un archivo con el '
                                                                                          'formato correcto.'}}
            if not root.findall('Emisor')[0].findall('Identificacion')[0].findall('Numero'):
                return {'value': {'xml_supplier_approval': False}, 'warning': {'title': 'Atención',
                                                                               'message': 'El archivo xml no contiene '
                                                                                          'el nodo Numero. Por favor '
                                                                                          'cargue un archivo con el '
                                                                                          'formato correcto.'}}
            # if not (root.findall('ResumenFactura') and root.findall('ResumenFactura')[0].findall('TotalImpuesto')):
            #	return {'value': {'xml_supplier_approval': False},'warning': {'title': 'Atención','message': 'No se
            #	puede localizar el nodo TotalImpuesto. Por favor cargue un archivo con el formato correcto.'}}
            if not (root.findall('ResumenFactura') and root.findall('ResumenFactura')[0].findall('TotalComprobante')):
                return {'value': {'xml_supplier_approval': False}, 'warning': {'title': 'Atención',
                                                                               'message': 'No se puede localizar el '
                                                                                          'nodo TotalComprobante. Por '
                                                                                          'favor cargue un archivo '
                                                                                          'con el formato correcto.'}}

    # self.fname_xml_supplier_approval = 'comrpobante_proveedor.xml'

    @api.multi
    def charge_xml_data(self):
        if self.xml_supplier_approval:
            root = ET.fromstring(re.sub(' xmlns="[^"]+"', '',self.xml_supplier_approval).decode("utf-8"),count=1)
            self.number_electronic = root.findall('Clave')[0].text
            self.date_issuance = root.findall('FechaEmision')[0].text
            partner = self.env['res.partner'].search(
                [('vat', '=', root.findall('Emisor')[0].find('Identificacion')[1].text)])
            if partner:
                self.partner_id = partner.id
            else:
                raise UserError('El proveedor con identificación ' + root.findall('Emisor')[0].find('Identificacion')[
                    1].text + ' no existe. Por favor creelo primero en el sistema.')
            if (root.findall('ResumenFactura') and root.findall('ResumenFactura')[0].findall('TotalImpuesto')):
                self.amount_tax_electronic_invoice = root.findall('ResumenFactura')[0].findall('TotalImpuesto')[0].text
            self.amount_total_electronic_invoice = root.findall('ResumenFactura')[0].findall('TotalComprobante')[0].text
            self.reference = root.findall('NumeroConsecutivo')[0].text

    @api.multi
    def send_xml(self):
        for inv in self:
            if inv.fec:
                if inv.company_id.frm_ws_ambiente != 'disabled':
                    print
                    inv.number
                    TipoDocumento = inv.number[8:10]
                    # TipoDocumento = ''
                    FacturaReferencia = ''
                    date_prueba = datetime.strptime(inv.write_date, tools.DEFAULT_SERVER_DATETIME_FORMAT)
                    #user_tz = self.env.user.tz or pytz.utc
                    user_tz = self.env.user.tz or 'America/Costa_Rica'
                    local = pytz.timezone(user_tz)
                    date_cr = datetime.strftime(pytz.utc.localize(
                        datetime.strptime(inv.write_date, tools.DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(local),
                                                "%Y-%m-%dT%H:%M:%S-06:00")
                    fecha = pytz.utc.localize(
                        datetime.strptime(inv.write_date, tools.DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(local)
                    print
                    fecha
                    tipo_documento_referencia = ''
                    numero_documento_referencia = ''
                    fecha_emision_referencia = ''
                    codigo_referencia = ''
                    razon_referencia = ''
                    medio_pago = inv.payment_methods_id.sequence or '01'
                    if inv.type == 'out_invoice':  # FC Y ND
                        if inv.invoice_id and inv.journal_id and inv.journal_id.nd:
                            TipoDocumento = '02'
                            tipo_documento_referencia = inv.invoice_id.number_electronic[
                                                        29:31]  # 50625011800011436041700100001 01 0000000154112345678
                            numero_documento_referencia = inv.invoice_id.number_electronic
                            fecha_emision_referencia = inv.invoice_id.date_issuance
                            codigo_referencia = inv.reference_code_id.code
                            razon_referencia = inv.reference_code_id.name
                            medio_pago = ''
                        else:
                            TipoDocumento = '01'
                    if inv.type == 'out_refund':  # NC
                        if inv.invoice_id.journal_id.nd:
                            tipo_documento_referencia = '02'
                        else:
                            tipo_documento_referencia = '01'
                        TipoDocumento = '03'
                        if inv.origin and inv.origin.isdigit():
                            documento_origen = inv.env['account.invoice'].search([['number', '=', inv.origin]])
                            if documento_origen == False:
                                numero_documento_referencia = inv.origin
                                fecha_emision_referencia = fecha
                                codigo_referencia = inv.reference_code_id.code
                            else:
                                FacturaReferencia = documento_origen.number_electronic
                                fecha_emision_referencia = documento_origen.date_issuance
                                numero_documento_referencia = documento_origen.number_electronic

                                # FacturaReferencia = (inv.origin)
                                # numero_documento_referencia = inv.invoice_id.number_electronic
                                # fecha_emision_referencia = inv.invoice_id.date_issuance
                                codigo_referencia = inv.reference_code_id.code
                            razon_referencia = inv.reference_code_id.name
                        else:
                            numero_documento_referencia = inv.origin
                            fecha_emision_referencia = date_cr
                            codigo_referencia = inv.reference_code_id.code
                            tipo_documento_referencia = '99'
                    if inv.payment_term_id:
                        if inv.payment_term_id.sale_conditions_id:
                            sale_conditions = inv.payment_term_id.sale_conditions_id.sequence or '01'
                        else:
                            raise UserError(
                                'No se pudo Crear la factura electrónica: Debe configurar condiciones de pago para')
                    else:
                        sale_conditions = '01'

                    if inv.number.isdigit() and TipoDocumento:
                        currency_rate = 1 / inv.currency_id.rate
                        lines = []
                        base_total = 0.0
                        numero = 0

                        if inv.partner_id.identification_id.code == '05':
                            receptor_identificacion = {'tipo': False, 'numero': False, }
                            receptor_identificacion_extranjero = inv.partner_id.vat
                        else:
                            receptor_identificacion = {'tipo': inv.company_id.identification_id.code,
                                                       'numero': inv.company_id.vat, }
                            receptor_identificacion_extranjero = ''

                        totalserviciogravado = 0.0
                        totalservicioexento = 0.0
                        totalmercaderiagravado = 0.0
                        totalmercaderiaexento = 0.0
                        impuestoTotal = 0.0
                        total_descuentos = 0.0

                        totalservicioexonerado = 0.0
                        totalmercaderiaexonerado = 0.0
                        totalexonerado = 0.0

                        for inv_line in inv.invoice_line_ids:
                            impuestos_acumulados = 0.0
                            numero += 1
                            base_total += inv_line.price_unit * inv_line.quantity
                            subtotal_linea = round(inv_line.price_unit * inv_line.quantity, 4)
                            subtotal_linea_descontado = (
                                    subtotal_linea - round((subtotal_linea * (inv_line.discount / 100)), 4))
                            total_descuentos += round((subtotal_linea * (inv_line.discount / 100)), 4)
                            # subtotal_linea_descontado = float("%.4f" % (subtotal_linea - round((subtotal_linea * (
                            # inv_line.discount / 100)), 4)))
                            impuestos = []
                            exonerado = False
                            for i in inv_line.invoice_line_tax_ids:
                                if i.tax_code not in ('00'):
                                    impuesto_linea = round((i.amount / 100) * subtotal_linea_descontado, 4)
                                    if inv_line.exoneration_id:
                                        exonerado = True
                                        impuestos.append(
                                            {'codigo': i.tax_code, 'codigotarifa': i.codigo_tarifa, 'tarifa': i.amount,
                                                'monto': round((i.amount / 100) * subtotal_linea_descontado, 4),
                                                'exoneracion': {'tipoDocumento': inv_line.exoneration_id.type,
                                                    'numeroDocumento': inv_line.exoneration_id.exoneration_number,
                                                    'nombreInstitucion': inv_line.exoneration_id.name_institution,
                                                    'fechaEmision': str(
                                                        inv_line.exoneration_id.date) + 'T00:00:00-06:00',
                                                    'montoImpuesto': round(
                                                        tax_amount * inv_line.exoneration_id.percentage_exoneration /
                                                        100,
                                                        2), 'porcentajeCompra': int(
                                                        inv_line.exoneration_id.percentage_exoneration), }})
                                        impuestoneto = round((i.amount / 100) * subtotal_linea_descontado, 4) - round(
                                            impuesto_linea * inv_line.exoneration_id.percentage_exoneration / 100, 4)
                                        impuestoTotal += round((i.amount / 100) * subtotal_linea_descontado, 4) - round(
                                            impuesto_linea * inv_line.exoneration_id.percentage_exoneration / 100, 4)
                                        impuestos_acumulados += round((i.amount / 100) * subtotal_linea_descontado,
                                                                      4) - round(
                                            impuesto_linea * inv_line.exoneration_id.percentage_exoneration / 100, 4)
                                    else:
                                        impuestos.append(
                                            {'codigo': i.tax_code, 'codigotarifa': i.codigo_tarifa, 'tarifa': i.amount,
                                             'monto': round((i.amount / 100) * subtotal_linea_descontado, 4), })
                                        impuestos_acumulados += round((i.amount / 100) * subtotal_linea_descontado, 4)
                                        impuestoTotal += round((i.amount / 100) * subtotal_linea_descontado, 4)

                            if inv_line.product_id:
                                if inv_line.product_id.type == 'service':
                                    if exonerado:
                                        totalservicioexonerado += inv_line.quantity * inv_line.price_unit
                                    else:
                                        if impuestos_acumulados:
                                            totalserviciogravado += inv_line.quantity * inv_line.price_unit
                                        else:
                                            totalservicioexento += inv_line.quantity * inv_line.price_unit
                                else:
                                    if exonerado:
                                        totalmercaderiaexonerado += inv_line.quantity * inv_line.price_unit
                                        if impuestos_acumulados:
                                            totalmercaderiagravado += inv_line.quantity * inv_line.price_unit
                                        else:
                                            totalmercaderiaexento += inv_line.quantity * inv_line.price_unit
                            else:  # se asume que si no tiene producto setrata como un type product
                                if impuestos_acumulados:
                                    totalmercaderiagravado += inv_line.quantity * inv_line.price_unit
                                else:
                                    totalmercaderiaexento += inv_line.quantity * inv_line.price_unit

                            unidad_medida = inv_line.uom_id.code
                            unidad_medida_comercial = inv_line.product_id.commercial_measurement

                            if unidad_medida_comercial == 'Sp':
                                unidad_medida = 'Sp'
                            if unidad_medida_comercial == False:
                                unidad_medida_comercial = 'Unid'
                                unidad_medida = 'Unid'
                            line = {'numero': numero, 'codigo': [{'tipo': inv_line.product_id.code_type_id.code or '04',
                                                                  'codigo': inv_line.product_id.default_code or
                                                                            '000', }],
                                    'cantidad': inv_line.quantity, 'unidad_medida': unidad_medida,
                                    'unidad_medida_comercial': unidad_medida_comercial, 'detalle': inv_line.name[:159],
                                    'precio_unitario': inv_line.price_unit,
                                    'monto_total': inv_line.quantity * inv_line.price_unit, 'descuento': [
                                    {'monto': (round((subtotal_linea * (inv_line.discount / 100)), 4)) or '0.0',
                                     'naturaleza': inv_line.discount_note, }], 'subtotal': subtotal_linea_descontado,
                                    'impuestos': impuestos,
                                    'montototallinea': subtotal_linea_descontado + impuestos_acumulados, }
                            lines.append(line)
                        _logger.error('MAB - formando payload')
                        # total_descuentos = round(base_total, 4) - round(inv.amount_untaxed, 4)
                        if total_descuentos < 0.0:
                            total_descuentos = 0.0
                        payload = {
                            'clave': {'sucursal': '001', 'terminal': '00001', 'tipo': '08', 'comprobante': inv.number,
                                      'pais': '506', 'dia': '%02d' % fecha.day, 'mes': '%02d' % fecha.month,
                                      'anno': str(fecha.year)[2:4], 'situacion_presentacion': '1',
                                      'codigo_seguridad': inv.company_id.security_code, },
                            'encabezado': {'fecha': date_cr,  # 'fecha': "2018-01-19T23:17:00+06:00",
                                           'condicion_venta': sale_conditions, 'plazo_credito': '0',
                                           'medio_pago': medio_pago,
                                           'codigo_actividad': inv.company_id.codigo_actividad, },
                            'emisor': {'nombre': inv.partner_id.name[:80],
                                       'identificacion': {'tipo': inv.company_id.identification_id.code,
                                                          'numero': inv.company_id.vat, },
                                       'nombre_comercial': inv.partner_id.name[:80] or '',
                                       'ubicacion': {'provincia': inv.partner_id.state_id.code,
                                                     'canton': inv.partner_id.county_id.code,
                                                     'distrito': inv.partner_id.district_id.code,
                                                     'barrio': inv.partner_id.neighborhood_id.code,
                                                     'sennas': inv.partner_id.street, },
                                       'telefono': {'cod_pais': 506, 'numero': inv.partner_id.phone, },
                                       'fax': {'cod_pais': 506, 'numero': inv.partner_id.fax, },
                                       'correo_electronico': inv.partner_id.email, }, 'receptor': {  ##
                                'nombre': inv.company_id.name[:80], 'identificacion': receptor_identificacion,
                                'IdentificacionExtranjero': receptor_identificacion_extranjero, }, 'detalle': lines,
                            'resumen': {'moneda': inv.currency_id.name, 'tipo_cambio': round(currency_rate, 5),
                                        'totalserviciogravado': totalserviciogravado,
                                        'totalservicioexento': totalservicioexento,
                                        'totalmercaderiagravado': totalmercaderiagravado,
                                        'totalmercaderiaexento': totalmercaderiaexento,
                                        'totalgravado': totalserviciogravado + totalmercaderiagravado,
                                        'totalexento': totalservicioexento + totalmercaderiaexento,
                                        'totalservicioexonerado': totalservicioexonerado,
                                        'totalmercaderiaexonerado': totalmercaderiaexonerado,
                                        'totalexonerado': totalmercaderiaexonerado + totalservicioexonerado,
                                        'totalventa': totalserviciogravado + totalmercaderiagravado +
                                                      totalservicioexento + totalmercaderiaexento +
                                                      totalmercaderiaexonerado + totalservicioexonerado,
                                        'totaldescuentos': total_descuentos, 'totalventaneta': (
                                                                                                       totalserviciogravado + totalmercaderiagravado + totalservicioexento + totalmercaderiaexento) - (
                                                                                                   total_descuentos) + (
                                                                                                           totalmercaderiaexonerado + totalservicioexonerado),
                                        'totalimpuestos': round(impuestoTotal, 4),
                                        'totalcomprobante': (totalmercaderiaexonerado + totalservicioexonerado) + ((
                                                                                                                           totalserviciogravado + totalmercaderiagravado + totalservicioexento + totalmercaderiaexento) - (
                                                                                                                       total_descuentos)) + round(
                                            impuestoTotal, 4), }, 'referencia': [
                                {'tipo_documento': tipo_documento_referencia,
                                 'numero_documento': numero_documento_referencia,
                                 'fecha_emision': fecha_emision_referencia, 'codigo': codigo_referencia,
                                 'razon': razon_referencia, }], 'otros': [{'codigo': 'OC', 'texto': inv.name}], }
                        headers = {'content-type': 'application/json',
                                   # 'authorization': "Bearer " + token['access_token'],
                                   }
                        if inv.company_id.frm_ws_ambiente == 'stag':
                            requests_url = self.env.user.company_id.url_api + '/makeXML.stag.43'
                        else:
                            requests_url = self.env.user.company_id.url_api + '/makeXML.stag.43'
                        xdata = json.dumps(payload)
                        response_document = requests.post(requests_url, headers=headers, data=xdata)
                        _logger.error('MAB - JSON DATA:%s', xdata)
                        _logger.error('MAB - response:%s', response_document._content)
                        response_content = json.loads(response_document._content)
                        if response_content.get('code'):
                            if response_content.get('code') == 1:
                                inv.number_electronic = response_content.get('clave')
                                inv.date_issuance = date_cr
                                self.env.cr.commit()
                            else:
                                continue
                                """
                                raise UserError(
                                    'No se pudo Crear la factura electrónica: \n' + str(response_document._content))
                                """
                        if response_document.status_code == 200:  # TODO
                            print
                            response_document
                            if response_document._content:
                                # inv.electronic_invoice_return_message = response_document._content
                                inv.fname_xml_comprobante = 'comprobante_' + inv.number + '.xml'
                                inv.xml_comprobante = response_content.get('data')
                                inv.state_tributacion = 'procesando'
                                # if not inv.partner_id.opt_out:
                                #	email_template = self.env.ref('account.email_template_edi_invoice', False)
                                #	attachment = self.env['ir.attachment'].search([('res_model','=',
                                #	'account.invoice'),('res_id','=',inv.id),('res_field','=','xml_comprobante')],
                                #	limit=1)
                                #	attachment.name = inv.fname_xml_comprobante
                                #	attachment.datas_fname = inv.fname_xml_comprobante
                                #	email_template.attachment_ids = [(6,0,[attachment.id])] # [(4, attachment.id)]
                                #	email_template.with_context(type='binary', default_type='binary').send_mail(
                                #	inv.id, raise_exception=False, force_send=True)#default_type='binary'
                                #	email_template.attachment_ids = [(3, attachment.id)]
                                if response_document._content.split(',')[1][5:] == 'false':
                                    raise UserError(
                                        'No se pudo Crear la factura electrónica: \n' + str(response_document._content))
                        elif response_document.status_code == 400:
                            if (response_content.get('hacienda_mensaje').find('anteriormente') > -1):
                                inv.fname_xml_comprobante = 'comprobante_' + inv.number + '.xml'
                                inv.xml_comprobante = response_content.get('data')
                                inv.number_electronic = response_content.get('clave')
                                inv.date_issuance = date_cr
                                self.env.cr.commit()
                        else:
                            raise UserError(
                                'No se pudo Crear la factura electrónica: \n' + str(response_document._content))
            if inv.xml_supplier_approval:
                root = ET.fromstring(re.sub(' xmlns="[^"]+"', '', base64.b64decode(inv.xml_supplier_approval).decode("utf-8"), count=1))
                if not inv.state_invoice_partner:
                    raise UserError('Aviso!.\nDebe primero seleccionar el tipo de respuesta para el archivo cargado.')
                # if round(float(root.findall('ResumenFactura')[0].findall('TotalComprobante')[0].text),0) == round(
                # inv.amount_total,0):
                if inv.company_id.frm_ws_ambiente != 'disabled' and inv.state_invoice_partner:
                    if inv.state_invoice_partner == '1':
                        detalle_mensaje = 'Aceptado'
                        tipo = '05'
                    if inv.state_invoice_partner == '2':
                        detalle_mensaje = 'Aceptado parcial'
                        tipo = '06'
                    if inv.state_invoice_partner == '3':
                        detalle_mensaje = 'Rechazado'
                        tipo = '07'
                    impuesto = 0.0
                    if (root.findall('ResumenFactura') and root.findall('ResumenFactura')[0].findall('TotalImpuesto')):
                        impuesto = root.findall('ResumenFactura')[0].findall('TotalImpuesto')[0].text

                    payload = {'clave': {'tipo': tipo, 'sucursal': '1',  # sucursal,#TODO
                        'terminal': '1',  # terminal,
                        'numero_documento': root.findall('Clave')[0].text,
                        'numero_cedula_emisor': root.findall('Emisor')[0].find('Identificacion')[1].text,
                        'fecha_emision_doc': root.findall('FechaEmision')[0].text, 'mensaje': inv.state_invoice_partner,
                        'detalle_mensaje': detalle_mensaje, 'monto_total_impuesto': impuesto,
                        'total_factura': root.findall('ResumenFactura')[0].findall('TotalComprobante')[0].text,
                        'numero_cedula_receptor': inv.company_id.vat,
                        'num_consecutivo_receptor': inv.number[:8] + '0' + str(tipo) + inv.number[8:18], 'emisor': {
                            'identificacion': {
                                'tipo': root.findall('Emisor')[0].findall('Identificacion')[0].findall('Tipo')[0].text,
                                'numero': root.findall('Emisor')[0].findall('Identificacion')[0].findall('Numero')[
                                    0].text, }, }, }, }
                    headers = {'content-type': 'application/json', # 'authorization': "Bearer " + token['access_token'],
                    }
                    if inv.company_id.frm_ws_ambiente == 'stag':
                        requests_url = self.env.user.company_id.url_api + '/accept_msg'
                    else:
                        requests_url = self.env.user.company_id.url_api + '/accept_msg'
                    response_document = requests.post(requests_url, headers=headers, data=json.dumps(payload))
                    response_content = json.loads(response_document._content)

                    # _logger.error('MAB - JSON DATA:%s', payload)
                    # _logger.error('MAB - response:%s', response_content)

                    if response_content.get('code'):
                        if response_content.get('code') == 1:  # aprovado
                            inv.number = inv.number[:8] + '0' + str(tipo) + inv.number[8:18]
                            inv.fname_xml_respuesta_tributacion = 'Respuesta_aprobacion' + inv.number + '.xml'
                            inv.xml_respuesta_tributacion = response_content.get('data')
                            inv.state_send_invoice = 'aceptado'
                        else:
                            raise UserError('Error con el comprobante: \n' + str(
                                response_document._content))  # if response_document.status_code == 'YY': #error  #	if
                            # response_document._content and response_document._content.split(',')[1][5:] == 'false':
                            # #		raise UserError('Error con el comprobante: \n' + str(response_document._content))
                            # else:  #	raise UserError('Error con el comprobante: \n'+str(
                            # response_document._content))  # else:  #	raise UserError('Error!.\nEl monto total de la
                            # factura no coincide con el monto total del archivo XML')

    @api.multi
    @api.returns('self')
    def refund(self, date_invoice=None, date=None, description=None, journal_id=None, invoice_id=None,
               reference_code_id=None):
        if self.env.user.company_id.frm_ws_ambiente == 'disabled':
            new_invoices = super(AccountInvoiceElectronic, self).refund()
            return new_invoices
        else:
            new_invoices = self.browse()
            for invoice in self:
                # create the new invoice
                values = self._prepare_refund(invoice, date_invoice=date_invoice, date=date, description=description,
                                              journal_id=journal_id)
                values.update({'invoice_id': invoice_id, 'reference_code_id': reference_code_id, 'payment_term_id': 1})
                refund_invoice = self.create(values)
                invoice_type = {'out_invoice': ('customer invoices refund'), 'in_invoice': ('vendor bill refund')}
                message = _(
                    "This %s has been created from: <a href=# data-oe-model=account.invoice data-oe-id=%d>%s</a>") % (
                              invoice_type[invoice.type], invoice.id, invoice.number)
                refund_invoice.message_post(body=message)
                new_invoices += refund_invoice
            return new_invoices

    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        super(AccountInvoiceElectronic, self)._onchange_partner_id()
        self.payment_methods_id = self.partner_id.payment_methods_id

    def _procesa_documentos(self, documentos):
        for inv in documentos:
            if inv.fname_xml_comprobante == False and inv.type in ('out_invoice', 'out_refund'):
                if inv.company_id.frm_ws_ambiente != 'disabled':
                    print(inv.number)
                    TipoDocumento = inv.number[8:10]
                    # TipoDocumento = ''
                    FacturaReferencia = ''
                    user_tz = self.env.user.tz or pytz.utc
                    local = pytz.timezone(user_tz)
                    date_cr = datetime.strftime(pytz.utc.localize(
                        datetime.strptime(str(inv.write_date)[:-7], tools.DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(
                        local), "%Y-%m-%dT%H:%M:%S-06:00")
                    fecha = pytz.utc.localize(
                        datetime.strptime(str(inv.write_date)[:-7], tools.DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(
                        local)
                    tipo_documento_referencia = ''
                    numero_documento_referencia = ''
                    fecha_emision_referencia = ''
                    codigo_referencia = ''
                    razon_referencia = ''
                    medio_pago = inv.payment_methods_id.sequence or '01'
                    if inv.type == 'out_invoice':  # FC Y ND
                        if inv.invoice_id and inv.journal_id and inv.journal_id.nd:
                            TipoDocumento = '02'
                            tipo_documento_referencia = inv.invoice_id.number_electronic[
                                                        29:31]  # 50625011800011436041700100001 01 0000000154112345678
                            numero_documento_referencia = inv.invoice_id.number_electronic
                            fecha_emision_referencia = inv.invoice_id.date_issuance
                            codigo_referencia = inv.reference_code_id.code
                            razon_referencia = inv.reference_code_id.name
                            medio_pago = ''
                        else:
                            TipoDocumento = '01'
                    if inv.type == 'out_refund':  # NC
                        if inv.invoice_id.journal_id.nd:
                            tipo_documento_referencia = '02'
                        else:
                            tipo_documento_referencia = '01'
                        TipoDocumento = '03'

                        if inv.origin and inv.origin.isdigit():
                            documento_origen = inv.env['account.invoice'].search([['number', '=', inv.origin]])
                            if documento_origen == False:
                                numero_documento_referencia = inv.origin
                                fecha_emision_referencia = fecha
                                codigo_referencia = inv.reference_code_id.code
                            else:
                                FacturaReferencia = documento_origen.number_electronic
                                fecha_emision_referencia = documento_origen.date_issuance
                                numero_documento_referencia = documento_origen.number_electronic

                                # FacturaReferencia = (inv.origin)
                                # numero_documento_referencia = inv.invoice_id.number_electronic
                                # fecha_emision_referencia = inv.invoice_id.date_issuance
                                codigo_referencia = inv.reference_code_id.code
                            razon_referencia = inv.reference_code_id.name
                        else:
                            numero_documento_referencia = inv.origin
                            fecha_emision_referencia = date_cr
                            codigo_referencia = inv.reference_code_id.code
                            tipo_documento_referencia = '99'

                    if inv.payment_term_id:
                        if inv.payment_term_id.sale_conditions_id:
                            sale_conditions = inv.payment_term_id.sale_conditions_id.sequence or '01'
                        else:
                            raise UserError(
                                'No se pudo Crear la factura electrónica: Debe configurar condiciones de pago para')
                    else:
                        sale_conditions = '01'

                    if inv.number.isdigit() and TipoDocumento:
                        currency_rate = 1 / inv.currency_id.rate
                        lines = []
                        base_total = 0.0
                        numero = 0

                        if inv.partner_id.identification_id.code == '05':
                            receptor_identificacion = {'tipo': False, 'numero': False, }
                            receptor_identificacion_extranjero = inv.partner_id.vat
                        else:
                            receptor_identificacion = {'tipo': inv.partner_id.identification_id.code,
                                'numero': inv.partner_id.vat, }
                            receptor_identificacion_extranjero = ''

                        totalserviciogravado = 0.0
                        totalservicioexento = 0.0
                        totalmercaderiagravado = 0.0
                        totalmercaderiaexento = 0.0
                        total_otros_cargos = 0.0

                        totalservicioexonerado = 0.0
                        totalmercaderiaexonerado = 0.0
                        totalexonerado = 0.0

                        impuestoTotal = 0.0
                        total_descuentos = 0.0
                        for inv_line in inv.invoice_line_ids:
                            print(inv_line.display_type)
                            if not inv_line.display_type:
                                impuestos_acumulados = 0.0
                                numero += 1
                                base_total += inv_line.price_unit * inv_line.quantity
                                subtotal_linea = round(inv_line.price_unit * inv_line.quantity, 4)
                                subtotal_linea_descontado = (
                                        subtotal_linea - round((subtotal_linea * (inv_line.discount / 100)), 4))
                                total_descuentos += round((subtotal_linea * (inv_line.discount / 100)), 4)
                                # subtotal_linea_descontado = float("%.4f" % (subtotal_linea - round((subtotal_linea * (
                                # inv_line.discount / 100)), 4)))
                                impuestos = []
                                otros_cargos = []
                                impuestoneto=0.0
                                exonerado = False
                                for i in inv_line.invoice_line_tax_ids:
                                    if i.tax_code not in ('00','TotalOtrosCargos'):
                                        impuesto_linea = round((i.amount / 100) * subtotal_linea_descontado, 4)
                                        if inv_line.exoneration_id:
                                            exonerado=True
                                            impuestos.append(
                                                {'codigo': i.tax_code, 'codigotarifa': i.codigo_tarifa, 'tarifa': i.amount,
                                                 'monto': round((i.amount / 100) * subtotal_linea_descontado, 4),
                                                 "exoneracion": {"tipodocumento": inv_line.exoneration_id.type,
                                                                 "numerodocumento":
                                                                     inv_line.exoneration_id.exoneration_number,
                                                                 "nombreinstitucion":
                                                                     inv_line.exoneration_id.name_institution,
                                                                 "fechaemision": str(
                                                                     inv_line.exoneration_id.date) + 'T00:00:00-06:00',
                                                                 "montoexoneracion": round(
                                                                     impuesto_linea *
                                                                     inv_line.exoneration_id.percentage_exoneration / 100,
                                                                     4), "porcentajeexoneracion": str(
                                                         int(inv_line.exoneration_id.percentage_exoneration))}})
                                            impuestoneto=round((i.amount / 100) * subtotal_linea_descontado, 4)-round(impuesto_linea * inv_line.exoneration_id.percentage_exoneration / 100, 4)
                                            impuestoTotal += round((i.amount / 100) * subtotal_linea_descontado,
                                                                 4) - round(
                                                impuesto_linea * inv_line.exoneration_id.percentage_exoneration / 100,
                                                4)
                                            impuestos_acumulados += round((i.amount / 100) * subtotal_linea_descontado,
                                                                                                             4) - round(
                                                                                            impuesto_linea * inv_line.exoneration_id.percentage_exoneration / 100,
                                                                                            4)
                                        else:
                                            impuestos.append(
                                                {'codigo': i.tax_code,
                                                 'codigotarifa': i.codigo_tarifa,
                                                 'tarifa': i.amount,
                                                 'monto': round((i.amount / 100) * subtotal_linea_descontado, 4),
                                                 })
                                            impuestoTotal += round((i.amount / 100) * subtotal_linea_descontado, 4)
                                            impuestos_acumulados += round((i.amount / 100) * subtotal_linea_descontado,
                                                                          4)
                                    if i.tax_code in ('TotalOtrosCargos'):
                                        otros_cargos.append(
                                            {'tipodocumento': i.codigo_tarifa, 'detalle': 'Impuesto de servicio',
                                                'porcentaje': i.amount,
                                                'montocargo': round((i.amount / 100) * inv_line.price_subtotal, 4), })
                                        total_otros_cargos += round((i.amount / 100) * subtotal_linea_descontado, 4)
                                if inv_line.product_id:
                                    if inv_line.product_id.type == 'service':
                                        if exonerado:
                                            totalservicioexonerado += inv_line.quantity * inv_line.price_unit
                                        else:
                                            if impuestos_acumulados:
                                                totalserviciogravado += inv_line.quantity * inv_line.price_unit
                                            else:
                                                totalservicioexento += inv_line.quantity * inv_line.price_unit
                                    else:
                                        if exonerado:
                                            totalmercaderiaexonerado += inv_line.quantity * inv_line.price_unit
                                        else:
                                            if impuestos_acumulados:
                                                totalmercaderiagravado += inv_line.quantity * inv_line.price_unit
                                            else:
                                                totalmercaderiaexento += inv_line.quantity * inv_line.price_unit

                                line = {'numero': numero, 'codigo': [{'tipo': inv_line.product_id.code_type_id.code or '04',
                                    'codigo': inv_line.product_id.default_code or '000', }], 'cantidad': inv_line.quantity,
                                    'unidad_medida': inv_line.uom_id.code,
                                    'unidad_medida_comercial': inv_line.product_id.commercial_measurement,
                                    'detalle': inv_line.name[:159], 'precio_unitario': inv_line.price_unit,
                                    'monto_total': inv_line.quantity * inv_line.price_unit, 'descuento': [
                                        {'monto': (round((subtotal_linea * (inv_line.discount / 100)), 4)) or '0.0',
                                         'naturaleza': inv_line.discount_note, }], 'subtotal': subtotal_linea_descontado,
                                    'impuestos': impuestos,
                                    'otroscargos': otros_cargos,
                                    'montototallinea': subtotal_linea_descontado + impuestos_acumulados,
                                    'impuestoneto':impuestoneto,
                                    'codigo_hacienda': inv_line.product_id.cabys_code or ''
                                        }
                                lines.append(line)
                        _logger.error('MAB - formando payload')
                        # total_descuentos = round(base_total, 4) - round(inv.amount_untaxed, 4)
                        if total_descuentos < 0.0:
                            total_descuentos = 0.0
                        payload = {
                            'clave': {'sucursal': '001', 'terminal': '00001', 'tipo': '04', 'comprobante': inv.number,
                                'pais': '506', 'dia': '%02d' % fecha.day, 'mes': '%02d' % fecha.month,
                                'anno': str(fecha.year)[2:4], 'situacion_presentacion': '1',
                                'codigo_seguridad': inv.company_id.security_code, },
                            'encabezado': {'fecha': date_cr, # 'fecha': "2018-01-19T23:17:00+06:00",
                                'condicion_venta': sale_conditions, 'plazo_credito': '0', 'medio_pago': medio_pago,
                                'codigo_actividad': inv.company_id.codigo_actividad, },
                            'emisor': {'nombre': inv.company_id.name,
                                'identificacion': {'tipo': inv.company_id.identification_id.code,
                                    'numero': inv.company_id.vat, },
                                'nombre_comercial': inv.company_id.commercial_name or '',
                                'ubicacion': {'provincia': inv.company_id.state_id.code,
                                    'canton': inv.company_id.county_id.code,
                                    'distrito': inv.company_id.district_id.code,
                                    'barrio': inv.company_id.neighborhood_id.code, 'sennas': inv.company_id.street, },
                                'telefono': {'cod_pais': inv.company_id.phone_code, 'numero': inv.company_id.phone, },
                                'fax': {'cod_pais': inv.company_id.phone_code, 'numero': inv.company_id.phone, },
                                'correo_electronico': inv.company_id.email, }, 'receptor': {  ##
                                'nombre': inv.partner_id.name[:80], 'identificacion': receptor_identificacion,
                                'IdentificacionExtranjero': receptor_identificacion_extranjero, }, 'detalle': lines,
                            'resumen': {'moneda': inv.currency_id.name, 'tipo_cambio': round(currency_rate, 5),
                                'totalserviciogravado': totalserviciogravado,
                                'totalservicioexento': totalservicioexento,
                                'totalmercaderiagravado': totalmercaderiagravado,
                                'totalmercaderiaexento': totalmercaderiaexento,
                                'totalgravado': totalserviciogravado + totalmercaderiagravado,
                                'totalexento': totalmercaderiaexento + totalservicioexento,
                                'totalservicioexonerado': totalservicioexonerado,
                                'totalmercaderiaexonerado': totalmercaderiaexonerado ,
                                'totalexonerado': totalmercaderiaexonerado+totalservicioexonerado,
                                'totalventa': totalserviciogravado + totalmercaderiagravado + totalservicioexento +
                                              totalmercaderiaexento+totalmercaderiaexonerado+totalservicioexonerado,
                                'totaldescuentos': total_descuentos, 'totalventaneta': (
                                                                                               totalserviciogravado +
                                                                                               totalmercaderiagravado
                                                                                               + totalservicioexento
                                                                                               +
                                                                                               totalmercaderiaexento)
                                                                                       - (
                                                                                           total_descuentos)+(totalmercaderiaexonerado+totalservicioexonerado),
                                'totalimpuestos': round(impuestoTotal, 4)
                                ,'totalotroscargos':total_otros_cargos
                                , 'totalcomprobante': (totalmercaderiaexonerado+totalservicioexonerado)+total_otros_cargos+((totalserviciogravado + totalmercaderiagravado + totalservicioexento + totalmercaderiaexento) - (
                                                                                                    total_descuentos)) + round(
                                    impuestoTotal, 4), }, 'referencia': [{'tipo_documento': tipo_documento_referencia,
                                'numero_documento': numero_documento_referencia,
                                'fecha_emision': fecha_emision_referencia, 'codigo': codigo_referencia,
                                'razon': razon_referencia, }], 'otros': [{'codigo': 'OC', 'texto': inv.name}], }
                        headers = {'content-type': 'application/json',
                            # 'authorization': "Bearer " + token['access_token'],
                        }
                        if inv.company_id.frm_ws_ambiente == 'stag':
                            requests_url = self.env.user.company_id.url_api + '/makeXML.stag.43'
                        else:
                            requests_url = self.env.user.company_id.url_api + '/makeXML.stag.43'
                        xdata = json.dumps(payload)
                        response_document = requests.post(requests_url, headers=headers, data=xdata)
                        _logger.error('MAB - JSON DATA:%s', xdata)
                        _logger.error('MAB - response:%s', response_document._content)
                        # response_content = json.loads(response_document._content)
                        response_content = json.loads(response_document._content.decode('utf-8'))
                        if response_content.get('code'):
                            if response_content.get('code') == 1:
                                inv.number_electronic = response_content.get('clave')
                                inv.date_issuance = date_cr
                            else:
                                continue
                                """
                                raise UserError(
                                    'No se pudo Crear la factura electrónica: \n' + str(response_document._content))
                                """
                        if response_document.status_code == 200:  # TODO
                            print(response_document)
                            if response_document._content:
                                # inv.electronic_invoice_return_message = response_document._content
                                inv.fname_xml_comprobante = 'comprobante_' + inv.number + '.xml'
                                inv.xml_comprobante = response_content.get('data')
                                # if not inv.partner_id.opt_out:
                                #	email_template = self.env.ref('account.email_template_edi_invoice', False)
                                #	attachment = self.env['ir.attachment'].search([('res_model','=',
                                #	'account.invoice'),('res_id','=',inv.id),('res_field','=','xml_comprobante')],
                                #	limit=1)
                                #	attachment.name = inv.fname_xml_comprobante
                                #	attachment.datas_fname = inv.fname_xml_comprobante
                                #	email_template.attachment_ids = [(6,0,[attachment.id])] # [(4, attachment.id)]
                                #	email_template.with_context(type='binary', default_type='binary').send_mail(
                                #	inv.id, raise_exception=False, force_send=True)#default_type='binary'
                                #	email_template.attachment_ids = [(3, attachment.id)]
                                if response_document._content.decode('utf-8').split(',')[1][5:] == 'false':
                                    raise UserError(
                                        'No se pudo Crear la factura electrónica: \n' + str(response_document._content))
                        elif response_document.status_code == 400:
                            if (response_content.get('hacienda_mensaje').find('anteriormente') > -1):
                                inv.fname_xml_comprobante = 'comprobante_' + inv.number + '.xml'
                                inv.xml_comprobante = response_content.get('data')
                                inv.number_electronic = response_content.get('clave')
                                inv.date_issuance = date_cr
                        else:
                            raise UserError(
                                'No se pudo Crear la factura electrónica: \n' + str(response_document._content))
                    print('Termino')

    @api.model
    def _enviar_hacienda(self):
        print('Entro enviar Hacienda')
        invoices = self.env['account.invoice'].search(
            [('date_invoice', '>=', '2018-11-01'), ('type', 'in', ('out_invoice', 'out_refund')),
             ('state', 'in', ('open', 'paid')), ('state_tributacion', '=', False),
             ('fname_xml_comprobante', '=', False), ], order='type asc', offset=0, limit=20)
        self._procesa_documentos(invoices)

    @api.model
    def _consultahacienda(self):  # cron
        invoices = self.env['account.invoice'].search(
            [('type', 'in', ('out_invoice', 'out_refund')), ('state', 'in', ('open', 'paid')),
             ('number_electronic', '!=', False), ('state_tributacion', '=', False)], order='type asc', offset=0,
            limit=5)

        for i in invoices:
            if i.number_electronic and len(i.number_electronic) == 50:
                if i.company_id.frm_ws_ambiente == 'stag':
                    requests_url = self.env.user.company_id.url_api + '/consultadocumento'
                else:
                    requests_url = self.env.user.company_id.url_api + '/consultadocumento'
                payload = {'clave': i.number_electronic, }
                headers = {'content-type': 'application/json', # 'authorization': "Bearer " + token['access_token'],
                }
                response_document = requests.post(requests_url, headers=headers, data=json.dumps(payload))
                # response_content = json.loads(response_document._content)
                response_content = json.loads(response_document._content.decode('utf-8'))
                if int(response_content.get('code')) == 33:
                    print('NO ENCONTRADO!!!!!!!!', response_content.get('code'))
                    # i.state_tributacion = 'no_encontrado'
                    # i.state_tributacion = False
                    i.fname_xml_comprobante = False
                    i.number_electronic = False

                if response_content.get('hacienda_result'):
                    if response_content.get('hacienda_result').get('ind-estado'):
                        print(response_content.get('hacienda_result').get('ind-estado'))
                        i.state_tributacion = response_content.get('hacienda_result').get('ind-estado')
                        i.fname_xml_respuesta_tributacion = 'respuesta_tributacion_' + i.number + '.xml'
                        i.xml_respuesta_tributacion = response_content.get('hacienda_result').get('respuesta-xml')

    """
    @api.multi
    def _enviar_correo_factura(self):
        documentos = self.env['account.invoice'].search(
            [('type', 'in', ('out_invoice', 'refund_invoice')), ('state', 'in', ('open', 'paid')),
             ('state_tributacion', '=', 'aceptado'), ('documento_enviado', '=', False)], offset=0, limit=5)
        root = self.env['res.users'].browse(SUPERUSER_ID)
        for documento in documentos:
            for cliente in documento.partner_id:
                email_cliente = cliente.email
                if not email_cliente:
                    pass
                else:
                    template_id = self.env['ir.model.data'].get_object_reference(
                        'account',
                        'email_template_edi_invoice')[1]
                    email_template_obj = self.env['mail.template'].browse(template_id)
                    if template_id:
                        values = email_template_obj.generate_email(documento.id, fields=None)
                        values['email_from'] = self.env.user.company_id.from_email_fe
                        values['email_to'] = cliente.email
                        values['res_id'] = documento.id
                        ir_attachment_obj = self.env['ir.attachment']
                        vals = {
                            'name': documento.number + '.pdf' or "Draft",
                            'type': 'binary',
                            'datas': values['attachments'][0][1],
                            'res_id': documento.id,
                            'res_model': 'account.invoice',
                            'datas_fname': documento.number + '.pdf' or "Draft",
                        }
                        xml = {
                            'name': documento.number + '.xml' or "Draft",
                            'type': 'binary',
                            'datas': documento.xml_comprobante,
                            'res_id': documento.id,
                            'res_model': 'account.invoice',
                            'datas_fname': documento.number + '.xml' or "Draft",
                        }
                        xml_respuesta = {
                            'name': 'Respuesta' + documento.number + '.xml' or "Draft",
                            'type': 'binary',
                            'datas': documento.xml_respuesta_tributacion,
                            'res_id': documento.id,
                            'res_model': 'account.invoice',
                            'datas_fname': 'Respuesta' + documento.number + '.xml' or "Draft",
                        }
                        attachment_id = ir_attachment_obj.create(vals)
                        attachment_id_xml = ir_attachment_obj.create(xml)
                        attachment_id_xml_repuesta = ir_attachment_obj.create(xml_respuesta)
                        # Set boolean field true after mass invoice email sent
                        documento.write({
                            'documento_enviado': True
                        })
                        mail_mail_obj = self.env['mail.mail']
                        msg_id = mail_mail_obj.create(values)
                        msg_id.attachment_ids = [
                            (6, 0, [attachment_id.id, attachment_id_xml.id, attachment_id_xml_repuesta.id])]
                        # msg_id.attachment_ids=[(6,0,[attachment_id_xml.id])]
                        # msg_id.attachment_ids=[(6,0,[attachment_id_xml_repuesta.id])]

                        if msg_id:
                            mail_mail_obj.send([msg_id])


"""

    @api.multi
    def _enviar_correo_factura(self):
        documentos = self.env['account.invoice'].search(
            [('type', 'in', ('out_invoice', 'out_refund')), ('state', 'in', ('open', 'paid')),
             ('state_tributacion', '=', 'aceptado'), ('documento_enviado', '=', False)], offset=0, limit=200)
        root = self.env['res.users'].browse(SUPERUSER_ID)
        for documento in documentos:
            for cliente in documento.partner_id:
                if not (cliente.email == False):
                    template_id = \
                    self.env['ir.model.data'].get_object_reference('account', 'email_template_edi_invoice')[1]
                    email_template_obj = self.env['mail.template'].browse(template_id)
                    if template_id:
                        values = email_template_obj.generate_email(documento.id, fields=None)
                        values['email_from'] = self.env.user.company_id.from_email_fe
                        # values['email_to'] = 'erickvenegascr@gmail.com'
                        values['email_to'] = cliente.email
                        values['res_id'] = documento.id
                        # pdf = self.env['report'].sudo().get_pdf([documento.id], 'account.report_invoice_document')
                        # pdf = self.env['report'].get_pdf([documento.id], 'account.report_invoice_with_payments')
                        pdf = self.env.ref('account.account_invoices').sudo().render_qweb_pdf([documento.id])[0]
                        ir_attachment_obj = self.env['ir.attachment']
                        vals = {'name': documento.number + '.pdf' or "Draft", 'type': 'binary',
                            'datas': base64.encodestring(pdf), 'res_id': documento.id, 'res_model': 'account.invoice',
                            'mimetype': 'application/x-pdf', 'datas_fname': documento.number + '.pdf' or "Draft", }
                        xml = {'name': documento.number + '.xml' or "Draft", 'type': 'binary',
                            'datas': documento.xml_comprobante, 'res_id': documento.id, 'res_model': 'account.invoice',
                            'datas_fname': documento.number + '.xml' or "Draft", }
                        xml_respuesta = {'name': 'Respuesta' + documento.number + '.xml' or "Draft", 'type': 'binary',
                            'datas': documento.xml_respuesta_tributacion, 'res_id': documento.id,
                            'res_model': 'account.invoice',
                            'datas_fname': 'Respuesta' + documento.number + '.xml' or "Draft", }
                        attachment_id = ir_attachment_obj.create(vals)
                        attachment_id_xml = ir_attachment_obj.create(xml)
                        attachment_id_xml_repuesta = ir_attachment_obj.create(xml_respuesta)
                        # Set boolean field true after mass invoice email sent
                        documento.write({'documento_enviado': True})
                        mail_mail_obj = self.env['mail.mail']
                        msg_id = mail_mail_obj.create(values)
                        msg_id.attachment_ids = [
                            (6, 0, [attachment_id.id, attachment_id_xml.id, attachment_id_xml_repuesta.id])]
                        # msg_id.attachment_ids=[(6,0,[attachment_id_xml.id])]
                        # msg_id.attachment_ids=[(6,0,[attachment_id_xml_repuesta.id])]

                        if msg_id:
                            mail_mail_obj.send([msg_id])


class FacturasAplicaInventario(models.TransientModel):
    """
    Envia y firma a hacienda
    """
    _name = "account.envia"
    _description = "Envia Hacienda Facturas"

    @api.multi
    def consulta_documento(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        for i in self.env['account.invoice'].browse(active_ids):
            if i.number_electronic and len(i.number_electronic) == 50:
                if i.company_id.frm_ws_ambiente == 'stag':
                    requests_url = self.env.user.company_id.url_api + '/consultadocumento'
                else:
                    requests_url = self.env.user.company_id.url_api + '/consultadocumento'
                payload = {'clave': i.number_electronic, }
                headers = {'content-type': 'application/json',  # 'authorization': "Bearer " + token['access_token'],
                           }
                response_document = requests.post(requests_url, headers=headers, data=json.dumps(payload))
                response_content = json.loads(response_document._content.decode('utf-8'))
                if int(response_content.get('code')) == 33:
                    # i.state_tributacion = 'no_encontrado'
                    # i.state_tributacion = False
                    i.fname_xml_comprobante = False
                    i.number_electronic = False

                if response_content.get('hacienda_result'):
                    if response_content.get('hacienda_result').get('ind-estado'):
                        i.state_tributacion = response_content.get('hacienda_result').get('ind-estado')
                        i.fname_xml_respuesta_tributacion = 'respuesta_tributacion_' + i.number + '.xml'
                        i.xml_respuesta_tributacion = response_content.get('hacienda_result').get('respuesta-xml')

    @api.multi
    def firma_envia_hacienda(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        _logger.error('Firma envia hacienda|')
        for inv in self.env['account.invoice'].browse(active_ids):
            inv._procesa_documentos(inv)


class Exoneration(models.Model):
    _name = "account.exoneration"
    type = fields.Selection([('01', 'Compras Autorizadas'),
                             ('02', 'Ventas exentas a diplomáticos'),
                             ('03', 'Ventas exentas a diplomáticos'),
                             ('04', 'Exenciones Dirección General de Hacienda'),
                             ('05', 'Transitorio V'),
                             ('06', 'Transitorio IX'),
                             ('07', 'Transitorio XVII'),
                             ('99', 'Otros')], 'Tipo Exoneracion')
    '''
    01 Compras Autorizadas, 02 Ventas exentas a diplomáticos, 03 Autorizado por Ley Especial, 04 Exenciones 
    Dirección General de Hacienda, 05 Transitorio V, 06 Transitorio IX, 07 Transitorio XVII, 99 Otros
    '''
    exoneration_number = fields.Char(string="Número de exoneración", required=True, )
    name_institution = fields.Char(string="Nombre de institución", required=False, )
    date = fields.Date(string="Fecha", required=True, )
    percentage_exoneration = fields.Float(string="Porcentaje de exoneración", required=False, )
    nombre = fields.Char(string="Nombre Exoneracion", required=True, )

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, record.nombre))
        return result
class WizardCabysProductos(models.TransientModel):
    """
        Asistente Usuario articulos masivo agregar Cabys
    """
    _name = "product.wizard_cabys"
    _description = "Wizard actualizar Cabys"

    cabys_code = fields.Char('Código Cabys', readonly=False, required=True,
                       help="Código Cabys")
    reemplazar_codigo_existente = fields.Boolean('Reemplazar Cabys')


    @api.multi
    def actualiza_cabys(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []
        for i in self.env['product.template'].browse(active_ids):
            if self.reemplazar_codigo_existente:
                i.cabys_code=self.cabys_code
            elif not i.cabys_code:
                i.cabys_code = self.cabys_code
                '''3563204010606'''
            elif len(i.cabys_code) != 13:
                i.cabys_code = self.cabys_code

