from odoo import _
import logging
from odoo.exceptions import UserError
from decimal import *
from odoo.tests.common import Form

from xml.sax.saxutils import escape
_logger = logging.getLogger(__name__)
import xml.etree.ElementTree as ET
import base64
import re

try:
    from lxml import etree
except ImportError:
    from xml.etree import ElementTree
'''
try:
    from OpenSSL import crypto
except(ImportError, IOError) as err:
    logging.info(err)
'''
def schema_validator(xml_file, xsd_file) -> bool:
    """
    verifies a xml
    :param xml_invoice: Invoice xml
    :param  xsd_file: XSD File Name
    :return:
    """

    xmlschema = etree.XMLSchema(etree.parse(os.path.join(
        os.path.dirname(__file__), "xsd/" + xsd_file
    )))

    xml_doc = base64decode(xml_file)
    root = etree.fromstring(xml_doc, etree.XMLParser(remove_blank_text=True))
    result = xmlschema.validate(root)

    return result

def load_xml_data(invoice, load_lines, account_id, product_id=False, analytic_account_id=False, default_iva_ventas=False):

        try:
            xml_string = base64.b64decode(invoice.xml_supplier_approval).decode("utf-8")
            #quita saltos de linea
            xml_string = xml_string.replace('\n','')
            pattern = re.compile(r'>\s+<')
            xml_string = re.sub(pattern, '><', xml_string)
            invoice_xml = etree.XML(bytes(bytearray(xml_string, encoding='utf-8')))
            document_type = re.search('FacturaElectronica|NotaCreditoElectronica|NotaDebitoElectronica|TiqueteElectronico', invoice_xml.tag).group(0)

            if document_type == 'TiqueteElectronico':
                raise UserError(_("Los tiquetes electrónicos no son aceptados como gastos"))

        except Exception as e:
            raise UserError(_("Este xml esta en un formato incorrecto: %s") % e)

        namespaces = invoice_xml.nsmap
        inv_xmlns = namespaces.pop(None)
        namespaces['inv'] = inv_xmlns

        # invoice.consecutive_number_receiver = invoice_xml.xpath("inv:NumeroConsecutivo", namespaces=namespaces)[0].text
        invoice.reference = invoice_xml.xpath("inv:NumeroConsecutivo", namespaces=namespaces)[0].text

        invoice.number_electronic = invoice_xml.xpath("inv:Clave", namespaces=namespaces)[0].text
        activity_node = invoice_xml.xpath("inv:CodigoActividad", namespaces=namespaces)
        activity = False
        '''
        if activity_node:
            activity_id = activity_node[0].text
            activity = invoice.env['economic.activity'].with_context(active_test=False).search([('code', '=', activity_id)], limit=1)
        else:
            activity_id = False
        invoice.economic_activity_id = activity
        '''
        invoice.date_issuance = invoice_xml.xpath("inv:FechaEmision", namespaces=namespaces)[0].text
        invoice.date_invoice = invoice.date_issuance
        invoice.tipo_documento= False

        emisor = invoice_xml.xpath("inv:Emisor/inv:Identificacion/inv:Numero", namespaces=namespaces)[0].text

        receptor_node = invoice_xml.xpath("inv:Receptor/inv:Identificacion/inv:Numero", namespaces=namespaces)
        if receptor_node:
            receptor = receptor_node[0].text
        else:
            raise UserError('El receptor no está definido en el xml')  # noqa

        if receptor != invoice.company_id.vat:
            raise UserError('El receptor no corresponde con la compañía actual con identificación ' +
                             receptor + '. Por favor active la compañía correcta.')  # noqa

        currency_node = invoice_xml.xpath("inv:ResumenFactura/inv:CodigoTipoMoneda/inv:CodigoMoneda", namespaces=namespaces)

        if currency_node:
            invoice.currency_id = invoice.env['res.currency'].search([('name', '=', currency_node[0].text)], limit=1).id
        else:
            invoice.currency_id = invoice.env['res.currency'].search([('name', '=', 'CRC')], limit=1).id

        partner = invoice.env['res.partner'].search([('vat', '=', emisor),
                                                    ('supplier', '=', True),
                                                    '|',
                                                     ('company_id', '=', invoice.company_id.id),
                                                     ('company_id', '=', False)],
                                                    limit=1)

        if partner:
            invoice.partner_id = partner
        else:
            raise UserError(_('El proveedor no existe, por favor creelo antes de cargar este documento'))

        invoice.account_id = partner.property_account_payable_id
        invoice.payment_term_id = partner.property_supplier_payment_term_id

        payment_method_node = invoice_xml.xpath("inv:MedioPago", namespaces=namespaces)
        if payment_method_node:
            invoice.payment_methods_id = invoice.env['payment.methods'].search([('sequence', '=', payment_method_node[0].text)], limit=1)
        else:
            invoice.payment_methods_id = partner.payment_methods_id

        _logger.debug('FECR - load_lines: %s - account: %s' %
                      (load_lines, account_id))

        product = False
        if product_id:
            product = product_id.id

        analytic_account = False
        if analytic_account_id:
            analytic_account = analytic_account_id.id

        # if load_lines and not invoice.invoice_line_ids:
        if load_lines:
            lines = invoice_xml.xpath("inv:DetalleServicio/inv:LineaDetalle", namespaces=namespaces)
            new_lines = invoice.env['account.invoice.line']
            for line in lines:
                cabys = False
                cabys_node = line.xpath("inv:Codigo", namespaces=namespaces)
                if cabys_node:
                    cabys = line.xpath("inv:Codigo", namespaces=namespaces)[0].text

                product_uom = invoice.env['uom.uom'].search(
                    [('code', '=', line.xpath("inv:UnidadMedida", namespaces=namespaces)[0].text)],
                    limit=1).id
                total_amount = float(line.xpath("inv:MontoTotal", namespaces=namespaces)[0].text)

                discount_percentage = 0.0
                discount_note = None

                if total_amount > 0:
                    discount_node = line.xpath("inv:Descuento", namespaces=namespaces)
                    if discount_node:
                        discount_amount_node = discount_node[0].xpath("inv:MontoDescuento", namespaces=namespaces)[0]
                        discount_amount = float(discount_amount_node.text or '0.0')
                        discount_percentage = discount_amount / total_amount * 100
                        discount_note = discount_node[0].xpath("inv:NaturalezaDescuento", namespaces=namespaces)[0].text
                    else:
                        discount_amount_node = line.xpath("inv:MontoDescuento", namespaces=namespaces)
                        if discount_amount_node:
                            discount_amount = float(discount_amount_node[0].text or '0.0')
                            discount_percentage = discount_amount / total_amount * 100
                            discount_note = line.xpath("inv:NaturalezaDescuento", namespaces=namespaces)[0].text

                total_tax = 0.0
                taxes = []

                tax_nodes = line.xpath("inv:Impuesto", namespaces=namespaces)
                impuesto_tasa=0.0
                for tax_node in tax_nodes:
                    tax_code = re.sub(r"[^0-9]+", "", tax_node.xpath("inv:Codigo", namespaces=namespaces)[0].text)
                    tax_amount = float(tax_node.xpath("inv:Tarifa", namespaces=namespaces)[0].text)
                    _logger.debug('FECR - tax_code: %s', tax_code)
                    _logger.debug('FECR - tax_amount: %s', tax_amount)

                    if product_id and product_id.non_tax_deductible:
                        tax = invoice.env['account.tax'].search(
                                        [('tax_code', '=', tax_code),
                                        ('amount', '=', tax_amount),
                                        ('type_tax_use', '=', 'purchase'),
                                        ('non_tax_deductible', '=', True),
                                        ('active', '=', True)],
                                        limit=1)
                    else:
                        '''
                        tax = invoice.env['account.tax'].search(
                            [('tax_code', '=', tax_code),
                            ('amount', '=', tax_amount),
                            ('type_tax_use', '=', 'purchase'),
                            ('non_tax_deductible', '=', False),
                            ('active', '=', True)],
                            limit=1)
                        '''
                        tax = invoice.env['account.tax'].search(
                            [('tax_code', '=', tax_code),
                             ('amount', '=', tax_amount),
                             ('type_tax_use', '=', 'purchase'),
                             ('active', '=', True)],
                            limit=1)
                    if tax:
                        total_tax += float(tax_node.xpath("inv:Monto", namespaces=namespaces)[0].text)

                        exonerations = tax_node.xpath("inv:Exoneracion", namespaces=namespaces)
                        if exonerations:
                            raise UserError(_(
                                'Tiene exoneracion informaor a soporte, no estaimplementado'))
                            '''
                            for exoneration_node in exonerations:
                                exoneration_percentage = float(exoneration_node.xpath("inv:PorcentajeExoneracion", namespaces=namespaces)[0].text)
                                tax = invoice.env['account.tax'].search(
                                [('percentage_exoneration', '=', exoneration_percentage),
                                ('type_tax_use', '=', 'purchase'),
                                ('non_tax_deductible', '=', False),
                                ('has_exoneration', '=', True),
                                ('active', '=', True)],
                                limit=1)
                                taxes.append((4, tax.id))
                            '''
                        else:
                            taxes.append((4, tax.id))
                            impuesto_tasa=tax_amount
                    else:
                        if product_id and product_id.non_tax_deductible:
                            raise UserError(_('Tax code %s and percentage %s as non-tax deductible is not registered in the system' % (tax_code, tax_amount)))
                        else:
                            raise UserError(_('El código de tarifa %s con el impuesto %s no existe en el sistema debe de crearlo' % (tax_code, tax_amount)))

                _logger.debug('FECR - impuestos de linea: %s' % (taxes))

                codigo_articulo_linea = line.xpath("inv:CodigoComercial", namespaces=namespaces)
                codigo_articulo_xml = False
                for codigo in codigo_articulo_linea:
                    codigo_articulo_xml=codigo.xpath("inv:Codigo", namespaces=namespaces)[0].text

                product_odoo = invoice.env['product.template'].search(
                    [
                        ('default_code', '=', codigo_articulo_xml),
                     ],
                    limit=1)

                if product_odoo:
                    pass
                else:
                    costo=Decimal(line.xpath("inv:PrecioUnitario", namespaces=namespaces)[0].text)
                    imp= Decimal(impuesto_tasa)
                    calculo_precio = calcula_precio_venta(costo,imp)
                    line.xpath("inv:PrecioUnitario", namespaces=namespaces)[0].text
                    product_odoo = invoice.env['product.template'].create(
                        {
                        'name' : line.xpath("inv:Detalle", namespaces=namespaces)[0].text,
                        'code_type_id' :  '4',
                        'type': 'product',
                        'default_code' : codigo_articulo_xml,
                        'commercial_measurement' : 'Unidad',
                        'standard_price' : line.xpath("inv:PrecioUnitario", namespaces=namespaces)[0].text,
                        'list_price' : calculo_precio['precio_sin_iva'],
                        'margen_costo': calculo_precio['margen_costo'],
                        'precio_ivi': calculo_precio['precio_iva_incluido'],
                        'taxes_id' : (4,default_iva_ventas),
                        'cabys_code' : cabys,

                        }
                    )
                    #invoice_form = Form(self.env['account.invoice'], view='account.invoice_supplier_form')
                    #invoice = invoice_form.save()
                #'supplier_taxes_id': taxes,
                invoice_line = invoice.env['account.invoice.line'].create({
                    'name': line.xpath("inv:Detalle", namespaces=namespaces)[0].text,
                    'invoice_id': invoice.id,
                    'price_unit': line.xpath("inv:PrecioUnitario", namespaces=namespaces)[0].text,
                    'quantity': line.xpath("inv:Cantidad", namespaces=namespaces)[0].text,
                    'uom_id': product_uom,
                    'sequence': line.xpath("inv:NumeroLinea", namespaces=namespaces)[0].text,
                    'discount': discount_percentage,
                    'discount_note': discount_note,
                    #'total_amount': total_amount,
                    'product_id': product_odoo.id,
                    'account_id': 75,
                    #'account_analytic_id': analytic_account,
                    'amount_untaxed': float(line.xpath("inv:SubTotal", namespaces=namespaces)[0].text),
                    'total_tax': total_tax,
                    #'economic_activity_id': invoice.economic_activity_id.id,
                })

                # This must be assigned after line is created
                invoice_line.invoice_line_tax_ids = taxes
                #invoice_line.economic_activity_id = activity
                new_lines += invoice_line

            invoice.invoice_line_ids = new_lines

        invoice.amount_total_electronic_invoice = invoice_xml.xpath("inv:ResumenFactura/inv:TotalComprobante", namespaces=namespaces)[0].text

        tax_node = invoice_xml.xpath("inv:ResumenFactura/inv:TotalImpuesto", namespaces=namespaces)
        if tax_node:
            invoice.amount_tax_electronic_invoice = tax_node[0].text

        invoice.compute_taxes()

def calcula_precio_venta(costo,impuesto):
    calculo={}
    if costo<=Decimal(100):
        #45%
        margen=45
        precio_sin_iva = (costo*(1+(margen/Decimal(100))))
        precio_iva_incluido = precio_sin_iva*(1+(impuesto/100))
        calculo['margen_costo']=margen
        calculo['precio_sin_iva'] = precio_sin_iva
        calculo['precio_iva_incluido'] = precio_iva_incluido
    if costo >=Decimal(101) and costo <= Decimal(1000):
        #40
        margen = 40
        precio_sin_iva = (costo * (1 + (margen / Decimal(100))))
        precio_iva_incluido = precio_sin_iva * (1 + (impuesto / 100))
        calculo['margen_costo'] = margen
        calculo['precio_sin_iva'] = precio_sin_iva
        calculo['precio_iva_incluido'] = precio_iva_incluido
    elif costo>=Decimal(1001) and costo<=Decimal(10000):
        #35
        margen = 35
        precio_sin_iva = (costo * (1 + (margen / Decimal(100))))
        precio_iva_incluido = precio_sin_iva * (1 + (impuesto / 100))
        calculo['margen_costo'] = margen
        calculo['precio_sin_iva'] = precio_sin_iva
        calculo['precio_iva_incluido'] = precio_iva_incluido
    elif costo>=Decimal(10000) and costo<=Decimal(100000):
        #32
        margen = 32
        precio_sin_iva = (costo * (1 + (margen / Decimal(100))))
        precio_iva_incluido = precio_sin_iva * (1 + (impuesto / 100))
        calculo['margen_costo'] = margen
        calculo['precio_sin_iva'] = precio_sin_iva
        calculo['precio_iva_incluido'] = precio_iva_incluido
    else:
        margen = 50
        precio_sin_iva = (costo * (1 + (margen / Decimal(100))))
        precio_iva_incluido = precio_sin_iva * (1 + (impuesto / 100))
        calculo['margen_costo'] = margen
        calculo['precio_sin_iva'] = precio_sin_iva
        calculo['precio_iva_incluido'] = precio_iva_incluido
    return calculo