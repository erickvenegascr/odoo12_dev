# -*- coding: utf-8 -*-

from odoo import models, api, _
from odoo.exceptions import UserError

class FacturasAplicaInventario(models.TransientModel):
    """
    Descarga del inventario las facturas que provienen de una orden reparacion
    """
    _name = "rev_inv.facturas"
    _description = "Aplica inventario de las facturas"

    @api.multi
    def descargaFacturas(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for record in self.env['account.invoice'].browse(active_ids):
            if record.state in ('draft', 'proforma', 'proforma2'):
                raise UserError("Las facturas selecionadas se aplicaran al inventario")
            if record.origin.find('PV')==-1 and record.type not in ('out_refund'):
                print 'Facturas: '+record.number+', '
                record.action_stock_transfer()
                for inv in self.env['stock.picking'].browse(record.invoice_picking_id.id):
                    inv.ensure_one()
                    # If still in draft => confirm and assign
                    if inv.state == 'draft':
                        inv.action_confirm()
                        if inv.state != 'assigned':
                            inv.action_assign()
                            if inv.state != 'assigned':
                                raise UserError(_("Could not reserve all requested products. Please use the \'Mark as Todo\' button to handle the reservation manually."))
                    for pack in inv.pack_operation_ids:
                        if pack.product_qty > 0:
                            pack.write({'qty_done': pack.product_qty})
                        else:
                            pack.unlink()
                    inv.do_transfer()

        return {'type': 'ir.actions.act_window_close'}

    @api.multi
    def aplica_inventario(self):
        context = dict(self._context or {})
        ids_seleccionados = context.get('active_ids', []) or []
        for mov_inventario in self.env['stock.picking'].browse(ids_seleccionados):
            if mov_inventario.state not in ('cancel','draft','done'):
                mov_inventario.ensure_one()
                for pack in mov_inventario.pack_operation_ids:
                    if pack.product_qty > 0:
                        pack.write({'qty_done': pack.product_qty})
                    else:
                        pack.unlink()
                mov_inventario.do_transfer()
        return {'type': 'ir.actions.act_window_close'}



# class rev_inv(models.Model):
#     _name = 'rev_inv.rev_inv'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100
