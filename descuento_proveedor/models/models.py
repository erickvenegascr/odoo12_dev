# -*- coding: utf-8 -*-

from odoo import models, fields, api

class descuento_proveedor(models.Model):
	_inherit = 'purchase.order.line'
	por_descuento = fields.Float(readonly=False,store=True,string="% Descuento")
	precio_unit_sin_desc = fields.Float(readonly=False,store=True,string="Precio sin Descuento")

	@api.onchange('por_descuento', 'precio_unit_sin_desc') 
	def _descuento_calculo(self):
		descuento=float(self.por_descuento)/100
		preciosindescuento=float(self.precio_unit_sin_desc)
		resultado=preciosindescuento-(preciosindescuento*descuento)
		self.price_unit=float(resultado)



# class descuento_proveedor(models.Model):
#     _name = 'descuento_proveedor.descuento_proveedor'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100