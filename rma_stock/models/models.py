# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
import logging

_logger = logging.getLogger(__name__)
"""
('draft', 'Quotation'),
('cancel', 'Cancelled'),
('confirmed', 'Confirmed'),
('under_repair', 'Under Repair'),
('ready', 'Ready to Repair'),
('2binvoiced', 'To be Invoiced'),
('invoice_except', 'Invoice Exception'),
('done', 'Repaired')], string='Status',
"""
class ProductProduct(models.Model):
	_inherit = 'product.template'
	rma_stock_used = fields.Integer(string='Cant Reparaciones', compute="_compute_rma_stock_used")
	@api.one
	def _compute_rma_stock_used(self):
		print 'HOLLLLLLLLLLLLLLLAAAAAAAAAAAAAAAA!!!!!!!'
		obj = self.env['mrp.repair.fee']
		for record in self:
			qty = 0
			lines = obj.search([('product_id','=',record.id),
				('repair_id.state','in',['confirmed','under_repair','ready','2binvoiced'])])
			for line in lines:
				qty += line.product_uom_qty
			record.rma_stock_used=qty


class InventoryLine(models.Model):
	_inherit='stock.inventory.line'
	rma_stock_used = fields.Integer(string='Cant Reparaciones', compute="_compute_rma_stock_used")
	cant_ajuste = fields.Float(string='Ajustar', compute="_diferencia")
	montoAjuste = fields.Float(string='Monto Ajuste',store=True,compute="_monto")

	@api.one
	@api.depends('location_id', 'product_id', 'package_id', 'product_uom_id', 'company_id', 'prod_lot_id', 'partner_id')
	def _compute_rma_stock_used(self):
		obj = self.env['mrp.repair.fee']
		lines = obj.search([('product_id','=',self.product_id.id),
			('repair_id.state','in',['confirmed','under_repair','ready','2binvoiced'])])
		qty = 0
		for a in lines:
			qty += a.product_uom_qty
		self.rma_stock_used=qty
	@api.one
	@api.depends('location_id', 'product_id', 'package_id', 'product_uom_id', 'company_id', 'prod_lot_id', 'partner_id')
	def _diferencia(self):
		self.cant_ajuste=self.product_qty-self.theoretical_qty
	@api.one
	@api.depends('location_id', 'product_id', 'package_id', 'product_uom_id', 'company_id', 'prod_lot_id', 'partner_id')
	def _monto(self):
		monto = ((self.product_qty - self.theoretical_qty) * self.product_id.standard_price)
		self.montoAjuste = monto


class Inventory(models.Model):
	_inherit = 'stock.inventory'
	montoAjustePositivo=fields.Float(string='Monto ajuste positivo',compute='_montos')
	montoAjusteNegativo=fields.Float(string='Monto ajuste negativo',compute='_montos')

	@api.multi
	def _montos(self):
		monto_negativo=0.0
		monto_positivo=0.0
		for linea in self.line_ids:
			if linea.montoAjuste>0:
				monto_positivo+=linea.montoAjuste
			else:
				monto_negativo+=linea.montoAjuste
		self.montoAjusteNegativo=monto_negativo
		self.montoAjustePositivo=monto_positivo

	@api.multi
	def action_done(self):
		for line in self.line_ids:
			print '!!Procesando: '+line.product_id.name
		negative = next((line for line in self.mapped('line_ids') if
						 line.product_qty < 0 and line.product_qty != line.theoretical_qty), False)
		if negative:
			raise UserError(_('You cannot set a negative product quantity in an inventory line:\n\t%s - qty: %s') % (
			negative.product_id.name, negative.product_qty))
		self.action_check()
		self.write({'state': 'done'})
		self.post_inventory()
		return True


class Repair(models.Model):
	_inherit='mrp.repair'
	user_id = fields.Many2one('res.users', string='Comercial', track_visibility='onchange',required=True)

	@api.multi
	def action_invoice_create(self, group=False):
		res = super(Repair, self).action_invoice_create()
		self.invoice_id.user_id=self.user_id
		return res



#res = super(Repair, self).action_invoice_open()




# class rma_stock(models.Model):
#     _name = 'rma_stock.rma_stock'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100
