 

Version 10.0.0.7:
		- Update date format in orderlist and popup

Version 10.0.1.7:
		- Add new feature to restrict on pos orders to load.

Version 10.0.1.8:
		- Update search view and correct session load count.
