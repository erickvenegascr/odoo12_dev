import logging
from openerp import models, SUPERUSER_ID, tools

_logger = logging.getLogger(__name__)


class res_users(models.Model):
    _inherit = 'res.users'

    def authenticate(self, db, login, password, user_agent_env):
        uid = super(res_users, self).authenticate(db, login, password, user_agent_env)
        if uid == SUPERUSER_ID:
            # the first time the admin logs in, we put the localhost url into report.url
            # this param is used in report generation as a base url for CSS dependencies
            cr = self.pool.cursor()
            ICP = self.pool['ir.config_parameter']
            try:
                ICP = self.pool['ir.config_parameter']
                base = 'http://' + (tools.config['xmlrpc_interface'] or '127.0.0.1') + ':' + str(tools.config['xmlrpc_port'])
                old_base = ICP.get_param(cr, uid, 'report.url')
                if not old_base or old_base != base:
                    ICP.set_param(cr, uid, 'report.url', base)
                cr.commit()
            except Exception:
                _logger.exception("Failed to update report.url configuration parameter")
            finally:
                cr.close()

        return uid
