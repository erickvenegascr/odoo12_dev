Fix URL report
=============================================

This Fix is necessary when the domain used in the browser can't be used for generating reports 
(if Odoo is in a container for instance)


Version française
===============

Correction URL report
------------
Cette correction est nécessaire lorsque le domaine utilisé par le fureteur ne peut pas être utilisé pour générer des rapports
(lorsque Odoo est dans un container par exemple)


Maintainer
----------

This module is maintained by Microcom.

.. image:: http://microcom.ca/odoo/MICROCOM_500x146.png
   :alt: Microcom
   :target: https://microcom.ca/en/contacts/
