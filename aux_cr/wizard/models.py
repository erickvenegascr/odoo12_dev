# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)
from odoo.exceptions import UserError, RedirectWarning, ValidationError

#models.TransientModel
class WizardCierreCaja(models.TransientModel):
    _name="cierre.wizard"
    _description="Wizard Cierre"
    ruta = fields.Char('Ruta', readonly=False, required=False,
                       help="Campo para identificar en que semana se realizo la acción")
    fecha_cierre = fields.Date("Fecha")
    product_ids = fields.Many2many('product.product','prod_id', string='Productos')

    @api.multi
    def action_report(self):
        """Metodo que llama la lógica que genera el reporte"""
        datas={'ids': self.env.context.get('active_ids', [])}
        res = self.read(['fecha_cierre'])
        res = res and res[0] or {}
        datas['form'] = res
        domain=[]
        busqueda=[]
        domain_detalle_cierre=[]
        if self.fecha_cierre:
            domain=[('date_invoice','=',self.fecha_cierre)
            ,('type','in',['out_refund','out_invoice']),('state','!=','draft')]
            busqueda = [('date_invoice', '=', self.fecha_cierre)]
            domain_recibos=[('partner_type','=','customer'),('payment_date','=',self.fecha_cierre),('invoice_ids','!=',False),('state','!=','draft')]
            domain_recibos_sin_factura=[('payment_date','=',self.fecha_cierre),('state','!=','draft'),('invoice_ids','=',False),('payment_type','in',['inbound'])]
            domain_cierre_detalle=[('fecha_inicio', '=', self.fecha_cierre)]
        else:
            domain = [('sale_ruta_id', '=like', self.ruta), ('type', 'in', ['out_refund', 'out_invoice']),
                      ('state', '!=', 'draft')]
            busqueda = [('sale_ruta_id', '=', self.ruta)]
            domain_recibos = [('partner_type', '=', 'customer'), ('sale_ruta_id', '=like', self.ruta),
                              ('invoice_ids', '!=', False), ('state', '!=', 'draft')]
            domain_recibos_sin_factura = [('sale_ruta_id', '=like', self.ruta), ('state', '!=', 'draft'),
                                          ('invoice_ids', '=', False), ('payment_type', 'in', ['inbound'])]
            domain_cierre_detalle = [('ruta', '=', self.ruta)]
            domain = [('sale_ruta_id', '=like', self.ruta), ('type', 'in', ['out_refund', 'out_invoice']),
                      ('state', '!=', 'draft')]
            domain_proveedor = [('sale_ruta_id', '=like', self.ruta), ('type', 'in', ['in_refund', 'in_invoice']),
                      ('state', '!=', 'draft')]
            domain_recibos_proveedor = [('partner_type', '<>', 'customer'), ('sale_ruta_id', '=like', self.ruta),
                              ('invoice_ids', '!=', False), ('state', '!=', 'draft')]


        fields=['number','date_invoice','amount_untaxed','amount_tax','amount_total','payment_term_id','payment_methods_id','type','nombre_cliente','subtotal_gravado','subtotal_exento','invoice_line_ids','partner_id']
        fields_cierre=['tipo','clasificacion','account_id','descripcion','monto']
        fields_recibo_proveedor=['invoice_vendor_references','metodo_pago','partner_id','amount','move_line_ids']
        facturas_ob = self.env['account.invoice'].search_read(domain,fields,order='payment_methods_id asc,type desc')
        facturas_ob_proveedor = self.env['account.invoice'].search_read(domain_proveedor, fields, order='payment_term_id asc,type desc')
        facturas_arts = self.env['account.invoice'].search_read(busqueda, fields, order='payment_term_id asc,type desc')
        facturas_recorset = self.env['account.invoice'].search(domain)
        cierre_recorset = self.env['cierre.caja'].search(domain_cierre_detalle)
        detalle_cierre=[]
        saldo_caja={"efectivo":0.0,"saldo":0.0,"monto_reportado":0.0}
        moto_reportado=0.0
        total_ingreso_egreso=0.0
        for cierre in cierre_recorset:
            saldo_caja["saldo"]=cierre.monto_efectivo
            saldo_caja["monto_reportado"]=cierre.monto_efectivo
            moto_reportado = cierre.monto_efectivo
            cierre_detalle_obj = self.env['cierre.caja.detalle'].search_read([('cierre_id', '=', cierre.id)], fields_cierre,
                                                                            order='tipo desc,clasificacion asc,descripcion asc')
            clasificacion_dic={1: 'Aumento de Ruta', 2: 'Cuenta por pagar TC', 3: 'Fondo Inicial', 4: 'Pago Proveedor',
            5: 'Gastos LM', 6: 'Gastos AC', 7: 'BADS'}
            subtotal=0.0
            descrip_subtotal=''
            subtotal_total=0.0
            clasificacion=0
            for c in cierre_detalle_obj:
                print(c)
                if clasificacion==0:
                    clasificacion = c['clasificacion']
                if clasificacion!=c['clasificacion']:
                    print('cambio')
                clasificacion = c['clasificacion']
                print(c['clasificacion'])
            for cierre_detalle in cierre_detalle_obj:
                if len(descrip_subtotal)==0:
                    descrip_subtotal=clasificacion_dic[cierre_detalle['clasificacion']]
                if (descrip_subtotal==clasificacion_dic[cierre_detalle['clasificacion']]):
                    if cierre_detalle['tipo'] == 'ingreso':
                        subtotal += cierre_detalle['monto']
                        subtotal_total += cierre_detalle['monto']
                    else:
                        subtotal += cierre_detalle['monto']*-1
                        subtotal_total += (cierre_detalle['monto']*-1)
                else:
                    detalle_cierre.append({'** Total '+descrip_subtotal: subtotal})
                    descrip_subtotal=clasificacion_dic[cierre_detalle['clasificacion']]
                    subtotal=0.0
                    if cierre_detalle['tipo'] == 'ingreso':
                        subtotal += cierre_detalle['monto']
                        subtotal_total += cierre_detalle['monto']
                    else:
                        subtotal += cierre_detalle['monto']*-1
                        subtotal_total += (cierre_detalle['monto']*-1)

                descrip=str(cierre_detalle['tipo'].capitalize())+' '+cierre_detalle['account_id'][1]+' '+str(clasificacion_dic[cierre_detalle['clasificacion']])+' '+str(cierre_detalle['descripcion'] if True else '')
                if cierre_detalle['tipo'] == 'ingreso':
                    detalle_cierre.append({descrip: cierre_detalle['monto']})
                    saldo_caja["efectivo"]+=cierre_detalle['monto']
                else:
                    #detalle_cierre.append(descrip)
                    #detalle_cierre.append({[str(cierre_detalle['tipo'])+' '+str(cierre_detalle['clasificacion'])+' '+str(cierre_detalle['descripcion'])]:(cierre_detalle['monto']*-1)})
                    detalle_cierre.append({descrip: cierre_detalle['monto']*-1})
                    saldo_caja["efectivo"]-=cierre_detalle['monto']
            detalle_cierre.append({'** Total ' + descrip_subtotal: subtotal})
            detalle_cierre.append({'*** Total ' : subtotal_total})
            total_ingreso_egreso=subtotal_total
        for name in detalle_cierre:
            print(name)
        Ventas={"Contado":0.0,"Credito":0.0}
        suma_dev=0.0
        for factura in facturas_recorset:
            es_devolucion = True
            es_nc = False
            for linea in factura.invoice_line_ids:
                if (linea.product_id.name == False):
                    suma_dev+=factura.amount_total
                    es_devolucion = False
                    es_nc = True
            if factura.payment_term_id.line_ids.days==0:
                if factura.type=="out_refund" and es_devolucion:
                    Ventas["Contado"]+=(factura.amount_total)*-1
                elif  not es_nc:
                    Ventas["Contado"]+=factura.amount_total
            else:
                if factura.type=="out_refund" and es_devolucion:
                    Ventas["Credito"]+=(factura.amount_total)*-1
                elif not es_nc:
                    Ventas["Credito"]+=factura.amount_total
        #print 'Suma Dev: '+str(suma_dev)
        datas['ventas']=Ventas
        #print "Ventas Contado"+"{:,}".format(Ventas["Contado"])
        #print "Ventas Credito"+"{:,}".format(Ventas["Contado"])
        #print datas

        forma_pagos = {}
        metodo_pago_recibo = {'Efectivo':0.0,'Tarjeta':0.0,'Transferencia – depósito bancario':0.0,'Cheque':0.0,'Recaudado por tercero':0.0,'Otros (se debe indicar el medio de pago)':0.0}
        metodo_pago_recibo_proveedor = {'Efectivo': 0.0, 'Tarjeta': 0.0, 'Transferencia – depósito bancario': 0.0, 'Cheque': 0.0,
                              'Recaudado por tercero': 0.0, 'Otros (se debe indicar el medio de pago)': 0.0}
        forma_pago_recorset = self.env['account.journal'].search([('code','!=','')])
        for fp_r in forma_pago_recorset:
            forma_pagos[fp_r.name] = 0.0
        recibos = {"Credito/Recuperacion":0.0,"Contado":0.0,"Abierto":0.0}
        recibos_recorset = self.env['account.payment'].search(domain_recibos)
        recibos_proveedor_recorset = self.env['account.payment'].search(domain_recibos_proveedor)
        recibos_proveedor_recorset_det=self.env['account.payment'].search_read(domain_recibos_proveedor, fields_recibo_proveedor,order='metodo_pago asc')
        recibos_recorset_sin_factura = self.env['account.payment'].search(domain_recibos_sin_factura)

        for rp in recibos_proveedor_recorset:
            if rp.metodo_pago:
                metodo_pago_recibo_proveedor[rp.metodo_pago]+=rp.amount
            else:
                raise UserError(_('El recibo no tiene forma de PAGO'+rp.name))


        d = dict.fromkeys(metodo_pago_recibo_proveedor)
        for key in d:
            if metodo_pago_recibo_proveedor[key] == 0.0:
                if key !='Efectivo':
                    del metodo_pago_recibo_proveedor[key]

        for r in recibos_recorset:
            metodo_pago_recibo[r.metodo_pago]+=r.amount

        d = dict.fromkeys(metodo_pago_recibo)
        for key in d:
            if metodo_pago_recibo[key] == 0.0:
                del metodo_pago_recibo[key]

        for recibo_sin_factura in recibos_recorset_sin_factura:
            if recibo_sin_factura.payment_type=="inbound":
                recibos["Abierto"] += recibo_sin_factura.amount
            else:
                recibos["Abierto"] -= recibo_sin_factura.amount
            if recibo_sin_factura.payment_type=="inbound":
                forma_pagos[recibo_sin_factura.journal_id.name]+=recibo_sin_factura.amount
            else:
                forma_pagos[recibo_sin_factura.journal_id.name]-=recibo_sin_factura.amount

        for recibo in recibos_recorset:
            if recibo.payment_type=="inbound":
                forma_pagos[recibo.journal_id.name]+=recibo.amount
            else:
                forma_pagos[recibo.journal_id.name]-=recibo.amount
            for rf in recibo.invoice_ids:
                #print 'id: '+str(rf.id)+' Factura:'+rf.number+'Tipo: '+ rf.type
                #print 'id: '+str(rf.id)+' Factura:'+rf.number
                if rf.type=="out_refund":
                    #print 'id: '+str(rf.id)+' Devolucion Dinero:'+rf.number
                    recibo.devolucion_dinero = True

                if rf.payment_term_id.line_ids.days==0:
                    recibo.cobro_credito = False
                else:
                    recibo.cobro_credito = True
            if recibo.cobro_credito:
                recibos["Credito/Recuperacion"]+=recibo.amount
            else:
                if recibo.payment_type=="inbound":
                    #print  'recibo'+str(recibo.id)
                    recibos["Contado"]+=recibo.amount
                else:
                    recibos["Contado"]-=recibo.amount

        d = dict.fromkeys(forma_pagos)
        for key in d:
            if forma_pagos[key]==0.0:
                del forma_pagos[key]

        for fp in forma_pagos:
            if fp=='Efectivo':
                saldo_caja['efectivo']+=forma_pagos[fp]
            if fp=='Devolucion':
                saldo_caja['efectivo']-=forma_pagos[fp]



        datas['forma_pagos']=forma_pagos
        datas['recibos']=recibos
        datas['detalle_cierre']=detalle_cierre

        s_d=0.0
        for mpr in metodo_pago_recibo:
            if mpr=='Efectivo':
                s_d+=metodo_pago_recibo[mpr]
        for mpp in metodo_pago_recibo_proveedor:
            if mpp=='Efectivo':
                s_d-=metodo_pago_recibo_proveedor[mpp]

        s_d+=total_ingreso_egreso
        saldo_caja['efectivo']=s_d
        datas['saldo_caja'] = saldo_caja
        #datas['saldo_efectivo']=(saldo_caja["efectivo"]-saldo_caja['saldo'])-(metodo_pago_recibo_proveedor['Efectivo'])
        datas['saldo_efectivo'] = moto_reportado-s_d
        datas['metodo_pago_recibo']=metodo_pago_recibo
        datas['metodo_pago_recibo_proveedor'] = metodo_pago_recibo_proveedor
        s = datas['form']['fecha_cierre']
        f = "%Y-%m-%d"
        titulo_reporte=''
        notas=''
        if s:
            fecha = datetime.strptime(s, '%Y-%m-%d')
            titulo_reporte = 'Cierre caja del '+fecha.strftime("%d/%m/%Y")
        else:
            titulo_reporte = 'Cierre caja ruta: ' + self.ruta +' del '
            if cierre_recorset.fecha_inicio:
                fecha_inicial = cierre_recorset.fecha_inicio
                fecha_final = cierre_recorset.fecha_cierre
                titulo_reporte += fecha_inicial.strftime("%d/%m/%Y") +' al '+fecha_final.strftime("%d/%m/%Y")
            else:
                raise UserError(_('Fecha de cierre no establecida'))
            notas=cierre_recorset.nota
        datas['notas'] = notas
        datas['titulo_reporte']=titulo_reporte
        print('**Ventas***')
        for f in facturas_ob:
            domain_cliente = [('id', '=', f['partner_id'][0])]
            fields_cliente = ['ref','name','id']
            clientes = self.env['res.partner'].search_read(domain_cliente, fields_cliente)
            print(clientes)
            f['ref_cliente'] = clientes[0]['ref']
            if f['type']=='out_invoice':
                f['tipo']='Factura'
            else:
                f['tipo']='Nota Credito'
                f['amount_untaxed']=f['amount_untaxed']*-1
                f['amount_tax'] = f['amount_tax'] * -1
                f['amount_total'] = f['amount_total'] * -1
                f['subtotal_exento'] = f['subtotal_exento'] * -1
                f['subtotal_gravado'] = f['subtotal_gravado'] * -1
            print(f)
        datas['facturas'] = facturas_ob
        for recibo_proveedor in recibos_proveedor_recorset_det:
            move_lines=self.env['account.move.line'].search([('id', 'in', recibo_proveedor['move_line_ids'])])
            for mv in move_lines:
                if mv.debit>0:
                    recibo_proveedor['CC']=mv.account_id.code+' '+mv.account_id.name

        datas['recibos_proveedor_det']=recibos_proveedor_recorset_det
        print('**Ventas***')

        factura_articulos=[]
        for fact_art in facturas_arts:
            for fl in fact_art['invoice_line_ids']:
                lineas_facturas = self.env['account.invoice.line'].search([('id','=',fl)])
                monto_transporte=0.0
                for linea in lineas_facturas:
                    for a in self.product_ids:
                        if linea.product_id.id == a.id:
                            factura_articulos.append(fact_art)
                            monto_transporte+=linea.price_unit*linea.quantity
            fact_art['monto_transporte']=monto_transporte

        for f in factura_articulos:
            if f['type'] == 'out_invoice':
                f['tipo'] = 'Factura'
            else:
                f['tipo'] = 'Nota Credito'

        '''
        for f in factura_articulos:
            if f['type'] == 'out_invoice':
                f['tipo'] = 'Factura'
            else:
                f['tipo'] = 'Nota Credito'
                f['amount_untaxed'] = f['amount_untaxed'] * -1
                f['amount_tax'] = f['amount_tax'] * -1
                f['amount_total'] = f['amount_total'] * -1
                f['subtotal_exento'] = f['subtotal_exento'] * -1
                f['subtotal_gravado'] = f['subtotal_gravado'] * -1
        '''
        factura_articulos_sin_duplicados = []
        for i in factura_articulos:
            if i not in factura_articulos_sin_duplicados:
                factura_articulos_sin_duplicados.append(i)

        datas['factura_articulos'] = factura_articulos_sin_duplicados

        fields_a = ['default_code', 'name']
        articulos=[]
        for a in self.product_ids:
            articulo=self.env['product.product'].search_read([('id', '=', a.id)],fields_a)
            articulos.append(articulo)

        datas['articulos'] = articulos
        return self.env.ref('aux_cr.action_report_cierre_diario').report_action([], data=datas)

class ParticularReport(models.AbstractModel):
    _name = 'report.aux_cr.cierre_diario_template'

    @api.model
    def _get_report_values(self, docids, data=None):
        data = data if data is not None else {}
        report_obj = self.env['ir.actions.report']
        report = report_obj._get_report_from_name('aux_cr.cierre_diario_template')
        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': self,
            'data': data,
        }
        return docargs


class WizarEstadoCuenta(models.TransientModel):
    _name="estado_cuenta.wizard"
    _description="Estado de cuenta"

    fecha_cierre = fields.Date("Fecha")
    id_cliente = partner_id = fields.Many2one('res.partner')

    @api.multi
    def action_report(self):
        """Metodo que llama la lógica que genera el reporte"""
        datas={'ids': self.env.context.get('active_ids', [])}
        res = self.read(['fecha_cierre'])
        res = self.read(['id_cliente'])
        res = res and res[0] or {}
        datas['form'] = res
        domain=[]
        domain_detalle_cierre=[]
        if self.fecha_cierre:
            domain=[('partner_id','=',self.id_cliente.id),('state','!=','draft')
            ,('type','in',['out_refund','out_invoice']),('recidual','>','0.0')]
            #domain_recibos=[('partner_type','=','customer'),('payment_date','=',self.fecha_cierre),('invoice_ids','!=',False),('state','!=','draft')]
            #domain_recibos_sin_factura=[('payment_date','=',self.fecha_cierre),('state','!=','draft'),('invoice_ids','=',False),('payment_type','in',['inbound'])]
        fields=['partner_id','payment_methods_id','number','date_invoice','amount_total','payment_term_id','type','residual','date_due','dias_vencida','intereses','saldo_intereses']
        facturas_ob = self.env['account.invoice'].search_read([('partner_id','=',self.id_cliente.id),('state','!=','draft'),('type','in',['out_refund','out_invoice']),('residual','>','0.0')],fields)
        #facturas_recorset = self.env['account.invoice'].search(domain)
        #cierre_recorset = self.env['cierre.caja'].search([('fecha_inicio', '=', self.fecha_cierre)])
        cliente = self.env['res.partner'].search_read([('id','=',self.id_cliente.id)],['name'])
        detalle_cierre={}

        titulo_reporte = 'Estado de cuenta'
        datas['titulo_reporte']=titulo_reporte
        datas['facturas'] = facturas_ob
        datas['cliente'] = cliente
        return self.env['report'].get_action([], 'estado_cuenta', data=datas)

class WizarAnalisVencimento(models.TransientModel):
    _name="analisis_vencimiento.wizard"
    _description="Analisis Vencimiento"

    fecha_corte = fields.Date("Fecha")
    #id_cliente = partner_id = fields.Many2one('res.partner')

    @api.multi
    def action_report(self):
        """Metodo que llama la lógica que genera el reporte"""
        datas={'ids': self.env.context.get('active_ids', [])}
        res = self.read(['fecha_corte'])
        #res = self.read(['id_cliente'])
        res = res and res[0] or {}
        datas['form'] = res
        domain=[]
        domain_detalle_cierre=[]


        if self.fecha_corte:
            domain=[('state','!=','draft')
            ,('type','in',['out_refund','out_invoice']),('recidual','>','0.0')]
            #domain_recibos=[('partner_type','=','customer'),('payment_date','=',self.fecha_cierre),('invoice_ids','!=',False),('state','!=','draft')]
            #domain_recibos_sin_factura=[('payment_date','=',self.fecha_cierre),('state','!=','draft'),('invoice_ids','=',False),('payment_type','in',['inbound'])]
        fields=['partner_id','number','date_invoice','amount_total','payment_term_id','type','residual','date_due','dias_vencida','intereses','saldo_intereses']
        facturas_ob = self.env['account.invoice'].search_read([('partner_id','=',self.id_cliente.id),('state','!=','draft'),('type','in',['out_refund','out_invoice']),('residual','>','0.0')],fields)
        #facturas_recorset = self.env['account.invoice'].search(domain)
        #cierre_recorset = self.env['cierre.caja'].search([('fecha_inicio', '=', self.fecha_cierre)])
        cliente = self.env['res.partner'].search_read([('id','=',self.id_cliente.id)],['name'])

        titulo_reporte = 'Analisis Vencimiento'
        datas['titulo_reporte'] = titulo_reporte
        datas['facturas'] = facturas_ob
        datas['cliente'] = cliente
        return self.env['report'].get_action([], 'estado_cuenta', data=datas)
