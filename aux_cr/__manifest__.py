# -*- coding: utf-8 -*-
{
    'name': "aux_cr",

    'summary': """
        Personalizaciones para costa rica""",

    'description': """
        Personalizaciones para Costa Rica
    """,

    'author': "Erick Venegas",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','account','sale','product','purchase','adaptaciones_cr','account_payment_show_invoice'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'security/security_view.xml',
        'views/views.xml',
        'views/templates.xml',
        'report/cierre_diario_template.xml',
        'report/estado_cuenta_template.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
