# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import datetime
from dateutil.relativedelta import relativedelta
from datetime import timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo.exceptions import UserError, RedirectWarning, ValidationError
from odoo.addons import decimal_precision as dp
from odoo.tools.float_utils import float_compare, float_round
from odoo import tools
from odoo.tools.safe_eval import safe_eval
import logging

MAP_INVOICE_TYPE_PARTNER_TYPE = {'out_invoice': 'customer', 'out_refund': 'customer', 'in_invoice': 'supplier',
                                 'in_refund': 'supplier', }
# Since invoice amounts are unsigned, this is how we know if money comes in or goes out
MAP_INVOICE_TYPE_PAYMENT_SIGN = {'out_invoice': 1, 'in_refund': 1, 'in_invoice': -1, 'out_refund': -1, }

_logger = logging.getLogger(__name__)


class Quant(models.Model):
    _inherit = "stock.quant"

    @api.model
    def quants_reserve(self, quants, move, link=False):
        ''' This function reserves quants for the given move and optionally
        given link. If the total of quantity reserved is enough, the move state
        is also set to 'assigned'

        :param quants: list of tuple(quant browse record or None, qty to reserve). If None is given as first tuple
        element, the item will be ignored. Negative quants should not be received as argument
        :param move: browse record
        :param link: browse record (stock.move.operation.link)
        '''
        # TDE CLEANME: use ids + quantities dict
        # TDE CLEANME: check use of sudo
        quants_to_reserve_sudo = self.env['stock.quant'].sudo()
        reserved_availability = move.reserved_availability
        nombre_poducto = move.name
        # split quants if needed
        for quant, qty in quants:
            if qty <= 0.0 or (quant and quant.qty <= 0.0):
                raise UserError(_('Actualizar la existencia del articulo: ' + str(nombre_poducto)))
            if not quant:
                continue
            quant._quant_split(qty)
            quants_to_reserve_sudo |= quant
            reserved_availability += quant.qty
        # reserve quants
        if quants_to_reserve_sudo:
            quants_to_reserve_sudo.write({'reservation_id': move.id})
        # check if move state needs to be set as 'assigned'
        # TDE CLEANME: should be moved as a move model method IMO
        rounding = move.product_id.uom_id.rounding
        if float_compare(reserved_availability, move.product_qty, precision_rounding=rounding) == 0 and move.state in (
                'confirmed', 'waiting'):
            move.write({'state': 'assigned'})
        elif float_compare(reserved_availability, 0, precision_rounding=rounding) > 0 and not move.partially_available:
            move.write({'partially_available': True})


class ResPartner(models.Model):
    _inherit = 'res.partner'
    cod_cliente = fields.Char('Codigo', index=True, required=False, translate=False, default='/')
    # codigo = fields.Char('Cod', index=True, required=False, translate=False, default='/')
    cedula = fields.Char('cedula', index=True, required=False, translate=False)

    '''
    @api.multi
    def name_get(self):
        result = []
        for record in self:
            if record.property_payment_term_id == False:
                name = '[%s] %s FALTA CONDICION PAGO!!!' % (record.cod_cliente, record.name)
            else:
                name = '[%s] %s %s' % (record.cod_cliente, record.name, record.property_payment_term_id.name)
            result.append((record.id, name))
        return result
    '''

class descuento_proveedor(models.Model):
    _inherit = 'purchase.order.line'
    por_descuento = fields.Float(readonly=False, store=True, string="% Descuento")
    precio_unit_sin_desc = fields.Float(readonly=False, store=True, string="Precio sin Descuento")

    @api.onchange('por_descuento', 'precio_unit_sin_desc')
    def _descuento_calculo(self):
        descuento = float(self.por_descuento) / 100
        preciosindescuento = float(self.precio_unit_sin_desc)
        resultado = preciosindescuento - (preciosindescuento * descuento)
        self.price_unit = float(resultado)


class ProductTemplate(models.Model):
    _inherit = 'product.template'
    ubicacion = fields.Char('Ubicacion', index=True, required=False, translate=False)
    notas = fields.Char('Notas', index=True, required=False, translate=False)
    margen_costo = fields.Float(string='% Utilidad', digits=dp.get_precision('margen_articulo'))
    precio_ivi = fields.Float(readonly=False, store=True, string="Precio IVI")
    rma_stock_used = fields.Integer(string='Cant Reparaciones', compute='_compute_rma_stock_used')
    type = fields.Selection([('consu', _('Consumable')), ('service', _('Service')), ('product', 'Stockable Product')],
                            string='Product Type', default='product', required=True,
                            help='A stockable product is a product for which you manage stock. The "Inventory" app '
                                 'has to be installed.\n'
                                 'A consumable product, on the other hand, is a product for which stock is not '
                                 'managed.\n'
                                 'A service is a non-material product you provide.\n'
                                 'A digital content is a non-material product you sell online. The files attached to '
                                 'the products are the '
                                 'one that are sold on '
                                 'the e-commerce such as e-books, music, pictures,... The "Digital Product" module '
                                 'has to be installed.')
    _sql_constraints = [('default_code_uniq', 'unique (default_code)', "Código articulo ya existe"), ]

    @api.one
    def _compute_rma_stock_used(self):
        obj = self.env['mrp.repair.fee']
        lines = obj.search([('product_id', '=', self.product_variant_ids.id),
                            ('repair_id.state', 'in', ['confirmed', 'under_repair', 'ready', '2binvoiced'])])
        qty = 0
        for a in lines:
            qty += a.product_uom_qty
        self.rma_stock_used = qty

    # @api.onchange('list_price', 'standard_price')
    @api.onchange('list_price', 'standard_price', )
    def _margen(self):
        if (self.standard_price == 0.0):
            pass
        else:
            utilidad = (self.list_price - self.standard_price) / self.standard_price
            self.margen_costo = (utilidad * 100)
            self.precio_ivi = round(((self.taxes_id.amount / 100) * self.list_price) + self.list_price, 2)

    @api.onchange('margen_costo')
    def _venta(self):
        # import pudb; pudb.set_trace()
        if (self.standard_price == 0.0):
            pass
        else:
            venta = 0.0
            venta = round((self.margen_costo / 100) * self.standard_price, 5)
            venta += self.standard_price
            self.list_price = venta
            self.precio_ivi = round(((self.taxes_id.amount / 100) * self.list_price) + self.list_price, 2)
            if self.margen_costo < self.company_id.margen_minimo:
                raise UserError(_('Muy bajo margen'))

    @api.onchange('precio_ivi')
    def _precio_ivi(self):
        if (self.standard_price == 0.0):
            pass
        else:
            venta = round((self.precio_ivi / (1 + (self.taxes_id.amount / 100))), 5)
            self.list_price = venta
            self.margen_costo = ((venta - self.standard_price) / self.standard_price) * 100

    '''
    @api.onchange('company_id', 'default_code', 'active')
    def check_unique_company_and_default_code(self):
        if self.active and self.default_code and self.company_id:
            filters = [('company_id', '=', self.company_id.id), ('default_code', '=', self.default_code),
                       ('active', '=', True)]
            prod_ids = self.search(filters)
            if len(prod_ids) > 1:
                raise Warning(_('Este código ya existe en el sistema.'))
    '''

    # @api.constrains('standard_price')  # def customvalidation(self):  #    if self.standard_price==0.0:  #  # raise
    # Warning(_('Margen bajo'))

    # @api.model  # def create(self, vals):  #    if self.standard_price==0.0:  #        raise UserError(_('Falta  #
    # costo'))  #    return super(ProductTemplate, self).create(vals)

    # @api.multi  # def write(self, vals):  #    if self.standard_price == 0.0 or self.margen_costo == 0.0:  #  #
    # raise UserError(_('Muy bajo margen'))  #   return super(ProductTemplate, self).create(vals)


class ResCompany(models.Model):
    _inherit = 'res.company'
    default_partner = fields.Many2one('res.partner', string='Cliente')
    default_payment = fields.Many2one('account.journal', string='Payment Journal',
                                      domain=[('type', 'in', ('bank', 'cash'))])
    margen_minimo = fields.Float(string='Margen minimo')


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    gasto = fields.Boolean(string='Factura Gasto', default=False)

    '''
    partner_id = fields.Many2one('res.partner', string='Partner', change_default=True, required=True, readonly=True,
                                 states={'draft': [('readonly', False)]}, track_visibility='always',
                                 default=lambda self: self._get_default_partner())
    '''
    '''
    payment_term_id = fields.Many2one('account.payment.term', string='Payment Terms', required=True, readonly=False,
                                      states={}, oldname='payment_term',
                                      help="If you use payment terms, the due date will be computed automatically at "
                                           "the generation "
                                           "of accounting entries. If you keep the payment term and the due date "
                                           "empty, it means direct payment. "
                                           "The payment term may compute several due dates, for example 50% now, "
                                           "50% in one month.")
                                           '''
    user_id = fields.Many2one('res.users', string='Salesperson', track_visibility='onchange',states={'draft': [('readonly', False)]}, required=True,copy=True)
    phone = fields.Char('Telefono', compute="_telefono", required=False, translate=False)
    dias_vencida = fields.Integer('Vencimiento', compute="_dias_vencida", required=False, translate=False)
    intereses = fields.Float(string='Monto Interes', digits=0, compute="_interes", required=False, translate=False)
    saldo_intereses = fields.Float(string='Saldo + Interes', digits=0, compute="_saldo", required=False,
                                   translate=False)
    total_mercaderia = fields.Float(string='Total Mercaderia', digits=0, compute="_mercaderia", required=False,
                                    translate=False)
    nombre_cliente = fields.Char(readonly=False, store=True, compute="_nombre_cliente", string="Nombre")
    subtotal_exento = fields.Float(compute="_subtotal_exento", string="SubTotal Exento")
    subtotal_gravado = fields.Float(compute="_subtotal_gravado", string="SubTotal Gravado")
    '''
    @api.model
    def _get_default_partner(self):
        return self.env.user.company_id.default_partner.id

   
    @api.model
    def create(self, values):
        # Override the original create function for the res.partner model
        record = super(AccountInvoice, self).create(values)

        if self.type == 'out_invoice':
            for detalle_forma_pago_cliente in self.partner_id.property_payment_term_id.line_ids:
                for forma_pago_factura in self.payment_term_id.line_ids:
                    if forma_pago_factura.days > detalle_forma_pago_cliente.days:
                        raise UserError(_('No se puede cambiar la condicion de pago'))
        else:
            for detalle_forma_pago_cliente in self.partner_id.property_payment_term_id.line_ids:
                for forma_pago_factura in self.payment_term_id.line_ids:
                    forma_pago_factura = detalle_forma_pago_cliente
    '''
    # Change the values of a variable in this super function
    # record['passed_override_write_function'] = True
    # print 'Passed this function. passed_override_write_function value: ' + str(record[
    # 'passed_override_write_function'])

    # Return the record so that the changes are applied and everything is stored.
    # return record

    '''
    @api.multi
    def write(self, values):
        record = super(AccountInvoice, self).write(values)
        for factura in self:
            if factura.type == 'out_invoice':
                for detalle_forma_pago_cliente in factura.partner_id.property_payment_term_id.line_ids:
                    for forma_pago_factura in factura.payment_term_id.line_ids:
                        if forma_pago_factura.days > detalle_forma_pago_cliente.days:
                            raise UserError(_('No se puede cambiar la condicion de pago'))
        return record
    '''

    @api.one
    def _subtotal_exento(self):
        exento = 0.0
        for linea in self.invoice_line_ids:
            for impuesto in linea.invoice_line_tax_ids:
                if impuesto.amount == 0.0:
                    exento += linea.price_subtotal
            if not linea.invoice_line_tax_ids:
                exento += linea.price_subtotal
        self.subtotal_exento = exento

    # exento=0.0
    # for linea in self.invoice_line_ids:
    #	for impuesto in linea.invoice_line_tax_ids:
    #		if impuesto.amount==0.0:
    #			exento+=linea.price_subtotal
    #	if not linea.invoice_line_tax_ids:
    #		exento+=linea.price_subtotal
    @api.one
    def _subtotal_gravado(self):
        gravado = 0.0
        for linea in self.invoice_line_ids:
            # if not (linea.invoice_line_tax_ids==False):
            for impuesto in linea.invoice_line_tax_ids:
                if impuesto.amount > 0.0:
                    gravado += linea.price_subtotal
        # if self.type in ('out_refund','in_refund'):
        #	gravado=exento*-1
        self.subtotal_gravado = gravado

    @api.one
    def _telefono(self):
        self.phone = self.partner_id.phone

    @api.one
    def _dias_vencida(self):
        pass
        '''
        # d_frm_obj = datetime.strptime(fields.Datetime.now(), DF)
        if self.state not in ['draft'] and self.residual > 0.0:
            d_frm_obj = fields.Datetime.from_string(fields.Datetime.now())
            d_to_obj = datetime.strptime(self.date_invoice, DF)
            if not (self.date_due == False):
                d_to_obj = datetime.strptime(self.date_due, DF)
            duration = d_frm_obj - d_to_obj
            self.dias_vencida = duration.days
        '''

    # _logger.error('Factura:'+self.number+' Estado:'+self.state+' dias'+str(duration.days))
    # self.dias_vencida=duration

    @api.one
    def _interes(self):
        pass

        # d_frm_obj = datetime.strptime(fields.Datetime.now(), DF)
        '''
        if self.state not in ['draft'] and self.residual > 0.0 and self.partner_id.customer:
            d_frm_obj = fields.Datetime.from_string(fields.Datetime.now())
            d_to_obj = datetime.strptime(self.date_invoice, DF)
            if not (self.date_due == False):
                d_to_obj = datetime.strptime(self.date_due, DF)
            duration = d_frm_obj - d_to_obj
            dias_vencidos = duration.days
            if dias_vencidos > 0:
                # self.intereses=(self.residual*0,015)*dias_vencidos
                monto_intereses = (self.residual * 0.0015) * dias_vencidos
                self.intereses = monto_intereses  # _logger.error('Factura:'+self.number+' Estado:'+self.state+'
                # Intereses:'+str(monto_intereses))
        '''

    @api.one
    def _saldo(self):
        if self.state not in ['draft'] and self.residual > 0.0 and self.intereses > 0.0:
            self.saldo_intereses = self.intereses + self.residual

    @api.one
    def _mercaderia(self):
        # logger.critical("The name '" + str(record.get('name')) + "' is not okay for us!")
        t_m = 0.0
        articulos = self.invoice_line_ids
        for a in articulos:
            # _logger.critical("Producto: " + str(a.name)+" Type: " +str(a.product_id.type))
            if a.product_id.type in ['product']:
                t_m += a.price_subtotal
        self.total_mercaderia = t_m

    '''
    @api.multi
    def invoice_validate(self):
        product_obj = self.env['product.product']
        # call the super methd here
        res = super(AccountInvoice, self).action_invoice_open()
        for invoice in self:
            if invoice.type in ('in_invoice', 'in_refund'):
                for line in invoice.invoice_line_ids:
                    if line.product_id and line.price_unit:
                        pr_id = product_obj.browse(line.product_id.id)
                        pr_id.write({'standard_price': (line.price_unit - (line.price_unit * (
                                    line.discount / 100)))})  # pr_id.write({'margen_costo': (
                        # line.margen_venta_nuevo)})  # pr_id.write({'list_price': (line.precio_venta_sin_impuesto)})
                        # pr_id.write({'pricio_ivi': (line.precio_venta_compra)}
            if invoice.type in ('in_invoice'):
                for line in invoice.invoice_line_ids:
                    if line.product_id and line.price_unit:
                        pr_id = product_obj.browse(line.product_id.id)
                        pr_id.write({'margen_costo': (line.margen_venta_nuevo)})
                        pr_id.write({'list_price': (line.precio_venta_sin_impuesto)})
                        pr_id.write({'precio_ivi': (line.precio_venta_compra)})

        # return self.write({'state':'open'})
        return res
        '''
    '''
    @api.multi
    def check_limit(self):

        """Check if credit limit for partner was exceeded."""
        self.ensure_one()
        self.sudo()
        if self.type == 'out_invoice':

            # _logger.error('Dias: '+str(self.partner_id.property_payment_term_id.line_ids.days))
            # _logger.error(str(datetime.strftime(datetime.now().date(), DF)))
            dias = self.partner_id.property_payment_term_id.line_ids.days
            if (dias > 0):
                partner = self.partner_id
                today_dt = datetime.strftime(datetime.now().date(), DF)
                cant_facturas_vencidas = 0
                facturas_obj = self.env['account.invoice']
                facturas = facturas_obj.search([('partner_id', '=', partner.id), ('state', 'in', ['open'])])
                for f in facturas:
                    if f.date_due <= today_dt:
                        cant_facturas_vencidas += 1

                if not partner.over_credit and cant_facturas_vencidas > 0:
                    msg = 'No se puede confirmar la orden'
                    raise UserError(_('Cliente tiene facturas vencidas!\n' + msg))

                # _logger.error('Cantidad Facturas Vencidas '+str(cant_facturas_vencidas))
                moveline_obj = self.env['account.move.line']
                movelines = moveline_obj.search(
                    [('partner_id', '=', partner.id), ('account_id.user_type_id.type', 'in', ['receivable', 'payable']),
                     ('full_reconcile_id', '=', False)])

                debit, credit = 0.0, 0.0

                for line in movelines:
                    if line.date_maturity < today_dt:
                        credit += line.debit
                        debit += line.credit

                if (credit - debit + self.amount_total) > partner.credit_limit:
                    # Consider partners who are under a company.
                    if partner.over_credit or (partner.parent_id and partner.parent_id.over_credit):
                        partner.write({'credit_limit': credit - debit + self.amount_total})
                        return True
                    else:
                        msg = 'No se puede confirmar la orden'
                        raise UserError(_('Limite de crédito excedido!\n' + msg))
                else:
                    return True
            else:
                return True
        '''

    @api.multi
    def verifica_margen(self):
        self.ensure_one()
        self.sudo()
        if self.type == 'out_invoice':
            margen = float(self.margin_percentage.replace("%", ""))
            if margen <= self.company_id.margen_minimo:
                raise ValidationError(_('Muy poco margen revisar! '))

    '''
    @api.multi
    def action_invoice_open(self):
        for factura in self:
            factura.verifica_margen()
            factura.check_limit()
        return super(AccountInvoice, self).action_invoice_open()
    '''


"""
    @api.multi
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        for order in self:
            order.check_limit()
        return super(AccountInvoice, self).invoice_validate()
"""

class SaleOrder(models.Model):
    _inherit = "sale.order"
    user_id = fields.Many2one('res.users', string='Salesperson', track_visibility='onchange', readonly=True,
                              states={'draft': [('readonly', False)]}, default=lambda self: False)
    nombre_cliente = fields.Char(readonly=False, store=True, string="Nombre")


    """
    @api.onchange('user_id','partner_id') # if these fields are changed, call method
    def check_change(self):sale.orde
        if not(len(self.user_id) > 0) and (len(self.partner_id)>0):
            return{
                'warning':{
                'title':'Asignar Vendedor!',
                'message':''
                }
            }
            
    @api.multi
    def action_confirm(self):
        res = super(SaleOrder, self).action_confirm()
        try:
            for order in self:
                if not(len(order.user_id) > 0) and (len(order.partner_id)>0):
                    error=0/0
        except:
            raise ValidationError('Falta Vendedor')
        return res
    @api.onchange('payment_term_id')
    def cambio_condicion(self):
        if not self.partner_id.property_payment_term_id==self.payment_term_id:
            try:
                error=0/0
            except Exception as e:
                raise ValidationError('No se puede cambiar la condicion de pago')

    @api.multi
    def _prepare_invoice(self):
        
        #Prepare the dict of values to create the new invoice for a sales order. This method may be
        #overridden to implement custom invoice generation (making sure to call super() to establish
        #a clean extension chain).
        
        self.ensure_one()
        journal_id = self.env['account.invoice'].default_get(['journal_id'])['journal_id']
        if not journal_id:
            raise UserError(_('Please define an accounting sale journal for this company.'))
        invoice_vals = {
            'name': self.client_order_ref or '',
            'origin': self.name,
            'type': 'out_invoice',
            'account_id': self.partner_invoice_id.property_account_receivable_id.id,
            'partner_id': self.partner_invoice_id.id,
            'partner_shipping_id': self.partner_shipping_id.id,
            'journal_id': journal_id,
            'currency_id': self.pricelist_id.currency_id.id,
            'comment': self.note,
            'payment_term_id': self.payment_term_id.id,
            'fiscal_position_id': self.fiscal_position_id.id or self.partner_invoice_id.property_account_position_id.id,
            'company_id': self.company_id.id,
            'user_id': self.user_id and self.user_id.id,
            'team_id': self.team_id.id,
            'nombre_cliente': self.nombre_cliente
        }
        return invoice_vals
    
    """

class account_payment(models.Model):
    _inherit = "account.payment"  # account.payment
    intereses = fields.Float(string='Monto Interes', digits=0, required=False, translate=False)
    saldo = fields.Float(string='Saldo', digits=0, required=False, translate=False)
    pago_con = fields.Float(string='Pago con', digits=0, required=False, translate=False)
    vuelto = fields.Float(string='Vuelto', digits=0, required=False, translate=False)
    intereses_revisado = fields.Boolean(String='Intereses revisados');

    journal_id = fields.Many2one('account.journal', string='Payment Journal', required=True,
                                 domain=[('type', 'in', ('bank', 'cash'))],
                                 default=lambda self: self._get_default_payment())

    @api.model
    def _get_default_payment(self):
        return self.env.user.company_id.default_payment.id

    @api.onchange('intereses')
    def _recalculo_interes(self):
        i = float(self.intereses + self.amount)
        self.saldo = float(i)

    @api.onchange('pago_con')
    def _vuelto(self):
        vuelto = float(self.pago_con - self.amount)
        self.vuelto = float(vuelto)

    @api.model
    def default_get(self, fields):
        rec = super(account_payment, self).default_get(fields)
        invoice_defaults = self.resolve_2many_commands('invoice_ids', rec.get('invoice_ids'))
        if invoice_defaults and len(invoice_defaults) == 1:
            invoice = invoice_defaults[0]
            rec['communication'] = invoice['reference'] or invoice['name'] or invoice['number']
            rec['currency_id'] = invoice['currency_id'][0]
            rec['payment_type'] = invoice['type'] in ('out_invoice', 'in_refund') and 'inbound' or 'outbound'
            rec['partner_type'] = MAP_INVOICE_TYPE_PARTNER_TYPE[invoice['type']]
            rec['partner_id'] = invoice['partner_id'][0]
            # rec['amount'] = invoice['residual']+invoice['intereses']
            rec['amount'] = invoice['residual']
            rec['intereses'] = invoice['intereses']
            rec['saldo'] = invoice['residual'] + invoice['intereses']
        return rec


class ResCompany(models.Model):
    _inherit = 'res.company'
    maximo_descuento = fields.Float(readonly=False, store=True, string="Descuento máximo")

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    indicaciones = fields.Char('Indicaciones', required=False)

    @api.multi
    def _prepare_invoice_line(self, qty):
        """
        Prepare the dict of values to create the new invoice line for a sales order line.

        :param qty: float quantity to invoice
        """
        self.ensure_one()
        res = {}
        account = self.product_id.property_account_income_id or \
                  self.product_id.categ_id.property_account_income_categ_id

        if not account and self.product_id:
            raise UserError(
                _('Please define income account for this product: "%s" (id:%d) - or for its category: "%s".') % (
                self.product_id.name, self.product_id.id, self.product_id.categ_id.name))

        fpos = self.order_id.fiscal_position_id or self.order_id.partner_id.property_account_position_id
        if fpos and account:
            account = fpos.map_account(account)

        res = {'name': self.name, 'sequence': self.sequence, 'origin': self.order_id.name, 'account_id': account.id,
            'price_unit': self.price_unit, 'quantity': qty, 'discount': self.discount, 'uom_id': self.product_uom.id,
            'product_id': self.product_id.id or False, 'invoice_line_tax_ids': [(6, 0, self.tax_id.ids)],
            'account_analytic_id': self.order_id.analytic_account_id.id,
            'analytic_tag_ids': [(6, 0, self.analytic_tag_ids.ids)], 'display_type': self.display_type,
            'indicaciones': self.indicaciones, }
        return res



class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    indicaciones = fields.Char('Indicaciones', required=False)


    @api.onchange('discount')
    def verifica_descuento_categoria(self):
        if self.product_id:
            if self.product_id.categ_id==False:
                raise ValidationError("Producto %s no tiene categoria " % (self.product_id.name,))
            else:
                discount=self.product_id.categ_id.discount
                if self.discount>discount:
                    raise ValidationError("Producto %s tiene un descuento no permitido " % (self.product_id.name,))
    '''
    precio_ivi = fields.Float('Precio IVI', store=True, readonly=True, digits=dp.get_precision('Product Price'),
                              compute='_precioivi')
    
    
    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_ids', 'product_qty', 'product_id')
    def _precioivi(self):
        precio_impuesto = 0.0
        for impuestos_linea in self.invoice_line_tax_ids:
            for imp in impuestos_linea:
                if self.discount > 0.0:
                    precio_impuesto = self.quantity * ((self.price_unit - (self.price_unit * (self.discount / 100))) * (
                            ((imp.amount) / 100) + 1))
                else:
                    precio_impuesto = self.quantity * ((((imp.amount) / 100) + 1) * self.price_unit)
        self.precio_ivi = precio_impuesto
'''

class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'
    ubicacion = fields.Char('Ubicacion', required=False, translate=False, compute='_ubicacion')
    price_unit_2 = fields.Float('Precio Unitario', store=True, readonly=True, digits=dp.get_precision('Product Price'),
                                compute='_compute_price_unit_2')
    precio_ivi = fields.Float('Precio IVI', store=True, readonly=True, digits=dp.get_precision('Product Price'),
                              compute='_precioivi')
    invoice_line_tax_ids_2 = fields.Many2many('account.tax', 'account_invoice_line_tax', 'invoice_line_id', 'tax_id',
                                              string='Imp', compute='_imp', readonly=True)
    existencia = fields.Float('Existencia', readonly=True, compute='_existencia')

    precio_venta_compra = fields.Float('PU Venta')
    margen_venta_nuevo = fields.Float(string='% Utilidad', digits=dp.get_precision('margen_articulo'))
    precio_venta_sin_impuesto = fields.Float('Precio Venta sin Impuesto')
    indicaciones = fields.Char('Indicaciones', required=False)

    @api.model
    def _prepare_invoice_line(self):
        data = {
            'name': self.name,
            'origin': self.origin,
            'uom_id': self.uom_id.id,
            'product_id': self.product_id.id,
            'account_id': self.account_id.id,
            'price_unit': self.price_unit,
            'quantity': self.quantity,
            'discount': self.discount,
            'account_analytic_id': self.account_analytic_id.id,
            'analytic_tag_ids': self.analytic_tag_ids.ids,
            'invoice_line_tax_ids': self.invoice_line_tax_ids.ids,
            'indicaciones': self.indicaciones
        }
        return data

    @api.onchange('product_id')
    def _precio_venta_nuevo(self):
        pass
        '''
        if self.invoice_id.type == 'in_invoice':
            if not (self.product_id.id == False):
                product = self.env['product.product'].search([['id', '=', self.product_id.id]])
                self.precio_venta_compra = product.precio_ivi
                margen = 0.0
                if product.taxes_id.amount > 0:
                    costo_unitario = (self.price_subtotal / self.quantity)
                    precio_sin_impuesto = product.precio_ivi / (1 + (product.taxes_id.amount / 100))
                    self.precio_venta_sin_impuesto = precio_sin_impuesto
                    margen = ((precio_sin_impuesto - costo_unitario) / costo_unitario) * 100
                    if margen < self.company_id.margen_minimo:
                        raise UserError(_('Muy bajo margen'))
                    self.margen_venta_nuevo = margen
                else:
                    costo_unitario = (self.price_subtotal / self.quantity)
                    precio_sin_impuesto = product.precio_ivi
                    self.precio_venta_sin_impuesto = precio_sin_impuesto
                    margen = ((precio_sin_impuesto - costo_unitario) / costo_unitario) * 100
                    if margen < self.company_id.margen_minimo:
                        raise UserError(_('Muy bajo margen'))
                    self.margen_venta_nuevo = margen
        '''

    @api.onchange('margen_venta_nuevo')
    def _precio_margen(self):
        pass
        '''
        if self.invoice_id.type == 'in_invoice':
            if not (self.product_id.id == False):
                product = self.env['product.product'].search([['id', '=', self.product_id.id]])
                # self.precio_venta_compra = product.precio_ivi
                margen = self.margen_venta_nuevo
                if margen < self.company_id.margen_minimo:
                    raise UserError(_('Muy bajo margen'))
                else:
                    # if product.taxes_id.amount > 0:
                    costo_unitario = (self.price_subtotal / self.quantity)
                    precio_sin_impuesto = (costo_unitario * (margen / 100)) + costo_unitario
                    self.precio_venta_sin_impuesto = precio_sin_impuesto
                    precio_con_impuesto = (precio_sin_impuesto * (product.taxes_id.amount / 100)) + precio_sin_impuesto
                    self.precio_venta_compra = precio_con_impuesto  # else:  #    costo_unitario = (
                    # self.price_subtotal / self.quantity)  #    precio_sin_impuesto = (costo_unitario * (margen /
                    # 100)) + costo_unitario  #    self.precio_venta_sin_impuesto = precio_sin_impuesto  #
                    # self.precio_venta_compra = precio_sin_impuesto
        '''

    @api.onchange('precio_venta_compra')
    def _precio_venta_compra(self):
        pass
        '''
        if self.invoice_id.type == 'in_invoice':
            if not (self.product_id.id == False):
                product = self.env['product.product'].search([['id', '=', self.product_id.id]])
                # self.precio_venta_compra = product.precio_ivi
                margen = 0.0
                if product.taxes_id.amount > 0:
                    costo_unitario = (self.price_subtotal / self.quantity)
                    precio_sin_impuesto = self.precio_venta_compra / (1 + (product.taxes_id.amount / 100))
                    self.precio_venta_sin_impuesto = precio_sin_impuesto
                    margen = ((precio_sin_impuesto - costo_unitario) / costo_unitario) * 100
                    if margen < self.company_id.margen_minimo:
                        raise UserError(_('Muy bajo margen'))
                    self.margen_venta_nuevo = margen
                else:
                    costo_unitario = (self.price_subtotal / self.quantity)
                    precio_sin_impuesto = self.precio_venta_compra
                    # precio_sin_impuesto = product.precio_ivi
                    self.precio_venta_sin_impuesto = precio_sin_impuesto
                    margen = ((precio_sin_impuesto - costo_unitario) / costo_unitario) * 100
                    if margen < self.company_id.margen_minimo:
                        raise UserError(_('Muy bajo margen'))
                    self.margen_venta_nuevo = margen
        '''  # venta = 0.0  # venta = round((self.margen_costo / 100) * self.standard_price, 5)  # venta +=   #  #
        # self.standard_price  # self.list_price = venta  # self.precio_ivi = round(((product.taxes_id.amount /  #
        # 100) * product.list_price) + product.list_price, 2)  # if self.margen_costo <  #
        # self.company_id.margen_minimo:  #    raise UserError(_('Muy bajo margen'))

    @api.model
    def _margen_nuevo(self):
        return 0.0

    '''
    @api.one
    @api.constrains('product_id', 'invoice_id')
    def _check_unique_constraint(self):
        if self.invoice_id.type == 'out_invoice':
            if len(self.search([('invoice_id', '=', self.invoice_id.id), ('product_id', '=', self.product_id.id)])) > 1:
                raise ValidationError("Producto %s ya existe en la factura " % (self.product_id.name,))
    '''

    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_ids', 'quantity', 'product_id', 'invoice_id.partner_id',
                 'invoice_id.currency_id', 'invoice_id.company_id', 'invoice_id.date_invoice')
    def _existencia(self):
        pass
        '''
        if not (self.product_id == False):
            self.existencia = self.product_id.qty_available
        else:
            0.0
        '''

    # precio_venta_compra = fields.Float(string="PU Venta",states={'draft': [('readonly', False)]})

    # @api.one
    # @api.onchange('precio_venta_compra')
    # @api.constrains('precio_venta_compra')
    # def _actualiza_precio(self):
    #    if self.invoice_id.type in ('in_invoice','in_refund'):
    # articulo=self.env['product.template'].search(['id', '=', self.product_id.id])
    #        print self.product_id.precio_ivi

    @api.one
    @api.depends('price_unit', 'discount', 'quantity', 'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id',
                 'invoice_id.company_id', 'invoice_id.date_invoice')
    def _ubicacion(self):
        pass  # self.ubicacion = self.product_id.ubicacion

    @api.one
    @api.depends('price_unit', 'discount', 'quantity', 'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id',
                 'invoice_id.company_id', 'invoice_id.date_invoice')
    def _compute_price_unit_2(self):
        pass  # self.price_unit_2 = self.price_unit

    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_ids', 'quantity', 'product_id', 'invoice_id.partner_id',
                 'invoice_id.currency_id', 'invoice_id.company_id', 'invoice_id.date_invoice')
    def _imp(self):
        pass  # self.invoice_line_tax_ids_2 = self.invoice_line_tax_ids

    @api.one
    @api.depends('price_unit', 'discount', 'invoice_line_tax_ids', 'quantity', 'product_id', 'invoice_id.partner_id',
                 'invoice_id.currency_id', 'invoice_id.company_id', 'invoice_id.date_invoice')
    def _precioivi(self):
        precio_impuesto = 0.0
        for impuestos_linea in self.invoice_line_tax_ids:
            for imp in impuestos_linea:
                if self.discount > 0.0:
                    precio_impuesto = self.quantity * ((self.price_unit - (self.price_unit * (self.discount / 100))) * (
                            ((imp.amount) / 100) + 1))
                else:
                    precio_impuesto = self.quantity * ((((imp.amount) / 100) + 1) * self.price_unit)
        self.precio_ivi = precio_impuesto

    @api.onchange('discount')
    @api.constrains('discount')
    def verif_descuento(self):
        pass
        '''
        if self.invoice_id.type not in ('in_invoice', 'in_refund'):
            descuento_maximo = self.company_id.maximo_descuento
            descuento_maximo = 25.0
            for line in self:
                descuento = line.discount
                if descuento > descuento_maximo:
                    line.discount = 0.0
                    raise ValidationError('El articulo: ' + line.display_name + '\n tiene un descuento no permitido')
        '''


class RepRecibos(models.Model):
    _name = "recibos.report"
    _description = "Reporte Recibos"
    _auto = False
    _table = "recibos_diarios"
    fecha = fields.Date("Fecha", readonly=True)
    numero = fields.Char("Numero", readonly=True)
    cliente = fields.Char("Cliente", readonly=True)
    monto = fields.Float('Monto', readonly=True)
    formapago = fields.Char("Forma Pago", readonly=True)

    def _select(self):
        select_str = """
			select
	ROW_NUMBER() OVER (ORDER BY p.payment_date desc) id
	,p.payment_date fecha
	,p.name numero
	,(select name from res_partner where res_partner.id=p.partner_id) cliente
	,p.amount monto
	,(
	select
		MAX(coalesce((
		select
			'Dias '||(pl.days)
		from
			account_payment_term p inner join account_payment_term_line pl
			on p.id=pl.payment_id
		where
			p.id=f.payment_term_id
		),'SN')) formapago
	from
	account_invoice_payment_rel fr
		inner join account_invoice f
			on f.id=fr.invoice_id
	where
		fr.payment_id=p.id
	)formapago
		"""
        return select_str

    def _from(self):
        from_str = """
			account_payment p
		"""
        return from_str

    def _where(self):
        where_str = """
			where
			p.partner_type='customer'
			and p.state<>'draft'
			and p.payment_date>='2017-12-18'
		"""
        return where_str

    @api.model_cr
    def init(self):
        # self._table = sale_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
			%s
			FROM  %s
			%s
			)""" % (self._table, self._select(), self._from(), self._where()))


class RepVentas(models.Model):
    _name = "ventas.report"
    _description = "Reporte Ventas Diarias"
    _auto = False
    _table = "ventas_diarias"
    # _rec_name = 'date'
    # _order = 'date desc'
    tipo_documento = fields.Char("Tipo Documento", readonly=True)
    fecha = fields.Date("Fecha", readonly=True)
    formapago = fields.Char("Forma Pago", readonly=True)
    vendedor = fields.Char("Vendedor", readonly=True)
    subtotalexento = fields.Float('SubTotal Exento', readonly=True)
    montodescuentoexento = fields.Float('Descuento Exento', readonly=True)
    subtotalgravado = fields.Float('SubTotal Gravado', readonly=True)
    montodescuentogravado = fields.Float('Descuento Gravado', readonly=True)
    subtotal = fields.Float('Sub Venta', readonly=True)
    impuesto = fields.Float('Monto Impuesto', readonly=True)
    total = fields.Float('Total', readonly=True)

    def _select(self):
        select_str = """
			select
				ROW_NUMBER() OVER (ORDER BY fecha desc,formapago asc) id
				,v.fecha
				,v.tipo_documento
				,v.vendedor
				,coalesce(v.formapago,'SN') formapago
				,sum(v.subtotalexento) subtotalexento
				,sum(v.MontoDescuentoExento) montodescuentoexento
				,sum(v.subtotalgravado) subtotalgravado
				,sum(v.MontoDescuentoGravado) montodescuentogravado
				,sum(v.subtotalexento)+sum(v.subtotalgravado) subtotal
				,sum(v.montoimpuesto) impuesto
				,sum(v.subtotalexento+v.subtotalgravado+v.montoimpuesto) total
		"""
        return select_str

    def _from(self):
        from_str = """
			(
				select
					d.nombre
					,d.numero
					,d.tipo_documento
					,d.fecha
					,d.reference
					,CASE
						WHEN imp=0 then subtotal
						ELSE 0
					END SubTotalExento
					,CASE
						WHEN imp=0 then montodescuento
						ELSE 0
					END MontoDescuentoExento
					,CASE
						WHEN imp>0 then subtotal
						ELSE 0
					END SubTotalGravado
					,CASE
						WHEN IMP>0 THEN montodescuento
						else 0
					END MontoDescuentoGravado
					,d.imp
					,CASE
						WHEN IMP>0 THEN (d.imp/100)*subtotal
						else 0
					END montoimpuesto

					,d.tipo
					,d.formapago
					,d.vendedor
				from(
					select
						c.name nombre
						,(select p.name from res_users u inner join res_partner p on u.partner_id=p.id where 
						u.id=f.user_id) vendedor
						,f.number numero
						,f.reference
						--,fl.price_unit
						--,fl.quantity
						,f.date_invoice fecha
						,case
							when f.type in ('out_refund','in_refund') then fl.price_subtotal*-1
							else fl.price_subtotal
						end subtotal
						--,fl.discount
						--,fl.price_subtotal_signed
						,coalesce((select
							sum(t.amount)
						from
							account_invoice_line_tax flt
								inner join account_tax t
							on flt.tax_id=t.id
						where
							fl.id=flt.invoice_line_id),0) imp
						,CASE WHEN f.type in ('in_invoice','in_refund') THEN 'Compra'
							ELSE 'Venta'
						END tipo
						,(
						select
							'Dias '||pl.days
						from
							account_payment_term p inner join account_payment_term_line pl
							on p.id=pl.payment_id
						where
							p.id=f.payment_term_id
						) formapago
						,case
							when f.type in ('out_refund','in_refund') then ((fl.price_unit*fl.quantity)*(
							fl.discount/100))*-1
							else (fl.price_unit*fl.quantity)*(fl.discount/100)
						end MontoDescuento
						,case
							when f.type in ('out_refund','in_refund') then 'Nota'
							else 'factura'
						end tipo_documento
					from
						account_invoice_line fl inner join account_invoice f
							on f.id=fl.invoice_id
						inner join res_partner c
							on f.commercial_partner_id=c.id
					where
						state not in ('draft')
						and f.date_invoice >='2017-11-01'
					) d
					where
							 tipo='Venta'
				) v
		"""
        return from_str

    def _group_by(self):
        group_by_str = """
			GROUP BY
				fecha
				,formapago
				,v.tipo_documento
				,v.vendedor
			order by
				fecha desc
				,formapago

		"""
        return group_by_str

    @api.model_cr
    def init(self):
        # self._table = sale_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
			%s
			FROM  %s
			%s
			)""" % (self._table, self._select(), self._from(), self._group_by()))


class RepUtilidad(models.Model):
    _name = "utilidad.report"
    _description = "Reporte de Utilidad"
    _auto = False
    _table = "utilidad"
    # _rec_name = 'date'
    # _order = 'date desc'
    numero = fields.Char("Numero", readonly=True)
    tipo_producto = fields.Char("Tipo Producto", readonly=True)
    codigo = fields.Char("Codigo", readonly=True)
    nombre = fields.Char("Nombre", readonly=True)
    fecha = fields.Date("Fecha", readonly=True)
    formapago = fields.Char("Forma Pago", readonly=True)
    tipo_documento = fields.Char("Tipo Documento", readonly=True)
    vendedor = fields.Char("Vendedor", readonly=True)
    cantidad_vendida = fields.Float('Cant Vendida', readonly=True)
    venta = fields.Float('Venta', readonly=True)
    costo = fields.Float('Costo', readonly=True)
    monto_utilidad = fields.Float('Monto Utilidad', readonly=True)
    por_utilidad = fields.Float('% Utilidad', group_operator="avg", readonly=True)
    por_util = fields.Float('Utilidad', readonly=True, compute="_por_utilidad")

    @api.one
    @api.depends('tipo_producto')
    def _por_utilidad(self):
        if self.costo > 0.0:
            self.por_util = (self.monto_utilidad / self.costo) * 100


class Cierre(models.Model):
    _name = 'cierre.caja'
    _cierre = 'Cierre de caja'
    state = fields.Selection([('draft', 'Borrador'), ('open', 'Aplicado'), ], string='Estado', index=True,
                             readonly=True, default='draft', track_visibility='onchange', copy=False, help="")
    consecutivo = fields.Char("Cierre");
    fecha_inicio = fields.Date("Fecha Inicio")
    fecha_cierre = fields.Date("Fecha Cierre")
    cierre_ids = fields.One2many('cierre.caja.detalle', 'cierre_id', string='Detalle cierre')
    monto_efectivo = fields.Float(string='Efectivo caja')
    ruta = fields.Char('Ruta', readonly=False, required=False,
                       help="Campo para identificar en que semana se realizo la acción")
    nota = fields.Text("Nota")

    @api.onchange('fecha_inicio')
    def _fecha(self):
        self.fecha_cierre = self.fecha_inicio

    @api.model
    def create(self, vals):
        vals['consecutivo'] = self.env['ir.sequence'].next_by_code('caja.cierre')
        return super(Cierre, self).create(vals)


class CierreDetalle(models.Model):
    _name = 'cierre.caja.detalle'
    _cierre = 'Detalle Cierre de Caja'
    tipo = fields.Selection([('ingreso', 'Ingreso'), ('egreso', 'Egreso')], 'Tipo',required=True)
    account_id = fields.Many2one('account.account', string='Cuenta Contable', help="Cuenta Contable",required=True,)
    clasificacion = fields.Selection(
        [(1, 'Aumento de Ruta'), (2, 'Cuenta por pagar TC'), (3, 'Fondo Inicial'), (4, 'Pago Proveedor'),
         (5, 'Gastos LM'), (6, 'Gastos AC'),(7, 'BADS')], 'Clasificacion',required=True)
    descripcion = fields.Char('Descripcion',)
    monto = fields.Float(string='Monto',required=True)
    cierre_id = fields.Many2one('cierre.caja', string='Referencia cierre', ondelete='cascade', index=True)
    ruta = fields.Char('Ruta', readonly=False, required=False,
                       help="Campo para identificar en que semana se realizo la acción")


class AccountPayment(models.Model):
    _inherit = 'account.payment'
    cobro_credito = fields.Boolean(readonly=False, store=True, compute="_factura_credito",
                                   string="Cobra facturas credito")
    devolucion_dinero = fields.Boolean(readonly=False, store=True, compute="_dev_dinero",
                                       string="Devolvio dinero al cliente")

    journal_id = fields.Many2one('account.journal', string='Payment Journal', required=True,
                                 domain=[('type', 'in', ('bank', 'cash'))])

    @api.one
    def _factura_credito(self):
        pass
        '''
        for f in self.invoice_ids:
            if f.rf.payment_term_id.line_ids.days > 0:
                self.cobro_credito = True
                break
            else:
                self.cobro_credito = False
        '''

    @api.one
    def _dev_dinero(self):
        for f in self.invoice_ids:
            if f.type == 'out_invoice':
                devolucion_dinero = True

class ProductCategory(models.Model):
    _inherit = "product.category"
    discount = fields.Float('Descuento máximo')


class CompanySequence(models.Model):
    _inherit = 'res.company'

    # codigo_cliente = fields.Integer(string='Código cliente', required=True)
    codigo_proveedor = fields.Integer(string='Código proveedor')
    codigo_siguiente = fields.Integer(string='Código siguiente')


class InventoryLine(models.Model):
    _inherit = 'stock.inventory.line'
    # rma_stock_used = fields.Integer(string='Cant Reparaciones', compute="_compute_rma_stock_used")
    cant_ajuste = fields.Float(string='Ajustar', compute="_diferencia")
    montoAjuste = fields.Float(string='Monto Ajuste', store=True, compute="_monto")

    """
    @api.one
    @api.depends('location_id', 'product_id', 'package_id', 'product_uom_id', 'company_id', 'prod_lot_id', 'partner_id')
    def _compute_rma_stock_used(self):
        obj = self.env['mrp.repair.fee']
        lines = obj.search([('product_id', '=', self.product_id.id),
                            ('repair_id.state', 'in', ['confirmed', 'under_repair', 'ready', '2binvoiced'])])
        qty = 0
        for a in lines:
            qty += a.product_uom_qty
        self.rma_stock_used = qty
    """

    @api.one
    @api.depends('location_id', 'product_id', 'package_id', 'product_uom_id', 'company_id', 'prod_lot_id', 'partner_id')
    def _diferencia(self):
        self.cant_ajuste = self.product_qty - self.theoretical_qty

    @api.one
    @api.depends('location_id', 'product_id', 'package_id', 'product_uom_id', 'company_id', 'prod_lot_id', 'partner_id')
    def _monto(self):
        monto = ((self.product_qty - self.theoretical_qty) * self.product_id.standard_price)
        self.montoAjuste = monto


class Inventory(models.Model):
    _inherit = 'stock.inventory'
    montoAjustePositivo = fields.Float(string='Monto ajuste positivo', compute='_montos')
    montoAjusteNegativo = fields.Float(string='Monto ajuste negativo', compute='_montos')

    @api.multi
    def _montos(self):
        monto_negativo = 0.0
        monto_positivo = 0.0
        for linea in self.line_ids:
            if linea.montoAjuste > 0:
                monto_positivo += linea.montoAjuste
            else:
                monto_negativo += linea.montoAjuste
        self.montoAjusteNegativo = monto_negativo
        self.montoAjustePositivo = monto_positivo


class FacturasAplicaInventario(models.TransientModel):
    _name = 'aux.cr.margen'
    _description = "Actualiza Precios"

    @api.multi
    def actualiza_margen_precio_ivi(self):
        context = dict(self._context or {})
        ids_seleccionados = context.get('active_ids', []) or []

        for a in self.env['product.template'].browse(ids_seleccionados):
            a.list_price = (a.standard_price * (a.margen_costo / 100)) + a.standard_price

        for articulo in self.env['product.template'].browse(ids_seleccionados):
            articulo.margen_costo = ((articulo.list_price - articulo.standard_price) / articulo.standard_price) * 100
            articulo.precio_ivi = round(((articulo.taxes_id.amount / 100) * articulo.list_price) + articulo.list_price,
                                        4)
        return {'type': 'ir.actions.act_window_close'}


"""
class MrpRepair(models.Model):
    _inherit = 'mrp.repair'
    user_id = fields.Many2one('res.users', string='Vendedor', track_visibility='onchange',default=lambda self: False, 
    required=True)
    mecanico_id = fields.Many2one('mrp.mecanico', 'Mecanico')
    detalle_reparacion = fields.Char('Modelo')
    nombre_cliente = fields.Char(readonly=False, store=True, compute="_nombre_cliente", string="Nombre")
    invoice_method = fields.Selection([
        ("none", "No Invoice"),
        ("b4repair", "Before Repair"),
        ("after_repair", "After Repair")], string="Invoice Method",
        default='after_repair', index=True, readonly=True, required=True,
        states={'draft': [('readonly', False)]},
        help='Selecting \'Before Repair\' or \'After Repair\' will allow you to generate invoice before or after the 
        repair is done respectively. \'No invoice\' means you don\'t want to generate invoice for this repair order.')
    serie = fields.Char(store=True, string='Serie')
    telefono = fields.Char(readonly=False, store=True, string="telefono")

    @api.onchange('partner_id')
    def _nombre_cliente(self):
        for oc in self:
            if (not oc.partner_id == False):
                oc.nombre_cliente = oc.partner_id.name

    @api.multi
    def action_invoice_create(self, group=False):
  
        res = dict.fromkeys(self.ids, False)
        invoices_group = {}
        InvoiceLine = self.env['account.invoice.line']
        Invoice = self.env['account.invoice']
        for repair in self.filtered(lambda repair: repair.state not in ('draft', 'cancel') and not repair.invoice_id):
            if not repair.partner_id.id and not repair.partner_invoice_id.id:
                raise UserError(_('You have to select a Partner Invoice Address in the repair form!'))
            comment = repair.quotation_notes
            if repair.invoice_method != 'none':
                if group and repair.partner_invoice_id.id in invoices_group:
                    invoice = invoices_group[repair.partner_invoice_id.id]
                    invoice.write({
                        'name': invoice.name + ', ' + repair.name,
                        'origin': invoice.origin + ', ' + repair.name,
                        'comment': (comment and (invoice.comment and invoice.comment + "\n" + comment or comment)) or (
                                invoice.comment and invoice.comment or ''),
                    })
                else:
                    if not repair.partner_id.property_account_receivable_id:
                        raise UserError(_('No account defined for partner "%s".') % repair.partner_id.name)
                    invoice = Invoice.create({
                        'name': repair.name,
                        'nombre_cliente': self.nombre_cliente,
                        'user_id': self.user_id.id,
                        'origin': repair.name,
                        'type': 'out_invoice',
                        'account_id': repair.partner_id.property_account_receivable_id.id,
                        'partner_id': repair.partner_invoice_id.id or repair.partner_id.id,
                        'currency_id': repair.pricelist_id.currency_id.id,
                        'comment': repair.quotation_notes,
                        'fiscal_position_id': repair.partner_id.property_account_position_id.id,
                        'create_stock_moves': True
                    })
                    invoices_group[repair.partner_invoice_id.id] = invoice
                repair.write({'invoiced': True, 'invoice_id': invoice.id})
                for operation in repair.operations.filtered(lambda operation: operation.to_invoice):
                    if group:
                        name = repair.name + '-' + operation.name
                    else:
                        name = operation.name

                    if operation.product_id.property_account_income_id:
                        account_id = operation.product_id.property_account_income_id.id
                    elif operation.product_id.categ_id.property_account_income_categ_id:
                        account_id = operation.product_id.categ_id.property_account_income_categ_id.id
                    else:
                        raise UserError(_('No account defined for product "%s".') % operation.product_id.name)

                    invoice_line = InvoiceLine.create({
                        'invoice_id': invoice.id,
                        'name': name,
                        'origin': repair.name,
                        'account_id': account_id,
                        'quantity': operation.product_uom_qty,
                        'invoice_line_tax_ids': [(6, 0, [x.id for x in operation.tax_id])],
                        'uom_id': operation.product_uom.id,
                        'price_unit': operation.price_unit,
                        'price_subtotal': operation.product_uom_qty * operation.price_unit,
                        'product_id': operation.product_id and operation.product_id.id or False
                    })
                    operation.write({'invoiced': True, 'invoice_line_id': invoice_line.id})
                for fee in repair.fees_lines.filtered(lambda fee: fee.to_invoice):
                    if group:
                        name = repair.name + '-' + fee.name
                    else:
                        name = fee.name
                    if not fee.product_id:
                        raise UserError(_('No product defined on Fees!'))

                    if fee.product_id.property_account_income_id:
                        account_id = fee.product_id.property_account_income_id.id
                    elif fee.product_id.categ_id.property_account_income_categ_id:
                        account_id = fee.product_id.categ_id.property_account_income_categ_id.id
                    else:
                        raise UserError(_('No account defined for product "%s".') % fee.product_id.name)

                    invoice_line = InvoiceLine.create({
                        'invoice_id': invoice.id,
                        'name': name,
                        'origin': repair.name,
                        'account_id': account_id,
                        'quantity': fee.product_uom_qty,
                        'invoice_line_tax_ids': [(6, 0, [x.id for x in fee.tax_id])],
                        'uom_id': fee.product_uom.id,
                        'product_id': fee.product_id and fee.product_id.id or False,
                        'price_unit': fee.price_unit,
                        'price_subtotal': fee.product_uom_qty * fee.price_unit
                    })
                    fee.write({'invoiced': True, 'invoice_line_id': invoice_line.id})
                invoice.compute_taxes()
                res[repair.id] = invoice.id
        return res
    """


class ResPartner(models.Model):
    _inherit = 'res.partner'

    over_credit = fields.Boolean('Allow Over Credit?')


class ProductTemplate(models.Model):
    _inherit = 'product.product'

    @api.constrains('standard_price')
    def customvalidation(self):
        if self.list_price == 1.0:
            raise UserError(_('Por favor actualice el costo'))


"""
class RepairFee(models.Model):
    _inherit = 'mrp.repair.fee'

    @api.onchange('repair_id', 'product_id', 'product_uom_qty')
    def onchange_product_id(self):
        if not self.product_id or not self.product_uom_qty:
            return

        partner = self.repair_id.partner_id
        pricelist = self.repair_id.pricelist_id

        if partner and self.product_id:
            self.tax_id = partner.property_account_position_id.map_tax(self.product_id.taxes_id, self.product_id,
                                                                       partner).ids
        if self.product_id:
            self.name = self.product_id.display_name
            self.product_uom = self.product_id.uom_id.id
            self.tax_id=self.product_id.taxes_id

        warning = False
        if not pricelist:
            warning = {'title': _('No Pricelist!'), 'message': _(
                'You have to select a pricelist in the Repair form !\n Please set one before choosing a product.')}
        else:
            price = pricelist.get_product_price(self.product_id, self.product_uom_qty, partner)
            if price is False:
                warning = {'title': _('No valid pricelist line found !'), 'message': _(
                    "Couldn't find a pricelist line matching this product and quantity.\nYou have to change either "
                    "the product, the quantity or the pricelist.")}
            else:
                self.price_unit = price
        if warning:
            return {'warning': warning}
"""
