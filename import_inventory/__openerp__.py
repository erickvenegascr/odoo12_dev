# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 BrowseInfo(<http://www.browseinfo.in>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Import Stock Inventory from Excel/CSV File',
    'version': '1.0',
    'sequence': 4,
    'summary': 'This app helps to import stock inventory adjustment from csv/excel file',
    "price": 30,
    "currency": 'EUR',
    'category' : 'Warehouse',
    'description': """
	BrowseInfo developed a new odoo/OpenERP module apps.
	This module is useful for import inventory adjustment from Excel and CSV file.
        Its also usefull for import opening stock balance from XLS or CSV file.
	-Import Stock from CSV and Excel file.
        -Import Stock inventory from CSV and Excel file.
	-Import inventory adjustment, import stock balance
	-Import opening stock balance from CSV and Excel file.
	-Inventory import from CSV, stock import from CSV, Inventory adjustment import, Opening stock import. Import warehouse stock, Import product stock.Manage Inventory
    -import inventory data, import stock data, import opening stock. 
    """,
    'author': 'BrowseInfo',
    'website': '',
    'depends': ['base','stock'],
    'data': ["stock_view.xml"
             ],
	'qweb': [
		],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    "images":['static/description/Banner.png'],
}
