# -*- encoding: utf-8 -*-

from openerp import api, fields, models, tools, _
from openerp.exceptions import ValidationError, except_orm
import base64  # file encode
import urllib2


class ProductTemplate(models.Model):
    _inherit = "product.template"

    image_url = fields.Char('Image URL')

    @api.model
    def create(self, vals):
        if vals.get('image_url'):
            try:
                vals['image'] = base64.encodestring(urllib2.urlopen(vals.get('image_url')).read())
            except:
                pass
        return super(ProductTemplate, self).create(vals)

    @api.multi
    def write(self, vals):
        if vals.get('image_url'):
            try:
                vals['image'] = base64.encodestring(urllib2.urlopen(vals.get('image_url')).read())
            except:
                pass
        return super(ProductTemplate, self).write(vals)


class ProductProduct(models.Model):
    _inherit = "product.product"

    image_url = fields.Char('Image URL')

    @api.model
    def create(self, vals):
        if vals.get('image_url'):
            try:
                vals['image'] = base64.encodestring(urllib2.urlopen(vals.get('image_url')).read())
            except:
                pass
        return super(ProductProduct, self).create(vals)

    @api.multi
    def write(self, vals):
        if vals.get('image_url'):
            try:
                vals['image'] = base64.encodestring(urllib2.urlopen(vals.get('image_url')).read())
            except:
                pass
        return super(ProductProduct, self).write(vals)

