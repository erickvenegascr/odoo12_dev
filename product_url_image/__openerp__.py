# -*- encoding: utf-8 -*-
###########################################################################
#    Copyright (C) 2016 - Turkesh Patel. <http://www.almightycs.com>
#
#    @author Turkesh Patel <info@almightycs.com>
###########################################################################

{
    'name':'Set/Import Product Image From URL',
    'version':'1.0',
    'category': "Product",
    'summary': """Set/Import Product image from URL.""",
    'description': """Set/Import Product image from URL.
    Image By URL
    Import Image
    Import Product Image
    Import image from URL
    image URL
    """,
    'author': 'Almighty Consulting Services',
    'website': 'http://www.almightycs.com',
    'depends': ['product'],
    'data':[
        'view/product_view.xml'
    ],
    'images': [
        'static/description/product_image_from_url_odoo_almightycs.png',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
    'price': 9,
    'currency': 'EUR',
}


