# -*- coding: utf-8 -*-
{
    'name': "adaptaciones_cr",

    'summary': """
       Agregar los campos de sexo y ruta """,

    'description': """
    El campo de sexo se agregara a los contactos que son indivicuales y clientes individuales, para compañias no aparece el campo y el campo de ruta se 
    agregara tanto como en la factra y a la hora de registrar  un pago y en ambos casos del campo ruta seran obligatorios
    """,

    'author': "Kenneth Parrales A",
    'website': "http://www.siteccr.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Gestion de rutas',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'account'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/ruta.xml',
        'views/ruta_pago.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}