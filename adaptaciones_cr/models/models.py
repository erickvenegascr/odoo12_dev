# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import time

#agrega el campo de sexo en los cantactos y cientes que no se consideren compañia
class campo_sexo(models.Model):
    _inherit = 'res.partner'
    res_sexo = fields.Selection([('M', 'Masculino'), ('F', 'Femenino')], 'Sexo')

    @api.onchange('name')
    def secuencia(self):
        if self.name:
            if not self.supplier:
                if not self.ref:
                    for codigo in self.env['res.partner'].search([('ref','not like','PO%')], order='ref desc', limit=1):
                        cod=codigo['ref']
                        self.ref = str(int(cod)+ 1).zfill(4)
            else:
                if self.supplier:
                    for codigo in self.env['res.partner'].search([('ref', 'like', 'PO')], order='ref desc', limit=1):
                        cod = codigo['ref']
                        cod_sustituido = cod.replace("PO", "")
                        consecutivo = int(cod_sustituido) + 1
                        self.ref = 'PO' + str(consecutivo).zfill(4)

    @api.onchange('supplier')
    def secuencia_provedor(self):
        if self.name:
            if self.supplier:
                for codigo in self.env['res.partner'].search([('ref','like','PO')],order ='ref desc',limit=1):
                    cod = codigo['ref']
                    cod_sustituido = cod.replace("PO", "")
                    consecutivo = int(cod_sustituido)+1
                    self.ref ='PO' + str(consecutivo).zfill(4)
            else:
                for codigo in self.env['res.partner'].search([('ref', 'not like', 'PO%')], order='ref desc', limit=1):
                    cod = codigo['ref']
                    self.ref = str(int(cod) + 1).zfill(4)

class consecutivo_pro(models.Model):
    _inherit = 'product.template'

    @api.onchange('name')
    def secuencia_producto(self):
               if self.name:
                name = self.type
                if self.type != 'service' and self.type != 'consu':
                    for codigo in self.env['product.template'].search([('default_code', 'not like', 'S%'),('default_code', 'not like', 'C%')],
                                                                                    order='default_code desc', limit=1):
                        cod = codigo['default_code']
                        self.default_code = int(cod) + 1
                else:
                    if self.type == 'service':
                         for codigo in self.env['product.template'].search([('default_code', 'like', 'S%')],
                                                                                    order='default_code desc', limit=1):
                            cod = codigo['default_code']
                            cod_sustituido = cod.replace("S", "")
                            consecutivo = int(cod_sustituido) + 1
                            self.default_code = 'S' + str(consecutivo).zfill(5)
                    else:
                        if self.type == 'consu':
                            for codigo in self.env['product.template'].search([('default_code','like','C%')],order='default_code desc', limit=1):
                                cod = codigo['default_code']
                                if cod:
                                    cod_sustituido = cod.replace("C", "")
                                    consecutivo = int(cod_sustituido) + 1
                                    self.default_code = 'C' + str(consecutivo).zfill(5)

    @api.onchange('type')
    def secuencia_producto_ser(self):
        if self.name:
                name = self.type
                if self.type != 'service' and self.type != 'consu':
                    for codigo in self.env['product.template'].search([('default_code', 'not like', 'S%'),('default_code', 'not like', 'C%')],
                                                                                    order='default_code desc', limit=1):
                        cod = codigo['default_code']
                        self.default_code = int(cod) + 1
                else:
                    if self.type == 'service':
                         for codigo in self.env['product.template'].search([('default_code', 'like', 'S%')],
                                                                                    order='default_code desc', limit=1):
                            cod = codigo['default_code']
                            cod_sustituido = cod.replace("S", "")
                            consecutivo = int(cod_sustituido) + 1
                            self.default_code = 'S' + str(consecutivo).zfill(5)
                    else:
                        if self.type == 'consu':
                            for codigo in self.env['product.template'].search([('default_code','like','C%')],order='default_code desc', limit=1):
                                cod = codigo['default_code']
                                if cod:
                                    cod_sustituido = cod.replace("C", "")
                                    consecutivo = int(cod_sustituido) + 1
                                    self.default_code = 'C' + str(consecutivo).zfill(5)

class consecutivo_pro(models.Model):
    _inherit = 'product.product'

    @api.onchange('name')
    def secuencia_producto_prod(self):
               if self.name:
                name = self.type
                if self.type != 'service' and self.type != 'consu':
                    for codigo in self.env['product.template'].search([('default_code', 'not like', 'S%'),('default_code', 'not like', 'C%')],
                                                                                    order='default_code desc', limit=1):
                        cod = codigo['default_code']
                        self.default_code = int(cod) + 1
                else:
                    if self.type == 'service':
                         for codigo in self.env['product.template'].search([('default_code', 'like', 'S%')],
                                                                                    order='default_code desc', limit=1):
                            cod = codigo['default_code']
                            cod_sustituido = cod.replace("S", "")
                            consecutivo = int(cod_sustituido) + 1
                            self.default_code = 'S' + str(consecutivo).zfill(5)
                    else:
                        if self.type == 'consu':
                            for codigo in self.env['product.template'].search([('default_code','like','C%')],order='default_code desc', limit=1):
                                cod = codigo['default_code']
                                if cod:
                                    cod_sustituido = cod.replace("C", "")
                                    consecutivo = int(cod_sustituido) + 1
                                    self.default_code = 'C' + str(consecutivo).zfill(5)
			
    @api.onchange('type')
    def secuencia_producto_otrostipo(self):
        if self.name:
                name = self.type
                if self.type != 'service' and self.type != 'consu':
                    for codigo in self.env['product.template'].search([('default_code', 'not like', 'S%'),('default_code', 'not like', 'C%')],
                                                                                    order='default_code desc', limit=1):
                        cod = codigo['default_code']
                        self.default_code = int(cod) + 1
                else:
                    if self.type == 'service':
                         for codigo in self.env['product.template'].search([('default_code', 'like', 'S%')],
                                                                                    order='default_code desc', limit=1):
                            cod = codigo['default_code']
                            cod_sustituido = cod.replace("S", "")
                            consecutivo = int(cod_sustituido) + 1
                            self.default_code = 'S' + str(consecutivo).zfill(5)
                    else:
                        if self.type == 'consu':
                            for codigo in self.env['product.template'].search([('default_code','like','C%')],order='default_code desc', limit=1):
                                cod = codigo['default_code']
                                if cod:
                                    cod_sustituido = cod.replace("C", "")
                                    consecutivo = int(cod_sustituido) + 1
                                    self.default_code = 'C' + str(consecutivo).zfill(5)



#agrega el campo de ruta en cuando se registra un pago de la factura
class campo_ruta(models.Model):
    _inherit = 'account.payment'
    sale_ruta_id = fields.Char('Ruta', readonly=False, required=False, help="Campo para identificar en que semana se realizo la acción")
    metodo_pago  = fields.Selection([('Efectivo','Efectivo'),('Tarjeta','Tarjeta'),('Transferencia – depósito bancario','Transferencia – depósito bancario'),('Cheque','Cheque'),('Recaudado por tercero','Recaudado por tercero'),('Otros (se debe indicar el medio de pago)','Otros (se debe indicar el medio de pago)')],
                                    'Metodo de pago',re0quired=False)		
    
#agrega el campo de ruta de la factura
class campo_en_fctura(models.Model):
    _inherit = 'account.invoice'
    sale_ruta_id = fields.Char('Ruta', readonly=False, required=False, help="Campo para identificar en que semana se realizo la acción")
    fecha_inicio = fields.Integer('Fecha inicio')

#agrega las facturas a la ruta
class agregar_fact_rut(models.TransientModel):
    _name = 'account.ruta_fact'
    _description = "Valida ruta"
    sale_ruta = fields.Char('Ruta', readonly=False, required=True, help="Campo para identificar en que semana se realizo la acción")
    fecha_inicio = fields.Integer('Fecha inicio')
    @api.multi
    def ruta_fact(self):
        if self.sale_ruta:
            context = dict(self._context or {})
            active_ids = context.get('active_ids', []) or []
            valor_ruta = self.sale_ruta
            veces = 0
            dia_de_semana = int(time.strftime("%w"))
            self.fecha_inicio = dia_de_semana
            for fecha_inicio in self.env['account.invoice'].search([('sale_ruta_id', '=', valor_ruta)],order='sale_ruta_id asc',limit=1):
                for i in self.env['account.invoice'].browse(active_ids):
                    if i.sale_ruta_id == False:
                        i.sale_ruta_id = self.sale_ruta
                        i.fecha_inicio = self.fecha_inicio
                '''
                if fecha_inicio['fecha_inicio'] != 10 and fecha_inicio['fecha_inicio'] > 10:
                    if veces == 0:
                        veces = 1
                        if (fecha_inicio.fecha_inicio <= 9 or fecha_inicio >= 9):
                            for i in self.env['account.invoice'].browse(active_ids):
                                if i.sale_ruta_id == False:
                                    i.sale_ruta_id = self.sale_ruta
                                    if valor_ruta == 6:
                                        i.fecha_inicio = 8
                                    else:
                                        if valor_ruta == 9:
                                            i.fecha_inicio = 10
                                        else:
                                            i.fecha_inicio = self.fecha_inicio
                                else:
                                    raise UserError("La factura numero " + str(i['number'])+" del cliente "+str(i['vendor_display_name'])+" ya tiene la ruta "+str(i['sale_ruta_id'])+" asignada")
                        else:
                            raise UserError("Esta ruta ya fue utilizada " + self.sale_ruta)
                    else:
                        if veces == 0:
                            for i in self.env['account.invoice'].browse(active_ids):
                                if i.sale_ruta_id == False:
                                    i.sale_ruta_id = self.sale_ruta
                                    i.fecha_inicio = self.fecha_inicio
                        else:
                            raise UserError("La factura numero " + str(i['number'])+" del cliente "+str(i['vendor_display_name'])+" ya tiene la ruta "+str(i['sale_ruta_id'])+" asignada")
                else:
                    raise UserError("La ruta " + self.sale_ruta + " ya fue utilizada")
                '''