# -*- coding: utf-8 -*-
{
    'name':'Print to Thermal Layouts',
    'description':'Print Reports on Thermal Format from BackEnd',
    'category': 'sale',
    'version':'0.1',
    'website':'https://globalresponse.cl',
    'author':'Daniel Santibáñez Polanco',
    'data': [
            'views/reports.xml',
        ],
    'depends': [
                'print_to_thermal',
            ],
    'application': True,
    'currency': 'EUR',
    'price': 10,
}
