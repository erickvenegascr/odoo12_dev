# -*- coding: utf-8 -*-

import json
import requests
import logging
import re
from odoo import models,\
    tools,fields, api, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools.safe_eval import safe_eval
import datetime
import pytz
import base64
import xml.etree.ElementTree as ET
from dateutil.parser import parse
from datetime import datetime
from odoo import SUPERUSER_ID
from odoo.tools.safe_eval import safe_eval

_logger = logging.getLogger(__name__)


# class cr_electronic_cr(models.Model):
#     _name = 'cr_electronic_cr.cr_electronic_cr'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class InvoiceLineElectronic(models.Model):
    _inherit = "pos.order.line"

    total_amount = fields.Float(string="Monto total", required=False, )
    total_discount = fields.Float(string="Total descuento", required=False, )
    discount_note = fields.Char(string="Nota de descuento", required=False, )
    total_tax = fields.Float(string="Total impuesto", required=False, )

    # exoneration_total = fields.Float(string="Exoneración total", required=False, )
    # total_line_exoneration = fields.Float(string="Exoneración total de la línea", required=False, )
    # exoneration_id = fields.Many2one(comodel_name="exoneration", string="Exoneración", required=False, )
    @api.onchange('discount')
    def _onchange_email(self):
        if self.discount > 0.0:
            self.discount_note = 'Condición de mercado'


class AccountInvoiceElectronic(models.Model):
    _inherit = "pos.order"

    number_electronic = fields.Char(string="Número electrónico", required=False, copy=False, index=True)
    date_issuance = fields.Char(string="Fecha de emisión", required=False, copy=False)
    state_send_invoice = fields.Selection([('aceptado', 'Aceptado'), ('rechazado', 'Rechazado'), ],
                                          'Estado FE Proveedor')
    state_tributacion = fields.Selection(
        [('aceptado', 'Aceptado'), ('rechazado', 'Rechazado'), ('no_encontrado', 'No encontrado')], 'Estado FE',
        copy=False)
    state_invoice_partner = fields.Selection([('1', 'Aceptado'), ('3', 'Rechazado'), ('2', 'Aceptacion parcial')],
                                             'Respuesta del Cliente')
    reference_code_id = fields.Many2one(comodel_name="reference.code", string="Código de referencia", required=False, )
    payment_methods_id = fields.Many2one(comodel_name="payment.methods", string="Métodos de Pago", required=False,
                                         default=1)
    invoice_id = fields.Many2one(comodel_name="account.invoice", string="Documento de referencia", required=False,
                                 copy=False)
    xml_respuesta_tributacion = fields.Binary(string="Respuesta Tributación XML", required=False, copy=False,
                                              attachment=True)
    fname_xml_respuesta_tributacion = fields.Char(string="Nombre de archivo XML Respuesta Tributación", required=False,
                                                  copy=False)
    xml_comprobante = fields.Binary(string="Comprobante XML", required=False, copy=False, attachment=True)
    fname_xml_comprobante = fields.Char(string="Nombre de archivo Comprobante XML", required=False, copy=False,
                                        attachment=True)
    xml_supplier_approval = fields.Binary(string="XML Proveedor", required=False, copy=False, attachment=True)
    fname_xml_supplier_approval = fields.Char(string="Nombre de archivo Comprobante XML proveedor", required=False,
                                              copy=False, attachment=True)
    amount_tax_electronic_invoice = fields.Float(string='Total de impuestos FE', readonly=True, )
    amount_total_electronic_invoice = fields.Float(string='Total FE', readonly=True, )

    _sql_constraints = [
        ('number_electronic_uniq', 'unique (number_electronic)', "La clave de comprobante debe ser única"),
    ]

    @api.model
    def _consultahacienda(self):  # cron
        print('!!!Buscando')
        invoices = self.env['pos.order'].search([('number_electronic', '!=', False), ('state_tributacion', '=', False)],offset=0,order='date_order desc',limit=25)
        for i in invoices:
            if i.number_electronic and len(i.number_electronic) == 50:
                if i.company_id.frm_ws_ambiente == 'stag':
                    requests_url = self.env.user.company_id.url_api + '/consultadocumento'
                else:
                    requests_url = self.env.user.company_id.url_api + '/consultadocumento'
                payload = {
                    'clave': i.number_electronic,
                }
                headers = {
                    'content-type': 'application/json',
                    # 'authorization': "Bearer " + token['access_token'],
                }
                response_document = requests.post(requests_url,
                                                  headers=headers,
                                                  data=json.dumps(payload))
                response_content = json.loads(response_document._content.decode('utf-8'))
                if response_content.get('code') == 33:
                    i.state_tributacion = 'no_encontrado'
                if response_content.get('hacienda_result'):
                    if response_content.get('hacienda_result').get('ind-estado'):
                        i.state_tributacion = response_content.get('hacienda_result').get('ind-estado')
                        i.fname_xml_respuesta_tributacion = 'respuesta_tributacion_' + i.name + '.xml'
                        i.xml_respuesta_tributacion = response_content.get('hacienda_result').get('respuesta-xml')

    @api.multi
    def firma_envia_hacienda_auto(self):
        invoices = self.env['pos.order'].search(
            [('name', '=like', '001000%'), ('state', 'in', ('done', 'paid')),
             ('state_tributacion', '=', False), ('xml_comprobante', '=', False),('amount_total','>','0.0')], offset=0,order='date_order desc',limit=25 )
        self._procesa_documentos(invoices)

    def _procesa_documentos(self, documentos):
        for inv in documentos:
            if inv.xml_comprobante == False and '001000' in inv.name and inv.state in (
                    'done', 'paid') and inv.amount_total > 0.0:
                if inv.company_id.frm_ws_ambiente != 'disabled':
                    TipoDocumento = inv.name[8:10]
                    FacturaReferencia = ''
                    user_tz = self.env.user.tz or pytz.utc
                    local = pytz.timezone(user_tz)
                    date_cr = datetime.strftime(pytz.utc.localize(
                        datetime.strptime(str(inv.write_date)[:-7], tools.DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(
                        local), "%Y-%m-%dT%H:%M:%S-06:00")
                    fecha = pytz.utc.localize(
                        datetime.strptime(str(inv.write_date)[:-7], tools.DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(
                        local)
                    now_cr = date_cr
                    tipo_documento_referencia = ''
                    numero_documento_referencia = ''
                    fecha_emision_referencia = ''
                    codigo_referencia = ''
                    razon_referencia = ''
                    medio_pago = inv.payment_methods_id.sequence or '01'
                    if inv.amount_total < 0:  # FC Y ND
                        if inv.invoice_id and inv.journal_id and inv.journal_id.nd:
                            TipoDocumento = '02'
                            tipo_documento_referencia = inv.invoice_id.number_electronic[
                                                        29:31]  # 50625011800011436041700100001 01 0000000154112345678
                            numero_documento_referencia = inv.invoice_id.number_electronic
                            fecha_emision_referencia = inv.invoice_id.date_issuance
                            codigo_referencia = inv.reference_code_id.code
                            razon_referencia = inv.reference_code_id.name
                            medio_pago = ''
                        else:
                            TipoDocumento = '01'
                    if inv.amount_total < 0:  # NC
                        if inv.invoice_id.journal_id.nd:
                            tipo_documento_referencia = '02'
                        else:
                            tipo_documento_referencia = '01'
                        TipoDocumento = '03'
                        numero_documento_referencia = inv.invoice_id.number_electronic
                        fecha_emision_referencia = inv.invoice_id.date_issuance
                        codigo_referencia = inv.reference_code_id.code
                        razon_referencia = inv.reference_code_id.name
                    # if inv.origin and inv.origin.isdigit():
                    #	FacturaReferencia = (inv.origin)
                    # else:
                    #	FacturaReferencia = 0
                    # if inv.payment_term_id:
                    #	if inv.payment_term_id.sale_conditions_id:
                    #		sale_conditions = inv.payment_term_id.sale_conditions_id.sequence or '01'
                    #	else:
                    #		raise UserError('No se pudo Crear la factura electrónica: Debe configurar condiciones de pago para')
                    # else:
                    sale_conditions = '01'
                    numero_comprobante = inv.name[10:20]
                    if numero_comprobante.isdigit() and TipoDocumento:
                        currency_rate = 1
                        lines = []
                        base_total = 0.0
                        numero = 0
                        if inv.partner_id.identification_id.code == '05':
                            receptor_identificacion = {
                                'tipo': False,
                                'numero': False,
                            }
                            receptor_identificacion_extranjero = inv.partner_id.vat
                        else:
                            receptor_identificacion = {
                                'tipo': inv.partner_id.identification_id.code,
                                'numero': inv.partner_id.vat,
                            }
                            receptor_identificacion_extranjero = ''

                        totalserviciogravado = 0.0
                        totalservicioexento = 0.0
                        totalmercaderiagravado = 0.0
                        totalmercaderiaexento = 0.0
                        descuento_acumulado = 0.0
                        totalImpuesto = 0.0
                        total_otros_cargos = 0.0
                        for inv_line in inv.lines:

                            impuestos_acumulados = 0.0
                            numero += 1
                            base_total += inv_line.price_unit * inv_line.qty

                            subtotal_linea = round(inv_line.price_unit * inv_line.qty, 4)
                            subtotal_linea_descontado = (
                                    subtotal_linea - round((subtotal_linea * (inv_line.discount / 100)), 4))
                            impuestos = []
                            otros_cargos = []
                            for i in inv_line.tax_ids_after_fiscal_position:
                                if i.tax_code not in ('00','TotalOtrosCargos'):
                                    impuestos.append({
                                        'codigo': i.tax_code,
                                        'codigotarifa': i.codigo_tarifa,
                                        'tarifa': i.amount,
                                        'monto': round((i.amount / 100) * inv_line.price_subtotal, 4),
                                    })

                                    #impuestos_acumulados += round((i.amount / 100) * inv_line.price_subtotal, 2)
                                    totalImpuesto += round((i.amount / 100) * subtotal_linea_descontado, 4)

                                    impuestos_acumulados += round((i.amount / 100) * subtotal_linea_descontado, 4)
                                    impuesto_linea = round((i.amount / 100) * subtotal_linea_descontado, 4)

                                if i.tax_code in ('TotalOtrosCargos'):
                                    otros_cargos.append(
                                        {'tipodocumento': i.codigo_tarifa, 'detalle': 'Impuesto de servicio',
                                            'porcentaje': i.amount,
                                            'montocargo': round((i.amount / 100) * inv_line.price_subtotal, 4), })
                                    total_otros_cargos += round((i.amount / 100) * subtotal_linea_descontado, 4)
                            descuento_acumulado += (round(inv_line.qty * inv_line.price_unit, 2) - round(inv_line.price_subtotal, 2))


                            if inv_line.product_id:
                                if inv_line.product_id.type == 'service':
                                    if impuestos_acumulados:
                                        totalserviciogravado += inv_line.qty * inv_line.price_unit
                                    else:
                                        totalservicioexento += inv_line.qty * inv_line.price_unit
                                else:
                                    if impuestos_acumulados:
                                        totalmercaderiagravado += inv_line.qty * inv_line.price_unit
                                    else:
                                        totalmercaderiaexento += inv_line.qty * inv_line.price_unit
                            else:  # se asume que si no tiene producto setrata como un type product
                                if impuestos_acumulados:
                                    totalmercaderiagravado += inv_line.qty * inv_line.price_unit
                                else:
                                    totalmercaderiaexento += inv_line.qty * inv_line.price_unit
                            line = {
                                'numero': numero,
                                'codigo': [{
                                    'tipo': inv_line.product_id.code_type_id.code or '04',
                                    'codigo': inv_line.product_id.default_code or '000',
                                }],
                                'cantidad': inv_line.qty,
                                'unidad_medida': 'Unid',
                                'unidad_medida_comercial': inv_line.product_id.commercial_measurement,
                                'detalle': inv_line.product_id.name[:159],
                                'precio_unitario': inv_line.price_unit,
                                'monto_total': inv_line.qty * inv_line.price_unit,
                                'descuento': [
                                    {'monto': (round(inv_line.qty * inv_line.price_unit, 4) - round(
                                                                        inv_line.price_subtotal, 4)) or '0.0',
                                     'naturaleza': inv_line.discount_note, }],
                                'naturaleza_descuento': inv_line.discount_note,
                                'subtotal': (inv_line.qty * inv_line.price_unit) - (
                                        round(inv_line.qty * inv_line.price_unit, 2) - round(
                                    inv_line.price_subtotal, 2)),
                                'impuestos': impuestos,
                                'otroscargos': otros_cargos,
                                'montototallinea': subtotal_linea_descontado + impuestos_acumulados,
                            }
                            lines.append(line)
                        _logger.error('MAB - formando payload')
                        nombre = ''
                        if inv.partner_id.id == False:
                            nombre = 'Cliente Contado'
                        else:
                            nombre = inv.partner_id.name[:80]
                        if descuento_acumulado < 0.0:
                            descuento_acumulado = 0.0
                        payload = {
                            'clave': {
                                'sucursal': inv.name[:3],
                                'terminal': inv.name[3:8],
                                'tipo': inv.name[8:10],
                                'comprobante': inv.name[10:20],
                                'pais': '506',
                                'dia': '%02d' % fecha.day,
                                'mes': '%02d' % fecha.month,
                                'anno': str(fecha.year)[2:4],
                                'situacion_presentacion': '1',
                                'codigo_seguridad': inv.company_id.security_code,
                            },
                            'encabezado': {
                                'fecha': date_cr,
                                # 'fecha': "2018-01-19T23:17:00+06:00",
                                'condicion_venta': sale_conditions,
                                'plazo_credito': '0',
                                'medio_pago': medio_pago,
                                'codigo_actividad': inv.company_id.codigo_actividad,
                            },
                            'emisor': {
                                'nombre': inv.company_id.name,
                                'identificacion': {
                                    'tipo': inv.company_id.identification_id.code,
                                    'numero': inv.company_id.vat,
                                },
                                'nombre_comercial': inv.company_id.commercial_name or '',
                                'ubicacion': {
                                    'provincia': inv.company_id.state_id.code,
                                    'canton': inv.company_id.county_id.code,
                                    'distrito': inv.company_id.district_id.code,
                                    'barrio': inv.company_id.neighborhood_id.code,
                                    'sennas': inv.company_id.street,
                                },
                                'telefono': {
                                    'cod_pais': inv.company_id.phone_code,
                                    'numero': inv.company_id.phone,
                                },
                                'fax': {
                                    'cod_pais': inv.company_id.phone_code,
                                    'numero': inv.company_id.phone,
                                },
                                'correo_electronico': inv.company_id.email,
                            },
                            'receptor': {  ##
                                'nombre': nombre,
                                'identificacion': receptor_identificacion,
                                'IdentificacionExtranjero': receptor_identificacion_extranjero,
                            },
                            'detalle': lines,
                            'resumen': {
                                'moneda': 'CRC',
                                'tipo_cambio': round(currency_rate, 5),
                                'totalserviciogravado': totalserviciogravado,
                                'totalservicioexento': totalservicioexento,
                                'totalmercaderiagravado': totalmercaderiagravado,
                                'totalmercaderiaexento': totalmercaderiaexento,
                                'totalgravado': totalserviciogravado + totalmercaderiagravado,
                                'totalexento': totalservicioexento + totalmercaderiaexento,
                                'totalventa': totalserviciogravado + totalmercaderiagravado + totalservicioexento + totalmercaderiaexento,
                                'totaldescuentos': round(descuento_acumulado, 2),
                                'totalventaneta': (
                                                          totalserviciogravado + totalmercaderiagravado + totalservicioexento + totalmercaderiaexento) - round(
                                    descuento_acumulado, 2)
                                ,'totalotroscargos': total_otros_cargos,
                                'totalimpuestos': totalImpuesto,
                                'totalcomprobante': inv.amount_total,
                            },
                            'referencia': [{
                                'tipo_documento': tipo_documento_referencia,
                                'numero_documento': numero_documento_referencia,
                                'fecha_emision': fecha_emision_referencia,
                                'codigo': codigo_referencia,
                                'razon': razon_referencia,
                            }],
                            'otros': [{
                                'codigo': '',
                                'texto': '',
                                'contenido': ''
                            }],
                        }
                        headers = {
                            'content-type': 'application/json',
                            # 'authorization': "Bearer " + token['access_token'],
                        }
                        _logger.error('Hola')
                        if inv.company_id.frm_ws_ambiente == 'stag':
                            requests_url = self.env.user.company_id.url_api + '/makeXML.stag.43'
                        else:
                            requests_url = self.env.user.company_id.url_api + '/makeXML.stag.43'
                        xdata = json.dumps(payload)
                        response_document = requests.post(requests_url,
                                                          headers=headers,
                                                          data=xdata)
                        _logger.error('MAB - JSON DATA:%s', xdata)
                        _logger.error('MAB - response:%s', response_document._content)
                        response_content = json.loads(response_document._content.decode('utf-8'))
                        if response_content.get('code'):
                            if response_content.get('code') == 1:
                                inv.number_electronic = response_content.get('clave')
                                inv.date_issuance = date_cr
                            else:
                                raise UserError(
                                    'No se pudo Crear la factura electrónica: \n' + str(response_document._content))
                        if response_document.status_code == 200:  # TODO
                            if response_document._content:
                                # inv.electronic_invoice_return_message = response_document._content
                                inv.fname_xml_comprobante = 'comprobante_' + inv.name + '.xml'
                                inv.xml_comprobante = response_content.get('data')
                                # if not inv.partner_id.opt_out:
                                #	email_template = self.env.ref('account.email_template_edi_invoice', False)
                                #	attachment = self.env['ir.attachment'].search([('res_model','=','account.invoice'),('res_id','=',inv.id),('res_field','=','xml_comprobante')], limit=1)
                                #	attachment.name = inv.fname_xml_comprobante
                                #	attachment.datas_fname = inv.fname_xml_comprobante
                                #	email_template.attachment_ids = [(6,0,[attachment.id])] # [(4, attachment.id)]
                                #	email_template.with_context(type='binary', default_type='binary').send_mail(inv.id, raise_exception=False, force_send=True)#default_type='binary'
                                #	email_template.attachment_ids = [(3, attachment.id)]
                                if response_document._content.decode('utf-8').split(',')[1][5:] == 'false':
                                    raise UserError('No se pudo Crear la factura electrónica: \n' + str(
                                        response_document._content))
                        else:
                            raise UserError(
                                'No se pudo Crear la factura electrónica: \n' + str(response_document._content))



class FacturasAplicaInventario(models.TransientModel):
    """
    Envia y firma a hacienda
    """
    _name = "pos.order.envia"
    _description = "Envia Hacienda Facturas"

    @api.multi
    def firma_envia_hacienda(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', []) or []

        for inv in self.env['pos.order'].browse(active_ids):
            inv._procesa_documentos(inv)
        """
        for inv in self.env['pos.order'].browse(active_ids):
            if inv.xml_comprobante == False and '001000' in inv.name and inv.state in (
            'done', 'paid') and inv.amount_total > 0.0:
                if inv.company_id.frm_ws_ambiente != 'disabled':
                    TipoDocumento = inv.name[8:10]
                    user_tz = self.env.user.tz or pytz.utc
                    local = pytz.timezone(user_tz)
                    FacturaReferencia = ''

                    date_cr = datetime.strftime(
                        pytz.utc.localize(datetime.strptime(str(inv.write_date)[:-7],
                                                            tools.DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(local),
                        "%Y-%m-%dT%H:%M:%S-06:00")
                    fecha = pytz.utc.localize(datetime.strptime(str(inv.write_date)[:-7],
                                                                tools.DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(
                        local)

                    now_cr=date_cr
                    tipo_documento_referencia = ''
                    numero_documento_referencia = ''
                    fecha_emision_referencia = ''
                    codigo_referencia = ''
                    razon_referencia = ''
                    medio_pago = inv.payment_methods_id.sequence or '01'
                    if inv.amount_total < 0:  # FC Y ND
                        if inv.invoice_id and inv.journal_id and inv.journal_id.nd:
                            TipoDocumento = '02'
                            tipo_documento_referencia = inv.invoice_id.number_electronic[
                                                        29:31]  # 50625011800011436041700100001 01 0000000154112345678
                            numero_documento_referencia = inv.invoice_id.number_electronic
                            fecha_emision_referencia = inv.invoice_id.date_issuance
                            codigo_referencia = inv.reference_code_id.code
                            razon_referencia = inv.reference_code_id.name
                            medio_pago = ''
                        else:
                            TipoDocumento = '01'
                    if inv.amount_total < 0:  # NC
                        if inv.invoice_id.journal_id.nd:
                            tipo_documento_referencia = '02'
                        else:
                            tipo_documento_referencia = '01'
                        TipoDocumento = '03'
                        numero_documento_referencia = inv.invoice_id.number_electronic
                        fecha_emision_referencia = inv.invoice_id.date_issuance
                        codigo_referencia = inv.reference_code_id.code
                        razon_referencia = inv.reference_code_id.name
                    # if inv.origin and inv.origin.isdigit():
                    #	FacturaReferencia = (inv.origin)
                    # else:
                    #	FacturaReferencia = 0
                    # if inv.payment_term_id:
                    #	if inv.payment_term_id.sale_conditions_id:
                    #		sale_conditions = inv.payment_term_id.sale_conditions_id.sequence or '01'
                    #	else:
                    #		raise UserError('No se pudo Crear la factura electrónica: Debe configurar condiciones de pago para')
                    # else:
                    sale_conditions = '01'
                    numero_comprobante = inv.name[10:20]
                    if numero_comprobante.isdigit() and TipoDocumento:
                        currency_rate = 1
                        lines = []
                        base_total = 0.0
                        numero = 0
                        if inv.partner_id.identification_id.code == '05':
                            receptor_identificacion = {
                                'tipo': False,
                                'numero': False,
                            }
                            receptor_identificacion_extranjero = inv.partner_id.vat
                        else:
                            receptor_identificacion = {
                                'tipo': inv.partner_id.identification_id.code,
                                'numero': inv.partner_id.vat,
                            }
                            receptor_identificacion_extranjero = ''

                        totalserviciogravado = 0.0
                        totalservicioexento = 0.0
                        totalmercaderiagravado = 0.0
                        totalmercaderiaexento = 0.0
                        descuento_acumulado = 0.0
                        totalImpuesto = 0.0
                        for inv_line in inv.lines:

                            impuestos_acumulados = 0.0
                            numero += 1
                            base_total += inv_line.price_unit * inv_line.qty
                            impuestos = []
                            for i in inv_line.tax_ids:
                                if i.tax_code not in '00':
                                    impuestos.append({
                                        'codigo': i.tax_code,
                                        'codigotarifa': i.codigo_tarifa,
                                        'tarifa': i.amount,
                                        'monto': round((i.amount / 100) * inv_line.price_subtotal, 2),
                                    })
                                    impuestos_acumulados += round((i.amount / 100) * inv_line.price_subtotal, 2)
                                    totalImpuesto += round((i.amount / 100) * inv_line.price_subtotal, 2)
                            descuento_acumulado += (
                                        round(inv_line.qty * inv_line.price_unit, 2) - round(inv_line.price_subtotal,
                                                                                             2))
                            if inv_line.product_id:
                                if inv_line.product_id.type == 'service':
                                    if impuestos_acumulados:
                                        totalserviciogravado += inv_line.qty * inv_line.price_unit
                                    else:
                                        totalservicioexento += inv_line.qty * inv_line.price_unit
                                else:
                                    if impuestos_acumulados:
                                        totalmercaderiagravado += inv_line.qty * inv_line.price_unit
                                    else:
                                        totalmercaderiaexento += inv_line.qty * inv_line.price_unit
                            else:  # se asume que si no tiene producto setrata como un type product
                                if impuestos_acumulados:
                                    totalmercaderiagravado += inv_line.qty * inv_line.price_unit
                                else:
                                    totalmercaderiaexento += inv_line.qty * inv_line.price_unit
                            line = {
                                'numero': numero,
                                'codigo': [{
                                    'tipo': inv_line.product_id.code_type_id.code or '04',
                                    'codigo': inv_line.product_id.default_code or '000',
                                }],
                                'cantidad': inv_line.qty,
                                'unidad_medida': 'Unid',
                                'unidad_medida_comercial': inv_line.product_id.commercial_measurement,
                                'detalle': inv_line.product_id.name[:159],
                                'precio_unitario': inv_line.price_unit,
                                'monto_total': inv_line.qty * inv_line.price_unit,
                                'descuento': (round(inv_line.qty * inv_line.price_unit, 2) - round(
                                    inv_line.price_subtotal, 2)) or '0.0',
                                'naturaleza_descuento': inv_line.discount_note,
                                'subtotal': (inv_line.qty * inv_line.price_unit) - (
                                            round(inv_line.qty * inv_line.price_unit, 2) - round(
                                        inv_line.price_subtotal, 2)),
                                'impuestos': impuestos,
                                'montototallinea': inv_line.price_subtotal_incl,
                            }
                            lines.append(line)
                        _logger.error('MAB - formando payload')
                        nombre = ''
                        if inv.partner_id.id == False:
                            nombre = 'Cliente Contado'
                        else:
                            nombre = inv.partner_id.name[:80]
                        if descuento_acumulado < 0.0:
                            descuento_acumulado = 0.0
                        payload = {
                            'clave': {
                                'sucursal': inv.name[:3],
                                'terminal': inv.name[3:8],
                                'tipo': inv.name[8:10],
                                'comprobante': inv.name[10:20],
                                'pais': '506',
                                'dia': '%02d' % fecha.day,
                                'mes': '%02d' % fecha.month,
                                'anno': str(fecha.year)[2:4],
                                'situacion_presentacion': '1',
                                'codigo_seguridad': inv.company_id.security_code,
                            },
                            'encabezado': {
                                'fecha': date_cr,
                                # 'fecha': "2018-01-19T23:17:00+06:00",
                                'condicion_venta': sale_conditions,
                                'plazo_credito': '0',
                                'medio_pago': medio_pago,
                                'codigo_actividad': inv.company_id.codigo_actividad,
                            },
                            'emisor': {
                                'nombre': inv.company_id.name,
                                'identificacion': {
                                    'tipo': inv.company_id.identification_id.code,
                                    'numero': inv.company_id.vat,
                                },
                                'nombre_comercial': inv.company_id.commercial_name or '',
                                'ubicacion': {
                                    'provincia': inv.company_id.state_id.code,
                                    'canton': inv.company_id.county_id.code,
                                    'distrito': inv.company_id.district_id.code,
                                    'barrio': inv.company_id.neighborhood_id.code,
                                    'sennas': inv.company_id.street,
                                },
                                'telefono': {
                                    'cod_pais': inv.company_id.phone_code,
                                    'numero': inv.company_id.phone,
                                },
                                'fax': {
                                    'cod_pais': '506',
                                    'numero': "22222222",
                                },
                                'correo_electronico': inv.company_id.email,
                            },
                            'receptor': {  ##
                                'nombre': nombre,
                                'identificacion': receptor_identificacion,
                                'IdentificacionExtranjero': receptor_identificacion_extranjero,
                            },
                            'detalle': lines,
                            'resumen': {
                                'moneda': 'CRC',
                                'tipo_cambio': round(currency_rate, 5),
                                'totalserviciogravado': totalserviciogravado,
                                'totalservicioexento': totalservicioexento,
                                'totalmercaderiagravado': totalmercaderiagravado,
                                'totalmercaderiaexento': totalmercaderiaexento,
                                'totalgravado': totalserviciogravado + totalmercaderiagravado,
                                'totalexento': totalservicioexento + totalmercaderiaexento,
                                'totalventa': totalserviciogravado + totalmercaderiagravado + totalservicioexento + totalmercaderiaexento,
                                'totaldescuentos': round(descuento_acumulado, 2),
                                'totalventaneta': (
                                                              totalserviciogravado + totalmercaderiagravado + totalservicioexento + totalmercaderiaexento) - round(
                                    descuento_acumulado, 2),
                                'totalimpuestos': totalImpuesto,
                                'totalcomprobante': inv.amount_total,
                            },
                            'referencia': [{
                                'tipo_documento': tipo_documento_referencia,
                                'numero_documento': numero_documento_referencia,
                                'fecha_emision': fecha_emision_referencia,
                                'codigo': codigo_referencia,
                                'razon': razon_referencia,
                            }],
                            'otros': [{
                                'codigo': '',
                                'texto': '',
                                'contenido': ''
                            }],
                        }
                        headers = {
                            'content-type': 'application/json',
                            # 'authorization': "Bearer " + token['access_token'],
                        }
                        _logger.error('Hola')
                        if inv.company_id.frm_ws_ambiente == 'stag':
                            requests_url = 'http://localhost:8084/decr/api/makeXML.stag.43'
                        else:
                            requests_url = 'http://localhost:8084/decr/api/makeXML.stag.43'
                        xdata = json.dumps(payload)
                        response_document = requests.post(requests_url,
                                                          headers=headers,
                                                          data=xdata)
                        _logger.error('MAB - JSON DATA:%s', xdata)
                        _logger.error('MAB - response:%s', response_document._content)
                        response_content = json.loads(response_document._content.decode('utf-8'))
                        #response_content = json.loads(response_document._content)
                        if response_content.get('code'):
                            if response_content.get('code') == 1:
                                inv.number_electronic = response_content.get('clave')
                                inv.date_issuance = date_cr
                            else:
                                raise UserError(
                                    'No se pudo Crear la factura electrónica: \n' + str(response_document._content))
                        if response_document.status_code == 200:  # TODO
                            if response_document._content:
                                # inv.electronic_invoice_return_message = response_document._content
                                inv.fname_xml_comprobante = 'comprobante_' + inv.name + '.xml'
                                inv.xml_comprobante = response_content.get('data')
                                # if not inv.partner_id.opt_out:
                                #	email_template = self.env.ref('account.email_template_edi_invoice', False)
                                #	attachment = self.env['ir.attachment'].search([('res_model','=','account.invoice'),('res_id','=',inv.id),('res_field','=','xml_comprobante')], limit=1)
                                #	attachment.name = inv.fname_xml_comprobante
                                #	attachment.datas_fname = inv.fname_xml_comprobante
                                #	email_template.attachment_ids = [(6,0,[attachment.id])] # [(4, attachment.id)]
                                #	email_template.with_context(type='binary', default_type='binary').send_mail(inv.id, raise_exception=False, force_send=True)#default_type='binary'
                                #	email_template.attachment_ids = [(3, attachment.id)]
                        else:
                            raise UserError(
                                'No se pudo Crear la factura electrónica: \n' + str(response_document._content))
            """


class wiz_mass_invoice(models.TransientModel):
    _name = 'wiz.mass.invoice.pos'

    @api.multi
    def mass_invoice_email_send(self):
        context = self._context
        active_ids = context.get('active_ids')
        super_user = self.env['res.users'].browse(SUPERUSER_ID)
        for a_id in active_ids:
            account_invoice_brw = self.env['pos.order'].browse(a_id)
            for partner in account_invoice_brw.partner_id:
                partner_email = partner.email
                if not partner_email:
                    raise UserError(_('%s customer has no email id please enter email address')
                                    % (account_invoice_brw.partner_id.name))
                else:
                    template_id = self.env['ir.model.data'].get_object_reference(
                        'account',
                        'email_template_edi_invoice')[1]
                    email_template_obj = self.env['mail.template'].browse(template_id)
                    if template_id:
                        values = email_template_obj.generate_email(a_id, fields=None)
                        # values['email_from'] = super_user.email
                        values['email_to'] = partner.email
                        values['res_id'] = a_id
                        ir_attachment_obj = self.env['ir.attachment']
                        vals = {
                            'name': account_invoice_brw.number or "Draft",
                            'type': 'binary',
                            'datas': values['attachments'][0][1],
                            'res_id': a_id,
                            'res_model': 'account.invoice',
                            'datas_fname': account_invoice_brw.number or "Draft",
                        }
                        attachment_id = ir_attachment_obj.create(vals)
                        # Set boolean field true after mass invoice email sent
                        account_invoice_brw.write({
                            'is_invoice_sent': True
                        })
                        mail_mail_obj = self.env['mail.mail']
                        msg_id = mail_mail_obj.create(values)
                        msg_id.attachment_ids = [(6, 0, [attachment_id.id])]
                        if msg_id:
                            mail_mail_obj.send([msg_id])

        return True


class PosOrderMail(models.Model):
    _inherit = 'pos.order'
    is_invoice_sent = fields.Boolean('Factura enviada', default=False)


class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail(self, auto_commit=False):
        for wizard in self:
            if wizard.attachment_ids and wizard.composition_mode != 'mass_mail' and wizard.template_id:
                new_attachment_ids = []
                for attachment in wizard.attachment_ids:
                    if attachment in wizard.template_id.attachment_ids:
                        new_attachment_ids.append(
                            attachment.copy({'res_model': 'mail.compose.message', 'res_id': wizard.id}).id)
                    else:
                        new_attachment_ids.append(attachment.id)
                    wizard.write({'attachment_ids': [(6, 0, new_attachment_ids)]})

            # Mass Mailing
            mass_mode = wizard.composition_mode in ('mass_mail', 'mass_post')

            Mail = self.env['mail.mail']
            ActiveModel = self.env[wizard.model if wizard.model else 'mail.thread']
            if wizard.template_id:
                # template user_signature is added when generating body_html
                # mass mailing: use template auto_delete value -> note, for emails mass mailing only
                Mail = Mail.with_context(mail_notify_user_signature=False)
                ActiveModel = ActiveModel.with_context(mail_notify_user_signature=False,
                                                       mail_auto_delete=wizard.template_id.auto_delete)
            if not hasattr(ActiveModel, 'message_post'):
                ActiveModel = self.env['mail.thread'].with_context(thread_model=wizard.model)
            if wizard.composition_mode == 'mass_post':
                # do not send emails directly but use the queue instead
                # add context key to avoid subscribing the author
                ActiveModel = ActiveModel.with_context(mail_notify_force_send=False, mail_create_nosubscribe=True)
            # wizard works in batch mode: [res_id] or active_ids or active_domain
            if mass_mode and wizard.use_active_domain and wizard.model:
                res_ids = self.env[wizard.model].search(safe_eval(wizard.active_domain)).ids
            elif mass_mode and wizard.model and self._context.get('active_ids'):
                res_ids = self._context['active_ids']
            else:
                res_ids = [wizard.res_id]

            batch_size = int(self.env['ir.config_parameter'].sudo().get_param('mail.batch_size')) or self._batch_size
            sliced_res_ids = [res_ids[i:i + batch_size] for i in range(0, len(res_ids), batch_size)]

            if wizard.composition_mode == 'mass_mail' or wizard.is_log or (
                    wizard.composition_mode == 'mass_post' and not wizard.notify):  # log a note: subtype is False
                subtype_id = False
            elif wizard.subtype_id:
                subtype_id = wizard.subtype_id.id
            else:
                subtype_id = self.sudo().env.ref('mail.mt_comment', raise_if_not_found=False).id

            for res_ids in sliced_res_ids:
                batch_mails = Mail
                account_invoice_brw = self.env['account.invoice'].browse(res_ids)
                all_mail_values = wizard.get_mail_values(res_ids)
                # Set boolean field true after mass invoice email sent
                account_invoice_brw.write({
                    'is_invoice_sent': True
                })
                for res_id, mail_values in all_mail_values.iteritems():
                    if wizard.composition_mode == 'mass_mail':
                        batch_mails |= Mail.create(mail_values)
                    else:
                        ActiveModel.browse(res_id).message_post(
                            message_type=wizard.message_type,
                            subtype_id=subtype_id,
                            **mail_values)

                if wizard.composition_mode == 'mass_mail':
                    batch_mails.send(auto_commit=auto_commit)

        return {'type': 'ir.actions.act_window_close'}

