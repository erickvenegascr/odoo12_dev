# -*- coding: utf-8 -*-
# Copyright 2016 Serpent Consulting Services Pvt. Ltd.
# See LICENSE file for full copyright and licensing details.

from odoo import api, models, _
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo.exceptions import UserError
import logging

_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
    _inherit = "sale.order"

    @api.multi
    def check_limit(self):
        """Check if credit limit for partner was exceeded."""
        self.ensure_one()
        self.sudo()
        #_logger.error('Dias: '+str(self.partner_id.property_payment_term_id.line_ids.days))
        #_logger.error(str(datetime.strftime(datetime.now().date(), DF)))
        dias = self.partner_id.property_payment_term_id.line_ids.days
        if(dias>0):
            partner = self.partner_id
            today_dt = datetime.strftime(datetime.now().date(), DF)
            cant_facturas_vencidas=0
            facturas_obj=self.env['account.invoice']
            facturas=facturas_obj.\
                search([('partner_id', '=', partner.id),
                        ('state','in',['open'])])
            for f in facturas:
                if f.date_due <= today_dt:
                    cant_facturas_vencidas+=1

            if not partner.over_credit and cant_facturas_vencidas > 0:
                msg = 'No se puede confirmar la orden'
                raise UserError(_('Cliente tiene facturas vencidas!\n' + msg))


            #_logger.error('Cantidad Facturas Vencidas '+str(cant_facturas_vencidas))
            moveline_obj = self.env['account.move.line']
            movelines = moveline_obj.\
                search([('partner_id', '=', partner.id),
                        ('account_id.user_type_id.type', 'in',
                        ['receivable', 'payable']),
                        ('full_reconcile_id', '=', False)])


            debit, credit = 0.0, 0.0

            for line in movelines:
                if line.date_maturity < today_dt:
                    credit += line.debit
                    debit += line.credit

            if (credit - debit + self.amount_total) > partner.credit_limit:
                # Consider partners who are under a company.
                if partner.over_credit or (partner.parent_id and partner.parent_id.over_credit):
                    partner.write({
                        'credit_limit': credit - debit + self.amount_total})
                    return True
                else:
                    msg = 'No se puede confirmar la orden'
                    raise UserError(_('Limite de crédito excedido!\n' + msg))
            else:
                return True

    @api.multi
    def action_confirm(self):
        """Extend to check credit limit before confirming sale order."""
        for order in self:
            order.check_limit()
        return super(SaleOrder, self).action_confirm()

    @api.multi
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        for order in self:
            order.check_limit()
        return super(SaleOrder, self).onchange_partner_id()
