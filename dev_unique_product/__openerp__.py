# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Devintelle Software Solutions (<http://www.devintelle.com>).
#
##############################################################################
{
    'name': 'Unique Product on Sale,Purchase and Invoice',
    'version': '1.0',
    'sequence':1,
    'category': 'Generic Modules/Sales Management',
    'summary': 'odoo app will raise a warning when same Product repeated on Sale,Purchase and Invoice lines.',
    'description': """
       odoo app will raise a warning when same Product repeated on Sale,Purchase and Invoice lines.
Unique Product on Sale,Purchase and Invoice
Odoo Unique Product on Sale,Purchase and Invoice
Unique product on sale 
Odoo unique product on sale 
Unique product on purchase 
Odoo unique product on purchase 
Unique product on invoice 
Odoo unique product on invoice 
Manage Unique product on sale 
Odoo manage unique product on sale 
Manage Unique product on purchase 
Odoo manage unique product on purchase 
Manage Unique product on invoice 
Odoo manage unique product on invoice 
Print Unique product on sale 
Odoo print unique product on sale 
Print Unique product on purchase 
Odoo print unique product on purchase 
Print Unique product on invoice 
Odoo print unique product on invoice 
raise a warning when same Product repeated on Sale,Purchase and Invoice lines
odoo raise a warning when same Product repeated on Sale,Purchase and Invoice lines
Raise a warning when same Product repeated on Sale Order
Odoo Raise a warning when same Product repeated on Sale Order
Raise a warning when same Product repeated on Purchase Order
Odoo Raise a warning when same Product repeated on Purchase Order
Raise a warning when same Product repeated on Invoice
Odoo Raise a warning when same Product repeated on Invoice
       
       
    """,
    'author': 'DevIntelle Consulting Service Pvt.Ltd',
    'website': 'http://www.devintellecs.com/',
    'images': ['images/main_screenshot.jpg'],
    'depends': ['sale', 'purchase', 'stock', 'account'],
    'data': [
        'sale_view.xml',
    ],
    'test': [],
    'installable':True,
    'application':True,
    'auto-install':False,
    'price':10.0,
    'currency':'EUR', 
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
