# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 Devintelle Software Solutions (<http://devintelle.com>).
#
##############################################################################


from openerp import api, models, fields, _
from openerp.exceptions import ValidationError


class SaleOrder(models.Model):
    
    _inherit = "sale.order"
    
    allow_duplicate = fields.Boolean('Allow Duplicates', default=False, copy=False)

    @api.multi
    def copy(self, default=None):
        self.ensure_one()
        default = dict(default or {})
        res=super(SaleOrder,self).copy(default)
        pro=[]
        for line in res.order_line:
            if line.product_id.id not in pro:
                pro.append(line.product_id.id)
            else:
                raise ValidationError(_('Product should be Unique per Sale Order !'))
        return res
    

    
class SaleOrderLine(models.Model):
    
    _inherit = "sale.order.line"
    
    @api.one
    @api.constrains('product_id', 'order_id')
    def _check_unique_constraint(self):
        if not self.order_id.allow_duplicate:
            if len(self.search([('order_id', '=', self.order_id.id),('product_id', '=', self.product_id.id)])) > 1:
                raise ValidationError("Product %s must be unique per Sale Order"% (self.product_id.name,))

class AccountInvoice(models.Model):
    
    _inherit = "account.invoice"
    
    @api.multi
    def copy(self, default=None):
        self.ensure_one()
        default = dict(default or {})
        res=super(AccountInvoice,self).copy(default)
        pro=[]
        for line in res.invoice_line_ids:
            if line.product_id.id not in pro:
                pro.append(line.product_id.id)
            else:
                raise ValidationError(_('Product should be Unique per Invoice !'))
        return res
        
    
class AccountInvoiceLine(models.Model):
    
    _inherit = "account.invoice.line"

    @api.one
    @api.constrains('product_id', 'invoice_id')
    def _check_unique_constraint(self):
        if len(self.search([('invoice_id', '=', self.invoice_id.id),('product_id', '=', self.product_id.id)])) > 1:
            raise ValidationError("Product %s must be unique per Invoice "%(self.product_id.name,))
    
class PurchaseOrder(models.Model):
    
    _inherit = "purchase.order"
    
    @api.multi
    def copy(self, default=None):
        self.ensure_one()
        default = dict(default or {})
        res=super(PurchaseOrder,self).copy(default)
        pro=[]
        for line in res.order_line:
            if line.product_id.id not in pro:
                pro.append(line.product_id.id)
            else:
                raise ValidationError(_('Product should be Unique per Purchase Order !'))
        return res
    

class PurchaseOrderLine(models.Model):
    
    _inherit = "purchase.order.line"

    @api.one
    @api.constrains('product_id', 'order_id')
    def _check_unique_constraint(self):
        if len(self.search([('order_id', '=', self.order_id.id),('product_id', '=', self.product_id.id)])) > 1:
            raise ValidationError("Product %s must be unique per Purchase Order" %(self.product_id.name,))

    
    

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

