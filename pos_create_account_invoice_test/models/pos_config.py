# -*- coding: utf-8 -*-

from odoo import fields, models


class AccountInvoice(models.Model):

    _inherit = 'account.invoice'

    pos_config_id = fields.Many2one('pos.config', string="Point of Sale")


class PosConfig(models.Model):

    _inherit = 'pos.config'

    create_invoice = fields.Boolean(
        string="Create Account Invoices",
        help="Allow creating Account Invoices from this POS.",
        default=True,
    )
    invoice_state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('proforma', 'Pro-forma'),
            ('proforma2', 'Pro-forma2'),
            ('open', 'Open'),
            ('paid', 'Done'),
            ('cancel', 'Cancelled')
        ],
        default='draft',
        string='Invoice Status',
        help="Choose state of future Invoices, which are created from POS."
    )
