odoo.define('pos_create_account_invoice.create_invoice', function (require) {"use strict";

var screens = require('point_of_sale.screens');
var Model = require('web.Model')
var AccountInvoice = new Model('account.invoice');
var AccountInvoiceLine = new Model('account.invoice.line');
var Prod = new Model('product.product');
var Categoria = new Model('product.category');


var CreateInvoiceButton = screens.ActionButtonWidget.extend({
  template: 'CreateInvoiceButton',

  button_click: function(){
    var order = this.pos.get_order();
    var lines = order.get_orderlines();
    var self = this
    if (lines.length == 0) {
      this.gui.show_popup('confirm', {
        'title': ("There's no order lines in order!"),
        'body': ('Do you want proceed and create empty Account Invoice?'),
        'confirm': function () {
          self.create_invoice()
        }
      });
    }
    else {
      this.create_invoice()
    }
  },

  create_invoice: function () {
    var self = this
    var order = this.pos.get_order();
    var lines = order.get_orderlines();
    var cat = 57;


    if (this.pos.get_client() === null) {
      this.gui.show_popup('confirm', {
        'title': ('No Customer selected!'),
        'body': ('Do you want select Customer now?'),
        'confirm': function () {
          this.gui.show_screen('clientlist')
        }
      });
    }
    else {
      AccountInvoice.call(
        'create',
        [{
          'partner_id': this.pos.get_client().id,
          'pos_config_id': this.pos.config.id,
          'state': this.pos.config.invoice_state,
        }]
      ).then(function (invoice_id) {
        AccountInvoice.query(['account_id']).filter(
          [['id','=', invoice_id]]
        ).first().then(function (invoice) {
          //console.log(invoice);
          var i = 0;
          var j = 0;
          var cat_id=57;
          while (i < lines.length) {
            Prod.query(['name','name', 'default_code','property_account_income_id','categ_id'])
            .filter([['id','=', lines[i].get_product().id]])
            .first().then(function (producto) {
              Categoria.query(['id','property_account_income_categ_id'])
              .filter([['id','=',producto.categ_id[0]]])
              .first().then(function (categoria) {
                //categoria.property_account_income_categ_id[0];
                cat=categoria.property_account_income_categ_id[0];
              });
            });
            AccountInvoiceLine.call(
              'create',
              [{
                'invoice_id': invoice_id,
                'account_id': cat,
                'product_id': lines[i].get_product().id,
                'name': lines[i].get_product().display_name,
                'price_unit': lines[i].price,
                'quantity': lines[i].get_quantity(),
                'layout_category_sequence': j,
                'sequence': j++,

              }]
            ).then(order.remove_orderline(lines[i]))
          }
        });
        return invoice_id
      }).then(function (invoice_id) {
        var invoice_url = ('/web?db=' + odoo.session_info.db + '#id=' + invoice_id + '&view_type=form&model=account.invoice')
        return invoice_url
      }).then(function (invoice_url) {
        self.gui.show_popup('confirm', {
          'title': ("Account Invoice successfully created!"),
          'body': ("Do you want open created Account Invoice in browser's new tab?"),
          'confirm': function () {
            window.open(invoice_url, '_blank')
          }
        })
      })
    }
  }
});

screens.define_action_button({
  'name': 'create_invoice',
  'widget': CreateInvoiceButton,
  'condition': function(){
    return this.pos.config.create_invoice && this.pos.config.invoice_state;}
  });


});
