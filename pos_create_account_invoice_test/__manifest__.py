# -*- coding: utf-8 -*-
# Author: Red Lab.
# Copyright: Red Lab.

{
    'name': 'Create Account Invoice from Point of Sale',
    'images': ['images/main_screenshot.png'],
    'version': '0.4.0',
    'category': 'Point of Sale',
    'summary': "Create Account Invoice from POS (Point of Sale). Configurable in Settings. "
               "Get track on created Invoice's from POS dashboard.",
    'author': 'Red Lab',
    'website': 'lab.stone.red@gmail.com',
    'price': 18.00,
    'currency': 'EUR',
    'depends': [
        'point_of_sale',
        'account',
    ],
    'qweb': [
        'static/src/xml/pos_templates.xml',
    ],
    'data': [
        'views/account_views.xml',
        'views/pos_views.xml',
        'views/pos_templates.xml',
    ],
}
