========================================
Create Account Invoice from Point of Sale
========================================

Installation
============

* `Install <https://odoo-development.readthedocs.io/en/latest/odoo/usage/install-module.html>`__ this module in a usual way

Configuration
=============

* `Enable technical features <https://odoo-development.readthedocs.io/en/latest/odoo/usage/technical-features.html>`__
* Go to ``Point of Sale`` settings via ``Point of Sale`` menu >> ``Point of Sale`` option under ``Configuration`` section. Open/create new Point of Sale.
* Under ``Account Invoices`` section disable/enable ``Create Account Invoices`` in order to allow or permit Account Invoice creation from selected Point of Sale.
* Choose ``Account Invoice Status`` of future PO's


Usage
=====

* In Point of Sale session click on ``Create Account Invoice`` button, after products are selected.
