{
    'name': "POS Auto Invoice",
    'version': '3.0',
    'category': 'Point of Sale',
    'author': 'TL Technology',
    'website': 'http://posodoo.com',
    'sequence': 0,
    'depends': ['point_of_sale'],
    'data': [
        'template/__import__.xml',
        'view/account_jounal.xml',
    ],
    'qweb': [
        'static/src/xml/*.xml'
    ],
    "currency": 'EUR',
    'price': '50',
    'application': True,
    'images': ['static/description/icon.png'],
    'support': 'thanhchatvn@gmail.com'
}
