from odoo import api, fields, models, _

class account_journal(models.Model):

    _inherit = "account.journal"

    auto_invoice = fields.Boolean('Auto Invoice', help='If customer use a this payment method, pos order auto made a invoice and register payment amount')
    payment_method_id = fields.Many2one('account.payment.method', 'Payment method', domain=[('payment_type', '=', 'inbound')])
    invoice_state = fields.Selection([
        ('open', 'Open'),
        ('paid', 'Paid')
    ], 'State of invoice', help='This is state of invoice come from POS order will be still', default='open')

