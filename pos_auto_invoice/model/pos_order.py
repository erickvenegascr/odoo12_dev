from openerp import fields, api, models
import logging

_logger = logging.getLogger(__name__)


class posOrder(models.Model):

    _inherit = "pos.order"

    @api.model
    def create_from_ui(self, orders):
        _logger.info('begin create_from_ui')
        order_ids = super(posOrder, self).create_from_ui(orders)
        for o in orders:
            statement_datas = o['data']['statement_ids']
            auto_invoice = False
            invoice_to_paid = False
            for statement in statement_datas:
                statement = statement[2]
                journal = self.env['account.journal'].browse(statement['journal_id'])
                if (journal.auto_invoice == True):
                    auto_invoice = True
                if (journal.invoice_state == 'paid'):
                    invoice_to_paid = True
            if auto_invoice == True:
                order = self.search([('pos_reference', '=', o['data']['name'])])
                _logger.info(order)
                _logger.info(order.invoice_id)
                if order and not order.invoice_id:
                    order.action_pos_order_invoice()
                    order.invoice_id.sudo().action_invoice_open()
                    order.account_move = order.invoice_id.move_id
                    _logger.info('==> created and processed invoice to open')
                    for data in statement_datas:
                        data = data[2]
                        journal = self.env['account.journal'].browse(data['journal_id'])
                        if invoice_to_paid == True:
                            forma_pago_contado_id=False;
                            terminos_pagos=self.env['account.payment.term'].search([])
                            for termino_pago in terminos_pagos:
                                for termino_pago_linea in termino_pago.line_ids:
                                    if termino_pago_linea.days==0:
                                        forma_pago_contado_id=termino_pago.id
                            order.invoice_id.payment_term_id=forma_pago_contado_id;

                            amount = data['amount']
                            if (amount > order.invoice_id.residual):
                                amount = order.invoice_id.residual
                            payment_vals = {
                                'invoice_ids': [(6, 0, [order.invoice_id.id])],
                                'amount': amount,
                                'payment_date': o['data']['creation_date'],
                                'communication': 'POS_AUTO/' + order.name,
                                'partner_id': order.partner_id.id,
                                'partner_type': 'customer',
                                'payment_type': journal.payment_method_id.payment_type,
                                'payment_method_id': journal.payment_method_id.id,
                                'journal_id': journal.id,
                            }
                            payment = self.env['account.payment'].create(payment_vals)
                            payment.post()
                            _logger.info('==> post register amount: %s and processed invoice to: %s' % (amount, order.invoice_id.state))
        _logger.info('end create_from_ui')
        return order_ids
