odoo.define('pos_auto_invoice', function (require) {

    var models = require('point_of_sale.models');
    var core = require('web.core');
    var _t = core._t;

    var _super_posmodel = models.PosModel.prototype;
    models.PosModel = models.PosModel.extend({
        initialize: function (session, attributes) {
            var account_journal_model = _.find(this.models, function (model) {
                return model.model === 'account.journal';
            });
            account_journal_model.fields.push('auto_invoice', 'invoice_state', 'payment_method_id');
            return _super_posmodel.initialize.call(this, session, attributes);
        },
    });

    var screens = require('point_of_sale.screens')
    screens.PaymentScreenWidget.include({

        click_numpad: function (button) {
            var paymentlines = this.pos.get_order().get_paymentlines();
            var open_paymentline = false;
            for (var i = 0; i < paymentlines.length; i++) {
                if (!paymentlines[i].paid) {
                    open_paymentline = true;
                }
            }
            if (!open_paymentline) {
                var order = this.pos.get_order().export_as_JSON();
                var casregister_the_first = this.pos.cashregisters[0];
                if (!order.partner_id && casregister_the_first && casregister_the_first && casregister_the_first.journal && casregister_the_first.journal.auto_invoice == true) {
                    this.gui.show_screen('clientlist');
                    this.gui.show_popup('error', {
                        'title': _t('Empty Customer'),
                        'body': _t('Please add customer the first, Payment method [' + casregister_the_first.journal_id[1]  + '] you just selected require a customer')
                    });
                }
            }
            return this._super(button);
        },

        click_paymentmethods: function (id) {
            var cashregister = null;
            for (var i = 0; i < this.pos.cashregisters.length; i++) {
                if (this.pos.cashregisters[i].journal_id[0] === id) {
                    cashregister = this.pos.cashregisters[i];
                    break;
                }
            }
            var order = this.pos.get_order().export_as_JSON();
            if (cashregister && cashregister.journal && cashregister.journal.auto_invoice == true && !order.partner_id) {
                this.gui.show_screen('clientlist');
                this.gui.show_popup('error', {
                    'title': _t('Empty Customer'),
                    'body': _t('Please add customer the first, Payment method [' + cashregister.journal_id[1]  + '] you just selected require a customer')
                });
            } else {
                return this._super(id);
            }
        },
    })

})