# -*- encoding: utf-8 -*-
###########################################################################
#    Copyright (C) 2016 - Almighty Consulting Services <http://www.almightycs.com>
#
#    @author Turkesh Patel <info@almightycs.com>
###########################################################################

{
    'name': "Web Calculator",
    'version': "1.0",
    'category': 'web',
    'summary': """Web Calculator on backend""",
    'description': """Web Calculator on backend to complete your need.
    calculator in odoo
    """,
    'author': 'Almighty Consulting Services',
    'website': 'http://www.almightycs.com',
    'depends': ['base', 'web'],
    'data': [
        'view/web_calculator_template.xml'
    ],
    'qweb': ["static/src/xml/calculator.xml"],
    'images': [
        'static/description/odoo_web_calculator_cover.png',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
    "price": 10,
    "currency": "EUR",
}
