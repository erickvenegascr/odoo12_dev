# -*- coding: utf-8 -*-
import base64

from odoo import models, fields, api, _
from escpos.printer import Network
from PIL import Image
import io
#from . import escpos
#from escpos.impl.epson import GenericESCPOS

from odoo.exceptions import UserError, RedirectWarning, ValidationError

from odoo.tools import email_re, email_split, email_escape_char, float_is_zero, float_compare, pycompat, date_utils
from odoo.addons import decimal_precision as dp
from datetime import date
import logging
import dateutil.parser
import unidecode

_logger = logging.getLogger(__name__)

class costoArticulo(Exception):
    def __init__(seln, valor):
        seln.valor = valor

    def __str__(seln):
        return repr(seln.valor)


class ResCompany(models.Model):
    _inherit = 'res.company'
    ip_impresora = fields.Char("IP Impresora Tiquetes", index=True, required=False, translate=False,
                               help="192.168.0.11:9100")
    copias_credito = fields.Integer("Copias credito", default=1)
    copias_contado = fields.Integer("Copias contado",default=1)

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    indicaciones = fields.Char('Indicaciones', required=False)

class AccountInvoice(models.Model):
    _inherit = 'sale.order'
    nota_impresion_boleta_mensajeria = fields.Text(string='Nota impresión boleta mensajeria')

class SaleOrderLine(models.Model):
    _inherit = 'account.invoice.line'
    indicaciones = fields.Char('Indicaciones', required=False)

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def _compute_picking_count(seln):
        for pos in seln:
            pickings = pos.order_ids.mapped('picking_id').filtered(lambda x: x.state != 'done')
            pos.picking_count = len(pickings.ids)

    @api.multi
    def imprime_documento(seln):
        pass


    @api.multi
    def action_invoice_open(seln):

        for inv in seln:
            if not inv.env.user.aplica_factura:
                raise UserError('Usuario no autorizado')
            imprimir = True
            to_open_invoices = inv.filtered(lambda inv: inv.state != 'open')
            if to_open_invoices.filtered(lambda inv: not inv.partner_id):
                raise UserError(_("The field Vendor is required, please complete it to validate the Vendor Bill."))
            if to_open_invoices.filtered(lambda inv: inv.state != 'draft'):
                raise UserError(_("Invoice must be in draft state in order to validate it."))
            if to_open_invoices.filtered(
                    lambda inv: float_compare(inv.amount_total, 0.0, precision_rounding=inv.currency_id.rounding) == -1):
                raise UserError(_(
                    "You cannot validate an invoice with a negative total amount. You should create a credit note "
                    "instead."))
            if to_open_invoices.filtered(lambda inv: not inv.account_id):
                raise UserError(
                    _('No account was found to create the invoice, be sure you have installed a chart of account.'))
            to_open_invoices.action_date_assign()
            to_open_invoices.action_move_create()
            if inv.type in ('out_invoice'):
                if imprimir:
                    copias=range(0)
                    if inv.payment_term_id.line_ids.days>0:
                        copias=range(inv.company_id.copias_credito)
                    else:
                        copias=range(inv.company_id.copias_contado)

                    for copia in copias:
                        printer = Network(inv.company_id.ip_impresora)
                        printer.set(align='center')
                        printer.set(font='a', text_type='BU', align='center')
                        printer.text(inv.company_id.company_registry+ '\n')
                        printer.set(font='a', align='center')
                        printer.text('Ced. Jur. ' + inv.company_id.vat+'\n')
                        printer.text(' '+'\n')
                        printer.text('Tel.: ' + inv.company_id.phone+'\n')
                        printer.text(''+inv.company_id.street+'\n')
                        printer.text('Email: '+inv.company_id.email+'\n')
                        printer.text(' '+'\n')
                        printer.set(align='left')
                        descuento_aplicado = 0.0
                        if (inv.type == 'out_invoice'):
                            printer.text('Factura: ' + inv.number+'\n')
                            if len(copias)>1:
                                if copia==0:
                                    printer.text('ORIGINAL'+'\n')
                                else:
                                    printer.text('COPIA'+'\n')
                        else:
                            printer.text('DEVOLUCION: ' + seln.number+'\n')
                        dias = inv.payment_term_id.line_ids.days
                        if (dias == 0):
                            printer.text('Forma de pago: CONTADO'+'\n')
                        else:
                            printer.text('Forma de pago: CREDITO'+'\n')
                        printer.text('Fecha:   ' + str(inv.date_invoice)+'\n')

                        printer.text('Vencimiento: ' + str(inv.date_due)+'\n')
                        if inv.nombre_cliente:
                            printer.text('Cliente: ' + unidecode.unidecode(inv.nombre_cliente) + '\n')
                        else:
                            printer.text('Cliente: ' + unidecode.unidecode(inv.partner_id.name) + '\n')
                        printer.text('Vendedor: ' + inv.user_id.name+'\n')

                        if inv.origin:
                            pedido = inv.env['sale.order'].search([('name', '=', inv.origin)])
                            if (not isinstance(pedido.team_id, bool)):
                                #printer.set(font='b', text_type='BU', align='center')
                                printer.text(unidecode.unidecode('Fuente: ' + pedido.team_id.name) + '\n')
                                #printer.set(font='a', text_type='NORMAL')
                        printer.text('Moneda: Colones'+'\n')
                        printer.text(' ')
                        printer.set(align='center')
                        printer.text('Detalle\n')
                        printer.set(align='center')
                        printer.text('---------------------------------------'+'\n')
                        articulos = inv.invoice_line_ids
                        for articulo in articulos:
                            descuento_aplicado += (articulo.price_unit * articulo.quantity) * (articulo.discount / 100)
                            printer.set(align='left')
                            impuesto=''
                            if articulo.invoice_line_tax_ids:
                                for imp in articulo.invoice_line_tax_ids:
                                    if len(impuesto)>0:
                                        impuesto= impuesto+', '+imp.display_name
                                    else:
                                        impuesto = imp.display_name
                            if articulo.name.find('Ind.') != -1:
                                posicion = articulo.name.find('Ind.')
                                printer.text(unidecode.unidecode('Prod: '+articulo.name[:posicion]) + '\n')
                            else:
                                printer.text(unidecode.unidecode('Prod: '+articulo.name)+'\n')
                            printer.text(' >P/U: ' + '{:,}'.format(articulo.price_unit) + '< *Cant: ' + str(articulo.quantity)+'*\n')
                            if len(impuesto)==0:
                                printer.text('<Exento IVA>\n')
                            else:
                                printer.text('<'+impuesto+'>\n')
                            printer.set(align='right')
                            printer.text('SubT: ' + '{:,}'.format(articulo.price_subtotal)+'\n')
                        printer.set(align='center')
                        printer.text('---------------------------------------'+'\n')
                        printer.set(align='left')
                        printer.set(font='a', text_type='B', align='right')
                        if (descuento_aplicado > 0.0):
                            printer.text('Descuento aplicado: ' + '{:,}'.format(descuento_aplicado)+'\n')
                        printer.text('SubTotal: CRC ' + '{:,}'.format(inv.amount_untaxed)+'\n')
                        printer.text('Impuesto: CRC ' + '{:,}'.format(inv.amount_tax)+'\n')
                        printer.text('Total: CRC ' + '{:,}'.format(inv.amount_total)+'\n')
                        printer.set(align='center')
                        printer.text(' '+'\n')
                        printer.text(' '+'\n')
                        if (not isinstance(inv.comment, bool)):
                            pass
                            #printer.text(unidecode.unidecode(seln.comment)+'\n')
                        printer.text(' ')
                        printer.text(' ')
                        if (inv.type == 'out_invoice'):
                            printer.text('Autorizado mediante oficio \n')
                            printer.text('No. DGT-R-033-2019 \n')
                            printer.text('de fecha 20-06-2019 de la D.G.T. \n')
                        if (inv.type == 'out_invoice'):
                            '''
                            printer.text(
                                'Clave Numerica: 506' + ('%02d' % fecha_factura.day) + ('%02d' % 
                                fecha_factura.month) + str(
                                    fecha_factura.year)[2:4] + str(seln.company_id.vat).zfill(
                                    12) + seln.number + '1' + seln.company_id.security_code)
                            '''
                        printer.text(' ')
                        printer.cut()
                        printer.close()
        res = super(AccountInvoice, seln).invoice_validate()
        return res
        #return to_open_invoices.invoice_validate()
                # except ValueError as e:
                #    raise ValidationError('El articulo: '+a.name+' no tiene COSTO')
                # except ZeroDivisionError as e:
                #    raise ValidationError('No se puede cambiar la condicion de pago')
                # except:
                #    raise  ValidationError('Error de impresion')

class account_payment(models.Model):
    _inherit = "account.payment"

    @api.multi
    def post(seln):
        if not seln.env.user.aplica_pago:
            raise UserError('Usuario no autorizado')
        res = super(account_payment, seln).post()
        credito = True
        devolucion_dinero = False
        for f in seln.invoice_ids:
            # _logger.debug('Entro!'+repr(f.payment_term_id.name))
            if (not isinstance(f.payment_term_id.name, bool)):
                if ('inmediato' in f.payment_term_id.name):
                    credito = False
            if (f.type == 'out_refund'):
                devolucion_dinero = True

        if (devolucion_dinero):
            pass
            '''
            try:
                conn = NetworkConnection.create(seln.company_id.ip_impresora)
                printer = GenericESCPOS(conn)
                printer.init()
                printer.text(seln.company_id.company_registry)
                printer.text(seln.company_id.vat)
                printer.text(' ')
                printer.text('Tel.: ' + seln.company_id.phone)
                printer.text(seln.company_id.street)
                printer.text(seln.company_id.email)
                # printer.text('PRUEBA!!')
                printer.text(' ')
                printer.justify_left()
                printer.text('NC Dinero CLIENTE: ' + seln.name)
                printer.text('Fecha: ' + seln.payment_date)
                printer.text('Cliente: ' + unidecode.unidecode(seln.partner_id.name))
                printer.text(' ')
                printer.text('Devolucion de dinero por ')
                printer.text('los siguientes documentos: ')
                printer.text(' ')
                printer.text('Factura                     Monto Pagado')
                printer.text('----------------------------------------')
                for f in seln.invoice_ids:
                    printer.justify_left()
                    printer.text(f.number)
                printer.justify_right()
                printer.text('{:,}'.format(seln.amount))
                printer.text('----------------------------------------')
                printer.text(' ')
                printer.text('Tipo: ' + unidecode.unidecode(seln.journal_id.display_name))
                printer.ln(3)
                printer.text('________________________________________')
                printer.text('Firma del cliente')
                printer.ln(10)
            except:
                raise ValidationError('Error de impresion')

        if (seln.payment_type == "inbound" and credito and not devolucion_dinero and seln.partner_id.customer):
            if seln.intereses > 0 and not seln.intereses_revisado:
                raise ValidationError('No se revisaron los intereses')
            try:
                conn = NetworkConnection.create(seln.company_id.ip_impresora)
                printer = GenericESCPOS(conn)
                printer.init()
                printer.text(seln.company_id.company_registry)
                printer.text(seln.company_id.vat)
                printer.text(' ')
                printer.text('Tel.: ' + seln.company_id.phone)
                printer.text(seln.company_id.street)
                printer.text(seln.company_id.email)
                # printer.text('PRUEBA!!')
                printer.text(' ')
                printer.justify_left()
                printer.text('RECIBO: ' + seln.name)
                printer.text('Fecha: ' + seln.payment_date)
                printer.text('Cliente: ' + unidecode.unidecode(seln.partner_id.name))
                printer.text(' ')
                printer.text('Por cancelacion de Recibo de dinero a')
                printer.text('los siguientes documentos: ')
                printer.text(' ')
                printer.text('Factura                     Monto Pagado')
                printer.text('----------------------------------------')
                for f in seln.invoice_ids:
                    printer.justify_left()
                    printer.text(f.number)
                    printer.text(' -->Nuevo Saldo: ' + '{:,}'.format(f.residual))
                printer.justify_right()
                printer.text('{:,}'.format(seln.amount))
                printer.text('----------------------------------------')
                printer.text(' ')
                printer.text('Forma de pago: ' + unidecode.unidecode(seln.journal_id.display_name))
                printer.ln(3)
                printer.text('________________________________________')
                printer.text(seln.company_id.company_registry)
                printer.ln(10)
            except:
                raise ValidationError('Error de impresion')
            '''
        return res


class Usuario(models.Model):
    _inherit = 'res.users'
    aplica_pago = fields.Boolean('Aplica pago', default=True )
    aplica_factura = fields.Boolean('Aplica factura', default=True)
