
================================================
``general_template`` changelog
================================================

*******************************************
Added a new feature for showing amount in words and product image
*******************************************

1. Added feature to show product image in all report template
2. Show amount in words in all report template
3. Show amount in words feature is added for 4 languages English, France(Français), German(Deutsch) and Arabic.

*******************************************
Added an access right for Portal user
*******************************************

While Printing the report from website for portal user set proper access right.

***********************************************
Display vat number
*********************************************

1. Add vat number in all reports template

**********************************************
Rearrange Reporting tab in partner view
**********************************************

If user not have accounting access right than partner view give error.

************************************************
display option for add amount in words
*********************************************
add hide/show option for amount in words

************************************************************
Added Amount Due in invoice reports and font size changes
************************************************************
added amount due in invoice reports and change font size of total invoice amount
changed font size of total sale amount
Updated all delivery reports for Order (Origin) field

**************************************************************************
Add new template,new language and solve image page break
**************************************************************************
1. Add new language Dutch / Nederlands (nl_NL) for all report template
2. Add new template (incredible and innovative) for all report template
3. change the po translation for de as per new traslation
4. Resolve the issue, when page breake at that time not image breake.
5. Add Sale layout category for Invoice Report.


************************************************************
Solve issue of installation
*****************************************************************
Solve issue of installation in same database.

***************************************************************************
Amount to word in multi-language.
***************************************************************************
Add amount to word in multi-language.
support following laguage.
en (English, default)
ar (Arabic)
de (German)
dk (Danish)
en_GB (English - Great Britain)
en_IN (English - India)
es (Spanish)
es_CO (Spanish - Colombia)
es_VE (Spanish - Venezuela)
eu (EURO)
fr (French)
fr_CH (French - Switzerland)
fr_DZ (French - Algeria)
he (Hebrew)
id (Indonesian)
it (Italian)
lt (Lithuanian)
lv (Latvian)
no (Norwegian)
pl (Polish)
pt_BR (Brazilian Portuguese)
sl (Slovene)
ru (Russian)
tr (Turkish)
vn (Vietnamese)
nl (Dutch)
uk (Ukrainian)
solve issue of blank-page in reports

*******************************************
Add colorpicker for hexa color code.
*******************************************

Add new colorpicker library for user input for hex color code.