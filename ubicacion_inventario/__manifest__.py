# -*- coding: utf-8 -*-
{
    'name': "ubicacion_inventario",

    'summary': """
        Agrega ubicacion a los articulos""",

    'description': """
        Campo para la ubicacion del inventario
    """,

    'author': "Erick Venegas",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['product'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}

