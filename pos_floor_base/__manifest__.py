# -*- coding: utf-8 -*-

{
    'name': 'Pos Floor Management',
    'version': '1.0',
    'category': 'Point of Sale',
    'sequence': 6,
    'author': 'Webveer',
    'summary': 'Easy way to manage restaurant floors and tables.',
    'description': """

=======================
Easy way to manage restaurant floors and tables.
""",
    'depends': ['pos_restaurant'],
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml'
    ],
    'qweb': [
        # 'static/src/xml/pos.xml',
    ],
    'images': [
        'static/description/floor.jpg',
    ],
    'installable': True,
    'website': '',
    'auto_install': False,
    'price': 19,
    'currency': 'EUR',
}
