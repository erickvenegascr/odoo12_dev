# -*- coding: utf-8 -*-

{
    'name': 'Factura Tiquete',
    'version': '1.0',
    'category': 'Account',
    'sequence': 6,
    'author': 'Erick Venegas',
    'summary': 'Imprime Factura tiquete',
    'description': """

=======================

Factura en tiquete.

""",
    'depends': ['base','account','sale'],
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
    ],
    'qweb': [
        # 'static/src/xml/pos.xml',
    ],
    'images': [
        'static/description/receipt.jpg',
    ],
    'installable': True,
    'website': '',
    'auto_install': False,
    'price': 29,
    'currency': 'EUR',
}
