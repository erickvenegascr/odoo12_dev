# -*- coding: utf-8 -*-
from datetime import datetime
import pytz
from odoo import api, fields, models, tools, _


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    indicaciones = fields.Char('Indicaciones', required=False)


class AccountInvoice(models.Model):
    _inherit = 'sale.order'
    nota_impresion_boleta_mensajeria = fields.Text(string='Nota impresión boleta mensajeria')


class SaleOrderLine(models.Model):
    _inherit = 'account.invoice.line'
    indicaciones = fields.Char('Indicaciones', required=False)


class AccountInvoice(models.Model):
    _inherit = "account.invoice"


class AccountPrintReceipt(models.TransientModel):
    _name = 'account.print.receipt'

    name = fields.Char("Test")


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def print_boleta_mensajeria(self):
        view_id = self.env.ref('account_receipt.account_print_receipt_form').id

        user_tz = self.env.user.tz or pytz.utc
        local = pytz.timezone(user_tz)
        date_cr = datetime.strftime(pytz.utc.localize(
            datetime.strptime(str(self.write_date)[:-7], tools.DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(local),
                                    "%d-%m-%Y")

        text_val = """
                        <div class="pos-sale-ticket">
                            <div style="font-size: 20px">
                            <div style="text-align:center">
                            <img class="pos-center-align" src="/web/binary/company_logo"/>
                            <br/><b>BOLETA MENSAJERIA</b><br/>
                            """ + self.company_id.name + """<br/>
                            <br/>Ced. Jur. """ + self.company_id.vat + """<br/>
                            Tel.:""" + self.company_id.phone + """<br/>
                            """ + self.company_id.street + """<br/>
                            Email:""" + self.company_id.email + """<br/>
                            <div class="pos-center-align">Factura: """ + self.number + """</div>
                             <div class="pos-center-align">Fecha: """ + date_cr + """</div>
                            Cliente: """ + self.partner_id.name + """<br/>
                            Codigo: """ + self.partner_id.ref + """<br/>
                            Telefono: """ + self.partner_id.mobile + """<br/>
                            Telefono: """ + self.partner_id.street + """<br/>
                            <br/>
                            </div>
                """
        mensaje_boleta=""
        if self.origin:
            pedido = self.env['sale.order'].search([('name', '=', self.origin)])
            if (not isinstance(pedido.nota_impresion_boleta_mensajeria, bool)):
                mensaje_boleta=pedido.nota_impresion_boleta_mensajeria
        text_val += """
                    <div style="text-align:center">
                    """ +mensaje_boleta+"""
                    </div>
                    <div style="text-align:center">
                        Receta y factura revisada por: </br></br></br>
                        ---------------------------------------
                    </div>
                    <div style="text-align:center">
                        Empacado por:: </br></br></br>
                        ---------------------------------------
                    </div>
                    <div style="text-align:center">
                        Revisado: </br></br></br>
                        ---------------------------------------
                    </div>
                    </div>     
                    </div>
                    """
        res = {"name": text_val, }
        wizard_id = self.env['account.print.receipt'].create(res)

        context = self._context.copy()
        return {'name': 'Factura Tiquete', 'view_type': 'form', 'view_mode': 'tree', 'views': [(view_id, 'form')],
                'res_model': 'account.print.receipt', 'view_id': view_id, 'type': 'ir.actions.act_window',
                'res_id': wizard_id.id, 'target': 'new', 'context': context, }

    @api.multi
    def print_new_receipt(self):
        view_id = self.env.ref('account_receipt.account_print_receipt_form').id

        user_tz = self.env.user.tz or pytz.utc
        local = pytz.timezone(user_tz)
        date_cr = datetime.strftime(pytz.utc.localize(
            datetime.strptime(str(self.write_date)[:-7], tools.DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(local),
            "%d-%m-%Y")
        text_val = """
                <div class="pos-sale-ticket">
                    <div style="font-size: 20px">
                    <div style="text-align:center">
                    <img class="pos-center-align" src="/web/binary/company_logo"/><br/>
                    """ + self.company_id.name + """<br/>
                    <br/>Ced. Jur. """ + self.company_id.vat + """<br/>
                    Tel.:""" + self.company_id.phone + """<br/>
                    """ + self.company_id.street + """<br/>
                    Email:""" + self.company_id.email + """<br/>
                    <div class="pos-center-align">Factura: """ + self.number + """</div>
                    <div class="pos-center-align">Fecha: """ + date_cr + """</div>
                    Cliente: """+self.partner_id.ref+" " + self.partner_id.name + """<br/>
                    <br/>
                    </div>
                    <div style="font-size: 28px">
            		<table class="receipt-orderlines">
            		    
                	<tbody>
        """
        for line in self.invoice_line_ids:
            if line.display_type:
                text_val += "<tr><td colspan='2'>" + line.name+ "</td></tr>"
            else:
                impuesto = ''
                if line.invoice_line_tax_ids:
                    for imp in line.invoice_line_tax_ids:
                        if len(impuesto) > 0:
                            impuesto = impuesto + ', ' + imp.display_name
                        else:
                            impuesto = imp.display_name
                text_val += "<tr><td colspan='2'>" + line.product_id.display_name + "</td></tr><tr><td> Cant: " + str(
                    '{:,}'.format(line.quantity)) + "</td><td class='pos-right-align'>" + str(
                    '{:,}'.format(line.price_subtotal)) + "</td></tr>"
                if len(impuesto) == 0:
                    text_val += "<tr><td>Exento IVA</td><tr>"
                else:
                    text_val += "<tr><td>""" + impuesto + """</td><tr>"""
                text_val += "<tr><td><br/></td><tr>"
        text_val += """
            </tbody></table>
            </div>
            <br/>
            <table class="receipt-orderlines">
    			<colgroup>
                    <col width="50%"/>
                    <col width="50%"/>
                </colgroup>
                <tbody><tr>
                    <td>Subtotal:</td>
                    <td class="pos-right-align">
                       """ + str('{:,}'.format(self.amount_untaxed)) + """
                    </td>
                </tr>
                
                    <tr>
                        <td>IVA</td>
                        <td class="pos-right-align">
                            """ + str('{:,}'.format(self.amount_tax)) + """
                        </td>
                    </tr>
                <tr class="emph">
                    <td>Total:</td>
                    <td class="pos-right-align">
                       """ + str('{:,}'.format(self.amount_total)) + """
                    </td>
                </tr>
            </tbody></table>
            <div style="text-align:center">
                 Autorizado mediante oficio<br/>
                No. DGT-R-033-2019<br/>
                de fecha 20-06-2019 de la D.G.T.<br/>
            </div>
            </div>
            </div>
            """
        res = {"name": text_val, }
        wizard_id = self.env['account.print.receipt'].create(res)

        context = self._context.copy()
        return {'name': 'Factura Tiquete', 'view_type': 'form', 'view_mode': 'tree', 'views': [(view_id, 'form')],
                'res_model': 'account.print.receipt', 'view_id': view_id, 'type': 'ir.actions.act_window',
                'res_id': wizard_id.id, 'target': 'new', 'context': context, }

    @api.multi
    def print_orden_despacho(self):

        user_tz = self.env.user.tz or pytz.utc
        local = pytz.timezone(user_tz)
        date_cr = datetime.strftime(pytz.utc.localize(
            datetime.strptime(str(self.write_date)[:-7], tools.DEFAULT_SERVER_DATETIME_FORMAT)).astimezone(local),
                                    "%d-%m-%Y")
        view_id = self.env.ref('account_receipt.account_print_receipt_form').id

        text_val = """
                    <div class="pos-sale-ticket">
                        <div style="font-size: 20px">	
                        <div style="text-align:center">
                        <img class="pos-center-align" src="/web/binary/company_logo"/>
                        <br/><br/><b>ORDEN DESPACHO</b><br/>
                        """ + self.company_id.name + """<br/>
                        <br/>Ced. Jur. """ + self.company_id.vat + """<br/>
                        Tel.:""" + self.company_id.phone + """<br/>
                        """ + self.company_id.street + """<br/>
                        Email:""" + self.company_id.email + """<br/>
                        <div class="pos-center-align">Factura: """ + self.number + """</div>
                         <div class="pos-center-align">Fecha: """ + date_cr + """</div>
                    Cliente: """+self.partner_id.ref+" " + self.partner_id.name + """<br/>
                        <br/>
                        <div>
                        </div>
                        <div style="font-size: 28px">
                		<table class="receipt-orderlines">
    		                <td></td>
                    	<tbody>
            """
        for line in self.invoice_line_ids:
            impuesto = ''
            if line.invoice_line_tax_ids:
                nombre=line.name
                indicaciones=""
                if line.indicaciones:
                    indicaciones+=line.indicaciones
                for imp in line.invoice_line_tax_ids:
                    if len(impuesto) > 0:
                        impuesto = impuesto + ', ' + imp.display_name
                    else:
                        impuesto = imp.display_name
            text_val += "<tr><td>" + nombre+ "</td></tr>"
            text_val += "<tr><td class='pos-left-align'>" + indicaciones + "</td></tr>"
            text_val += "<tr><td><br/></td></tr>"
        text_val += """
                </tbody></table>
                <br/>
                </div>
                <div style="text-align:center">
                <br/>
                </div>
                </div>
                </div>
                """
        res = {"name": text_val, }
        wizard_id = self.env['account.print.receipt'].create(res)

        context = self._context.copy()
        return {'name': 'Factura Tiquete', 'view_type': 'form', 'view_mode': 'tree', 'views': [(view_id, 'form')],
                'res_model': 'account.print.receipt', 'view_id': view_id, 'type': 'ir.actions.act_window',
                'res_id': wizard_id.id, 'target': 'new', 'context': context, }
