# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from escpos import NetworkConnection
from escpos.impl.epson import GenericESCPOS
from odoo.exceptions import UserError, RedirectWarning, ValidationError
from datetime import date
import logging
import dateutil.parser
import unidecode

_logger = logging.getLogger(__name__)

class costoArticulo(Exception):
     def __init__(self, valor):
         self.valor = valor
     def __str__(self):
         return repr(self.valor)

class AccountInvoice(models.Model):
    _inherit="account.invoice"
    @api.multi
    def _compute_picking_count(self):
        for pos in self:
            pickings = pos.order_ids.mapped('picking_id').filtered(lambda x: x.state != 'done')
            pos.picking_count = len(pickings.ids)

    @api.multi
    def action_invoice_open(self):
        res = super(AccountInvoice, self).action_invoice_open()
        imprimir=False
        #try:

        #Revisa que el tipo de documento a imprimir es una factura
        if self.type=='out_invoice':
            #import pudb; pudb.set_trace()
            #Revisa cambio en forma de pago
            if not self.partner_id.property_payment_term_id==self.payment_term_id:
                error=0/0
            #Verifica costos en articulos
            #arts = self.invoice_line_ids
            for a in self.invoice_line_ids:
                producto_obj = self.env['product.product']
                articulo = producto_obj.search([('id', '=', a.product_id.id)])
                if articulo.product_tmpl_id.standard_price==0.0 and articulo.product_tmpl_id.type=='product':
                    raise ValueError('El articulo: ' + a.name + ' no tiene COSTO')


            #for a in self.invoice_line_ids:
            #    if a.id.standard_price>0.0 and a.id.type=='product':


                #articulo = producto_obj.search([('id','=',a.product_id.id)])
                #if not articulo.standard_price>0.0 and articulo.type=='product':
                 #   print articulo.name
                 #   print articulo.default_code

            ##Verifica que la factura tenga un origen
            #if self.origin.find('PV')==-1:
            orden_venta=self.env['sale.order'].search([('name','=',self.origin)])
            ##raise  ValueError('orden: '+str(orden_venta.picking_ids.id)+' ')
            id_pack_operations=[]
            #id_pack_operations=self.env['stock.picking'].browse([orden_venta.picking_ids.id.pack_operation_ids.id])
            #for op in self.env['stock.pack.operation'].browse(id_pack_operations):
            #    op.unlink
            for pick in orden_venta.picking_ids:
                salida_inventario=self.env['stock.picking'].search([('id','=',pick.id)])
                for s in salida_inventario:
                    #print '------------*****---------'
                    #print '------------*****---------'
                    #print str(s.origin)+' '+str(s.name)
                    #print '------------*****---------'
                    #print '------------*****---------'
                    self.env.cr.execute("DELETE FROM stock_move_operation_link WHERE operation_id IN (SELECT id FROM stock_pack_operation WHERE picking_id=%d)" % (s.id))
                    self.env.cr.execute("DELETE FROM stock_pack_operation WHERE picking_id=%d" % (s.id))
                    self.env.cr.execute("DELETE FROM stock_move WHERE picking_id=%d" % (s.id))
                    self.env.cr.execute("DELETE FROM stock_picking WHERE id=%d" % (s.id))

                #

            #for picking in self.env['stock.picking'].browse(orden_venta.picking_ids.id):
            #    print str(picking.id)
                #for o_p in picking.pack_operation_ids:
                #    pass
                #    o_p.unlink
                #for operacion_picking in self.env['stock.pack.operation'].browse([picking.pack_operation_ids.id]):
                #    operacion_picking.unlink
                #picking.unlink

            #salida_inventario_pack=self.env['stock.pack.operation'].search([('id','=',salida_inventario.pack_operation_ids.id)])
            #s_i=self.env['stock.pack.operation'].browse([salida_inventario.pack_operation_ids.id])
            #for s_i in self.env['stock.pack.operation'].browse([salida_inventario.pack_operation_ids.id]):
                #s_i.ensure_one()
                #s_i.unlink()
            #salida_inventario_pack.unlink()
            #salida_inventario.unlink()

            #-------------------------
            #self.action_stock_transfer()
            #for inv in self.env['stock.picking'].browse(self.invoice_picking_id.id):
            #    inv.ensure_one()
                # If still in draft => confirm and assign
            #    if inv.state == 'draft':
            #        inv.action_confirm()
            #        if inv.state != 'assigned':
            #            inv.action_assign()
            #            if inv.state != 'assigned':
            #                raise UserError(_("Could not reserve all requested products. Please use the \'Mark as Todo\' button to handle the reservation manually."))
            #    for pack in inv.pack_operation_ids:
            #        if pack.product_qty > 0:
            #            pack.write({'qty_done': pack.product_qty})
            #        else:
            #            pack.unlink()
            #    inv.do_transfer()
            #------------------------

            #Inicia la impresion
            if imprimir:
                conn = NetworkConnection.create('192.168.0.11:9100')
                printer = GenericESCPOS(conn)
                printer.init()
                printer.text_center('Agencia Stihl Guapiles')
                printer.text_center('Ced. Jur. 3-101-616131 S.A.')
                printer.lf()
                printer.text_center('Tel: 2710-7159')
                printer.text_center('Guapiles, 300m Oeste del INS')
                printer.lf()
                printer.justify_left()
                descuento_aplicado=0.0
                #printer.text('**PRUEBA***')
                printer.text('Factura: '+self.number)
                dias=self.payment_term_id.line_ids.days
                if(dias==0):
                    printer.text('Forma de pago: CONTADO')
                else:
                    printer.text('Forma de pago: CREDITO')
                fecha_factura=dateutil.parser.parse(self.date_invoice)
                printer.text('Fecha:   '+fecha_factura.strftime('%d/%m/%y'))
                fecha_vencimiento=dateutil.parser.parse(self.date_due)
                printer.text('Vencimiento: '+fecha_vencimiento.strftime('%d/%m/%y'))
                printer.text('Cliente: '+unidecode.unidecode(self.nombre_cliente))
                printer.text('Vendedor: '+self.user_id.name)
                printer.text('Moneda: Colones')
                printer.lf()
                printer.text_center('----------------------------------------')
                articulos = self.invoice_line_ids
                for articulo in articulos:
                    descuento_aplicado+=(articulo.price_unit*articulo.quantity)*(articulo.discount/100)
                    printer.justify_left()
                    printer.text(unidecode.unidecode(articulo.name))
                    printer.text(' P/U '+'{:,}'.format(articulo.price_unit)+'  Cant: '+str(articulo.quantity))
                    printer.justify_right()
                    printer.text('SubT: '+'{:,}'.format(articulo.price_subtotal))
                printer.text_center('----------------------------------------')
                printer.justify_left()
                if(descuento_aplicado > 0.0):
                    printer.text('Descuento aplicado: '+'{:,}'.format(descuento_aplicado))
                #printer.lf()
                printer.justify_right()
                printer.text('SubTotal: CRC '+'{:,}'.format(self.amount_untaxed))
                printer.text('Impuesto: CRC '+'{:,}'.format(self.amount_tax))
                printer.text('Total: CRC '+'{:,}'.format(self.amount_total))
                printer.lf(5)
                if(not isinstance(self.comment,bool)):
                    printer.text(unidecode.unidecode(self.comment))
                printer.lf(2)
                printer.text_center('Esta factura constituye un titulo ejecu-tivo.El deudor da por aceptadas las     condiciones del codigo de comercio')
                printer.text_center('Articulo numero 460')
                printer.text_center('Esta factura devenga interes del 4% men-sual despues de su vencimiento.El deudor  renuncia a su domilicio y tramites de jucio ejecutivo en caso de ejecucion.')
                printer.lf()
                printer.text_center('________________________________________')
                printer.text_center('Nombre')
                printer.lf()
                printer.text_center('________________________________________')
                printer.text_center('Firma')
                printer.lf()
                printer.text_center('________________________________________')
                printer.text_center('Cedula')
                printer.justify_left()
                printer.text_center('Emitida por: '+self.env.user.name)
                printer.text_center('Autorizado mediante oficio')
                printer.text_center('No. 4521000005891')
                printer.text_center('de fecha 10-08-12 de la D.G.T.')
                printer.lf()
                printer.lf(10)
            return res
#        except ValueError as e:
#            raise ValidationError('El articulo: '+a.name+' no tiene COSTO')
#        except ZeroDivisionError as e:
#            raise ValidationError('No se puede cambiar la condicion de pago')
#        except:
#            raise  ValidationError('Error de impresion')

class account_payment(models.Model):
    _inherit="account.payment"

    @api.multi
    def post(self):
        res = super(account_payment, self).post()
        credito=True
        devolucion_dinero=False
        for f in self.invoice_ids:
            #_logger.debug('Entro!'+repr(f.payment_term_id.name))
            if(not isinstance(f.payment_term_id.name,bool)):
                if('inmediato' in f.payment_term_id.name):
                    credito=False
            if(f.type=='out_refund'):
                devolucion_dinero=True

        if(devolucion_dinero):
            try:
                conn = NetworkConnection.create('192.168.0.11:9100')
                printer = GenericESCPOS(conn)
                printer.init()
                printer.text_center('Agencia Stihl Guapiles')
                printer.text_center('Ced. Jur. 3-101-616131 S.A.')
                printer.lf()
                printer.text_center('Tel: 2710-7159')
                printer.text_center('Guapiles, 300m Oeste del INS')
                #printer.text_center('PRUEBA!!')
                printer.lf()
                printer.justify_left()
                printer.text('NC Dinero CLIENTE: '+self.name)
                printer.text('Fecha: '+self.payment_date)
                printer.text('Cliente: '+unidecode.unidecode(self.partner_id.name))
                printer.lf()
                printer.text('Devolucion de dinero por ')
                printer.text('los siguientes documentos: ')
                printer.lf()
                printer.text('Factura                     Monto Pagado')
                printer.text_center('----------------------------------------')
                for f in self.invoice_ids:
                    printer.justify_left()
                    printer.text(f.number)
                printer.justify_right()
                printer.text('{:,}'.format(self.amount))
                printer.text_center('----------------------------------------')
                printer.lf()
                printer.text('Tipo: '+unidecode.unidecode(self.journal_id.display_name))
                printer.lf(3)
                printer.text_center('________________________________________')
                printer.text_center('Firma del cliente')
                printer.lf(10)
            except:
                raise  ValidationError('Error de impresion')

        if(self.payment_type=="inbound" and credito and not devolucion_dinero and self.partner_id.customer):
            if self.intereses>0 and not self.intereses_revisado:
                raise  ValidationError('No se revisaron los intereses')
            try:
                conn = NetworkConnection.create('192.168.0.11:9100')
                printer = GenericESCPOS(conn)
                printer.init()
                printer.text_center('Agencia Stihl Guapiles')
                printer.text_center('Ced. Jur. 3-101-616131 S.A.')
                printer.lf()
                printer.text_center('Tel: 2710-7159')
                printer.text_center('Guapiles, 300m Oeste del INS')
                #printer.text_center('PRUEBA!!')
                printer.lf()
                printer.justify_left()
                printer.text('RECIBO: '+self.name)
                printer.text('Fecha: '+self.payment_date)
                printer.text('Cliente: '+unidecode.unidecode(self.partner_id.name))
                printer.lf()
                printer.text('Por cancelacion de Recibo de dinero a')
                printer.text('los siguientes documentos: ')
                printer.lf()
                printer.text('Factura                     Monto Pagado')
                printer.text_center('----------------------------------------')
                for f in self.invoice_ids:
                    printer.justify_left()
                    printer.text(f.number)
                    printer.text(' -->Nuevo Saldo: '+'{:,}'.format(f.residual))
                printer.justify_right()
                printer.text('{:,}'.format(self.amount))
                printer.text_center('----------------------------------------')
                printer.lf()
                printer.text('Forma de pago: '+unidecode.unidecode(self.journal_id.display_name))
                printer.lf(3)
                printer.text_center('________________________________________')
                printer.text_center('Por 3-101-616131 S.A.')
                printer.lf(10)
            except:
                raise  ValidationError('Error de impresion')
        return res




# class imp_stihl(models.Model):
#     _name = 'imp_stihl.imp_stihl'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100
