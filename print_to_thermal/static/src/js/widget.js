odoo.define('print_to_thermal.widget', function (require) {
  var core = require('web.core');
  var Widget = require('web.Widget');
  var sidebar = require('web.Sidebar');
  var Device = require('print_to_thermal.device');
  var ActionManager= require('web.ActionManager');
  var crash_manager = require('web.crash_manager');
  var framework = require('web.framework');
  var utils = require('web.utils');
  var session = require('web.session');

  var QWeb = core.qweb;
  var thermal_printers = {};
  var _t = core._t;

  ActionManager.include({

      _downloadReportThermal: function (url, actions) {
          var self = this;
          var progressHandler = function (deferred) {
                return function (state) {
                    if(state.lengthComputable) {
                        deferred.notify({
                            h_loaded: utils.human_size(state.loaded),
                            h_total : utils.human_size(state.total),
                            loaded  : state.loaded,
                            total   : state.total,
                            pcent   : Math.round((state.loaded/state.total)*100)
                        });
                    }
                };
            };
            var cloned_action = _.clone(actions);

            framework.blockUI();
            var def = $.Deferred();
            var thermal_url = 'report/thermal/' + cloned_action.report_name;
            if (_.isUndefined(cloned_action.data) ||
                _.isNull(cloned_action.data) ||
                (_.isObject(cloned_action.data) && _.isEmpty(cloned_action.data)))
            {
                if (cloned_action.context.active_ids) {
                    thermal_url += "/" + cloned_action.context.active_ids.join(',');
                }
            } else {
                thermal_url += '?options=' + encodeURIComponent(JSON.stringify(cloned_action.data));
                thermal_url += '&context=' + encodeURIComponent(JSON.stringify(cloned_action.context));
            }
            var Def = $.Deferred();
            var postData = new FormData();
            if (core.csrf_token) {
               postData.append('csrf_token', core.csrf_token);
            }

            var xhr = new XMLHttpRequest();
            if(xhr.upload) xhr.upload.addEventListener('progress', progressHandler(Def), false);

            var ajaxDef = $.ajax(thermal_url, {
                xhr: function() {return xhr;},
                data: postData,
                processData:    false,
                contentType:    false,
                type:           'POST'
            }).then(function (data) {
                  var xml = new XMLSerializer().serializeToString(data.documentElement);
                  var active_printer = localStorage.getItem("active_printer");
                  if(thermal_printers[active_printer] && thermal_printers[active_printer].proxy.get('status').status === 'connected'){
                        thermal_printers[active_printer].print_receipt(xml);
                    }else{
                        window.alert(_t("There is not an active printer, check printers connection"));
                    }
             }).fail(function (data) {crash_manager.rpc_error.bind(crash_manager); Def.reject(data);});
            framework.unblockUI();
            return def;
      },
      _triggerDownload: function (action, options, type) {
            var self = this;
            var reportUrls = this._makeReportUrls(action);
            if (type === "qweb-thermal" || type === 'qweb-thermal_remote') {
                return self._downloadReportThermal(reportUrls[type], action).then(function () {
                    if (action.close_on_report_download) {
                        var closeAction = {type: 'ir.actions.act_window_close'};
                        return self.doAction(closeAction, _.pick(options, 'on_close'));
                    } else {
                        return options.on_close();
                    }
                });
            }
            return this._super.apply(this, arguments);
        },
    _makeReportUrls: function (action) {
            var reportUrls = this._super.apply(this, arguments);
            reportUrls.thermal = '/report/thermal/' + action.report_name;
            return reportUrls;
        },
    _executeReportAction: function (action, options) {
            var self = this;
            if (action.report_type === 'qweb-thermal' || action.report_type === 'qweb-thermal_remote') {
                return self._triggerDownload(action, options, 'qweb-thermal');
            }
            return this._super.apply(this, arguments);
      }
  });

  var PrintToThermal = Widget.extend( {
    template: "PrintToThermal",

    events: {
        'click .btn-thermal-status' : 'on_click_status',
    },
    init: function (parent, options) {
        this._super.apply(this, arguments);
        var self = this;
        self.options = _.defaults(options || {}, {});
        self.init_connection();
    },
    init_connection: function(){
        this.proxy = new Device.ProxyDevice(this, this.options);
        return this.proxy.autoconnect({
              force_ip: this.options.url || undefined,
          });
    },
    start: function() {
        this._render_value();
        return this._super();
    },
    set_value: function(){
      this._render_value();
    },
    render_value: function() {
      console.log("render_value");
    },
    _update_control_panel: function () {
        this.update_control_panel({
            breadcrumbs: this.action_manager.get_breadcrumbs(),
            cp_content: {
                $buttons: this.$buttons,
            },
        });
        this._update_control_panel_buttons();
    },
    print_receipt: function(receipt){
      if(this.proxy.get('status').status !== 'connecting' && this.proxy.get('status').status !== 'connected'){
        this.proxy.reconnect().then(function(){
            this.proxy.print_receipt(receipt);
          });
        }else{
          this.proxy.print_receipt(receipt);
        }
    },
    disconnect: function(){
      this.proxy.disconnect();
    }
  });

  sidebar.include({
    init: function(parent, options){
      var self = this;
      this._super.apply(this, arguments);
      if (options.sections){
        return ;
      }
      var thermal = false;
      if(self.items.print){
        for(var i in self.items.print){
          if(self.items.print[i].action.report_type === 'qweb-thermal' || self.items.print[i].action.report_type === 'thermal_remote'){
            thermal = true;
          }
        }
      }
      if (thermal){
        self.sections.push(
                {
                  name: 'thermal',
                  label: _.str.sprintf(_t("Thermal Printers"))
                }
            );
        self.items.thermal = [];

        var params = {}
        function draw_widget(){
          var printers = params['thermal.printer_urls'].split(',');
          printers.push(params['thermal.default_printer']);
          var default_printer = localStorage.getItem("active_printer");
          if (!default_printer || default_printer === '' || default_printer === 'false'){
              default_printer = params['thermal.default_printer'];
          }
          for (var x in printers){
            if (printers[x]){
              var data = {'url': printers[x], 'timeout_val': params['thermal.time_alive_printer']};
              if (printers[x] == default_printer){
                data.keptalive = true;
              }
              var thermal_printer = new PrintToThermal(self, data);
              thermal_printers[printers[x]] = thermal_printer;
              var status = thermal_printer.proxy.get('status').status;
              self.items.thermal.push(
                {
                  label: _t('Set as active: ' + printers[x] + ' - &nbsp;<span id="thermal_') + printers[x].replace('.', '_').replace('.', '_').replace('.', '_') + '">' + status + "</span>",
                  thermal: printers[x]
                }
              );
            }
          }
          localStorage.setItem('active_printer', default_printer);
          self._redraw();
        }
        var i = 0;
        _.each(['thermal.default_printer', 'thermal.printer_urls', 'thermal.time_alive_printer'], function(param){
          parent._rpc(
              {
                model: 'ir.config_parameter',
                method: 'get_param',
                context: options.env.context,
                args: [param]}
          ).then(function(result){
              params[param] = result;
              i++;
              if(i===3){
                draw_widget();
              }
            })
          })
      }
    },
    start: function(){
      this._super.apply(this, arguments);
      var self = this;
      self.$el.on('click', '.btn_thermal_status', function() {
           _.each(thermal_printers, function(printer, printer_o){
              self.$el.find("#thermal_" + printer.options.url.replace('.', '_').replace('.', '_').replace('.', '_')).html(printer.proxy.get('status').status)
           })
        });
    },
    _onDropdownClicked: function (event) {
      var section = $(event.currentTarget).data('section');
      var index = $(event.currentTarget).data('index');
      var item = this.items[section][index];
      if (item.thermal){
        var therm =localStorage.getItem('active_printer');
        thermal_printers[therm].proxy.killalive();
        thermal_printers[item.thermal].proxy.keepalive();
        localStorage.setItem('active_printer', item.thermal);
        event.preventDefault();
      }else{
        this._super.apply(this, arguments);
      }
    }
  });
});
