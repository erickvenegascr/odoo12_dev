# -*- coding: utf-8 -*-
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api, _
from datetime import timedelta
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import logging

MAP_INVOICE_TYPE_PARTNER_TYPE = {
	'out_invoice': 'customer',
	'out_refund': 'customer',
	'in_invoice': 'supplier',
	'in_refund': 'supplier',
}
# Since invoice amounts are unsigned, this is how we know if money comes in or goes out
MAP_INVOICE_TYPE_PAYMENT_SIGN = {
	'out_invoice': 1,
	'in_refund': 1,
	'in_invoice': -1,
	'out_refund': -1,
}

_logger = logging.getLogger(__name__)

class especifico_stihl(models.Model):
	_inherit = 'res.partner'
	# = fields.Char('Codigo', index=True, required=False, translate=False)
	#cedula = fields.Char('cedula', index=True, required=False, translate=False)
class AccountInvoice(models.Model):
	_inherit = 'account.invoice'
	payment_term_id = fields.Many2one('account.payment.term', string='Payment Terms',readonly=False, states={}, oldname='payment_term',
	help="If you use payment terms, the due date will be computed automatically at the generation "
			 "of accounting entries. If you keep the payment term and the due date empty, it means direct payment. "
			 "The payment term may compute several due dates, for example 50% now, 50% in one month.")

	@api.onchange('payment_term_id')
	def cambio_condicion(self):
		if self.type=='out_invoice':
			if not self.partner_id.property_payment_term_id==self.payment_term_id:
				try:
					error=0/0
				except Exception as e:
					raise ValidationError('No se puede cambiar la condicion de pago')

class SaleOrder(models.Model):
	_inherit = "sale.order"
	user_id = fields.Many2one('res.users', string='Salesperson', track_visibility='onchange',
		readonly=True, states={'draft': [('readonly', False)]},
		default=lambda self: False)
	nombre_cliente = fields.Char(readonly=False,store=True,compute="_nombre_cliente",string="Nombre")
	@api.onchange('partner_id')
	def _nombre_cliente(self):
		if (not self.partner_id==False):
				self.nombre_cliente=self.partner_id.name
	@api.onchange('user_id','partner_id') # if these fields are changed, call method
	def check_change(self):
		if not(len(self.user_id) > 0) and (len(self.partner_id)>0):
			return{
				'warning':{
				'title':'Asignar Vendedor!',
				'message':''
				}
			}
	@api.multi
	def action_confirm(self):
		res = super(SaleOrder, self).action_confirm()
		try:
			for order in self:
				if not(len(order.user_id) > 0) and (len(order.partner_id)>0):
					error=0/0
		except:
			raise ValidationError('Falta Vendedor')
		return res
	@api.onchange('payment_term_id')
	def cambio_condicion(self):
		if not self.partner_id.property_payment_term_id==self.payment_term_id:
			try:
				error=0/0
			except Exception as e:
				raise ValidationError('No se puede cambiar la condicion de pago')

	class SaleOrderLine(models.Model):
		_inherit = "sale.order.line"
		@api.multi
		def _prepare_invoice_line(self, qty):
			"""
			Prepare the dict of values to create the new invoice line for a sales order line.

			:param qty: float quantity to invoice
			"""
			self.ensure_one()
			res = {}
			account = self.product_id.property_account_income_id or self.product_id.categ_id.property_account_income_categ_id
			if not account:
				raise UserError(_('Please define income account for this product: "%s" (id:%d) - or for its category: "%s".') %
					(self.product_id.name, self.product_id.id, self.product_id.categ_id.name))

			fpos = self.order_id.fiscal_position_id or self.order_id.partner_id.property_account_position_id
			if fpos:
				account = fpos.map_account(account)

			res = {
				'name': self.name,
				'sequence': self.sequence,
				'origin': self.order_id.name,
				'account_id': account.id,
				'price_unit': self.price_unit,
				'quantity': qty,
				'discount': self.discount,
				'uom_id': self.product_uom.id,
				'product_id': self.product_id.id or False,
				'layout_category_id': self.layout_category_id and self.layout_category_id.id or False,
				'invoice_line_tax_ids': [(6, 0, self.tax_id.ids)],
				'account_analytic_id': self.order_id.project_id.id,
				'analytic_tag_ids': [(6, 0, self.analytic_tag_ids.ids)],
			}
			return res

	@api.multi
	def _prepare_invoice(self):
		"""
		Prepare the dict of values to create the new invoice for a sales order. This method may be
		overridden to implement custom invoice generation (making sure to call super() to establish
		a clean extension chain).
		"""
		self.ensure_one()
		journal_id = self.env['account.invoice'].default_get(['journal_id'])['journal_id']
		if not journal_id:
			raise UserError(_('Please define an accounting sale journal for this company.'))
		invoice_vals = {
			'name': self.client_order_ref or '',
			'origin': self.name,
			'type': 'out_invoice',
			'account_id': self.partner_invoice_id.property_account_receivable_id.id,
			'partner_id': self.partner_invoice_id.id,
			'partner_shipping_id': self.partner_shipping_id.id,
			'journal_id': journal_id,
			'currency_id': self.pricelist_id.currency_id.id,
			'comment': self.note,
			'payment_term_id': self.payment_term_id.id,
			'fiscal_position_id': self.fiscal_position_id.id or self.partner_invoice_id.property_account_position_id.id,
			'company_id': self.company_id.id,
			'user_id': self.user_id and self.user_id.id,
			'team_id': self.team_id.id,
			'nombre_cliente': self.nombre_cliente,
			'create_stock_moves': True
		}
		return invoice_vals

class account_invoice(models.Model):
	_inherit = 'account.invoice'
	user_id = fields.Many2one('res.users', string='Salesperson', track_visibility='onchange',
		readonly=True, states={'draft': [('readonly', False)]},
		default=lambda self: False)
	phone = cod_cliente = fields.Char('Telefono',compute="_telefono", required=False, translate=False)
	dias_vencida = fields.Integer('Vencimiento',compute="_dias_vencida", required=False, translate=False)
	intereses = fields.Float(string='Monto Interes',digits=0,compute="_interes", required=False, translate=False)
	saldo_intereses=fields.Float(string='Saldo + Interes',digits=0,compute="_saldo", required=False, translate=False)
	total_mercaderia=fields.Float(string='Total Mercaderia',digits=0,compute="_mercaderia", required=False, translate=False)
	total_mercaderia=fields.Float(string='Total Mercaderia',digits=0,compute="_mercaderia", required=False, translate=False)
	subtotal_exento = fields.Float(compute="_subtotal_exento",string="SubTotal Exento")
	subtotal_gravado = fields.Float(compute="_subtotal_gravado",string="SubTotal Gravado")
	@api.one
	def _telefono(self):
		self.phone= self.partner_id.phone
	@api.one
	def _dias_vencida(self):
		#d_frm_obj = datetime.strptime(fields.Datetime.now(), DF)
		if self.state not in ['draft'] and self.residual>0.0:
			d_frm_obj = fields.Datetime.from_string(fields.Datetime.now())
			d_to_obj=datetime.strptime(self.date_invoice,DF)
			if not (self.date_due == False):
				d_to_obj = datetime.strptime(self.date_due,DF)
			duration = d_frm_obj - d_to_obj
			self.dias_vencida=duration.days
			#_logger.error('Factura:'+self.number+' Estado:'+self.state+' dias'+str(duration.days))
			#self.dias_vencida=duration
	@api.one
	def _interes(self):
		#d_frm_obj = datetime.strptime(fields.Datetime.now(), DF)
		if self.state not in ['draft'] and self.residual>0.0 and self.partner_id.customer:
			d_frm_obj = fields.Datetime.from_string(fields.Datetime.now())
			d_to_obj=datetime.strptime(self.date_invoice,DF)
			if not (self.date_due == False):
				d_to_obj = datetime.strptime(self.date_due,DF)
			duration = d_frm_obj - d_to_obj
			dias_vencidos=duration.days
			if dias_vencidos>0:
				#self.intereses=(self.residual*0,015)*dias_vencidos
				monto_intereses=(self.residual*0.0015)*dias_vencidos
				self.intereses=monto_intereses
				#_logger.error('Factura:'+self.number+' Estado:'+self.state+' Intereses:'+str(monto_intereses))
	@api.one
	def _saldo(self):
		if self.state not in ['draft'] and self.residual>0.0 and self.intereses>0.0:
			self.saldo_intereses=self.intereses+self.residual
	@api.one
	def _mercaderia(self):
		#logger.critical("The name '" + str(record.get('name')) + "' is not okay for us!")
		t_m=0.0
		articulos=self.invoice_line_ids
		for a in articulos:
			#_logger.critical("Producto: " + str(a.name)+" Type: " +str(a.product_id.type))
			if a.product_id.type in ['product']:
				t_m+=a.price_subtotal
		self.total_mercaderia=t_m
	@api.one
	def _subtotal_exento(self):
		exento=0.0
		for linea in self.invoice_line_ids:
			for impuesto in linea.invoice_line_tax_ids:
				if impuesto.amount==0.0:
					exento+=linea.price_subtotal
			if not linea.invoice_line_tax_ids:
				exento+=linea.price_subtotal



		#if self.type in ('out_refund','in_refund'):
		#	exento=exento*-1
		self.subtotal_exento=exento
	@api.one
	def _subtotal_gravado(self):
		gravado=0.0
		for linea in self.invoice_line_ids:
			#if not (linea.invoice_line_tax_ids==False):
			for impuesto in linea.invoice_line_tax_ids:
				if impuesto.amount>0.0:
					gravado+=linea.price_subtotal
		#if self.type in ('out_refund','in_refund'):
		#	gravado=exento*-1
		self.subtotal_gravado=gravado

	#@api.model
	#@api.multi




		#hoy = fields.Datetime.from_string(fields.Datetime.now())
		#vencimiento=fields.Datetime.from_string(self.date_due)
		#dias=vencimiento-hoy
		#self.dias_vencida=dias.day
		#dias = hoy - self.date_due
		#self.dias_vencida=dias.days


class sale_oc(models.Model):
	_inherit = "sale.order"
	fields.Many2one('res.users', string='Salesperson', index=True,
		default=lambda self: False)

class account_payment(models.Model):
	_inherit = "account.payment" #	account.payment
	intereses = fields.Float(string='Monto Interes',digits=0,required=False, translate=False)
	saldo = fields.Float(string='Saldo',digits=0,required=False, translate=False)
	pago_con = fields.Float(string='Pago con',digits=0,required=False, translate=False)
	vuelto = fields.Float(string='Vuelto',digits=0,required=False, translate=False)
	intereses_revisado=fields.Boolean(String='Intereses revisados');

	@api.onchange('intereses')
	def _recalculo_interes(self):
		i=float(self.intereses+self.amount)
		self.saldo=float(i)
	@api.onchange('pago_con')
	def _vuelto(self):
		vuelto=float(self.pago_con-self.amount)
		self.vuelto=float(vuelto)
	@api.model
	def default_get(self, fields):
		rec = super(account_payment, self).default_get(fields)
		invoice_defaults = self.resolve_2many_commands('invoice_ids', rec.get('invoice_ids'))
		if invoice_defaults and len(invoice_defaults) == 1:
			invoice = invoice_defaults[0]
			rec['communication'] = invoice['reference'] or invoice['name'] or invoice['number']
			rec['currency_id'] = invoice['currency_id'][0]
			rec['payment_type'] = invoice['type'] in ('out_invoice', 'in_refund') and 'inbound' or 'outbound'
			rec['partner_type'] = MAP_INVOICE_TYPE_PARTNER_TYPE[invoice['type']]
			rec['partner_id'] = invoice['partner_id'][0]
			#rec['amount'] = invoice['residual']+invoice['intereses']
			rec['amount'] = invoice['residual']
			rec['intereses']= invoice['intereses']
			rec['saldo']= invoice['residual']+invoice['intereses']
		return rec

class Repair(models.Model):
	_inherit="mrp.repair"
	nombre_cliente = fields.Char(readonly=False,store=True,compute="_nombre_cliente",string="Nombre")
	@api.onchange('partner_id')
	def _nombre_cliente(self):
		if (not self.partner_id==False):
				self.nombre_cliente=self.partner_id.name
	@api.multi
	def action_invoice_create(self, group=False):
		""" Creates invoice(s) for repair order.
		@param group: It is set to true when group invoice is to be generated.
		@return: Invoice Ids.
		"""
		res = dict.fromkeys(self.ids, False)
		invoices_group = {}
		InvoiceLine = self.env['account.invoice.line']
		Invoice = self.env['account.invoice']
		for repair in self.filtered(lambda repair: repair.state not in ('draft', 'cancel') and not repair.invoice_id):
			if not repair.partner_id.id and not repair.partner_invoice_id.id:
				raise UserError(_('You have to select a Partner Invoice Address in the repair form!'))
			comment = repair.quotation_notes
			if repair.invoice_method != 'none':
				if group and repair.partner_invoice_id.id in invoices_group:
					invoice = invoices_group[repair.partner_invoice_id.id]
					invoice.write({
						'name': invoice.name + ', ' + repair.name,
						'origin': invoice.origin + ', ' + repair.name,
						'comment': (comment and (invoice.comment and invoice.comment + "\n" + comment or comment)) or (invoice.comment and invoice.comment or ''),
					})
				else:
					if not repair.partner_id.property_account_receivable_id:
						raise UserError(_('No account defined for partner "%s".') % repair.partner_id.name)
					invoice = Invoice.create({
						'name': repair.name,
						'nombre_cliente': self.nombre_cliente,
						'user_id': self.user_id.id,
						'origin': repair.name,
						'type': 'out_invoice',
						'account_id': repair.partner_id.property_account_receivable_id.id,
						'partner_id': repair.partner_invoice_id.id or repair.partner_id.id,
						'currency_id': repair.pricelist_id.currency_id.id,
						'comment': repair.quotation_notes,
						'fiscal_position_id': repair.partner_id.property_account_position_id.id,
						'create_stock_moves': True
					})
					invoices_group[repair.partner_invoice_id.id] = invoice
				repair.write({'invoiced': True, 'invoice_id': invoice.id})
				for operation in repair.operations.filtered(lambda operation: operation.to_invoice):
					if group:
						name = repair.name + '-' + operation.name
					else:
						name = operation.name

					if operation.product_id.property_account_income_id:
						account_id = operation.product_id.property_account_income_id.id
					elif operation.product_id.categ_id.property_account_income_categ_id:
						account_id = operation.product_id.categ_id.property_account_income_categ_id.id
					else:
						raise UserError(_('No account defined for product "%s".') % operation.product_id.name)

					invoice_line = InvoiceLine.create({
						'invoice_id': invoice.id,
						'name': name,
						'origin': repair.name,
						'account_id': account_id,
						'quantity': operation.product_uom_qty,
						'invoice_line_tax_ids': [(6, 0, [x.id for x in operation.tax_id])],
						'uom_id': operation.product_uom.id,
						'price_unit': operation.price_unit,
						'price_subtotal': operation.product_uom_qty * operation.price_unit,
						'product_id': operation.product_id and operation.product_id.id or False
					})
					operation.write({'invoiced': True, 'invoice_line_id': invoice_line.id})
				for fee in repair.fees_lines.filtered(lambda fee: fee.to_invoice):
					if group:
						name = repair.name + '-' + fee.name
					else:
						name = fee.name
					if not fee.product_id:
						raise UserError(_('No product defined on Fees!'))

					if fee.product_id.property_account_income_id:
						account_id = fee.product_id.property_account_income_id.id
					elif fee.product_id.categ_id.property_account_income_categ_id:
						account_id = fee.product_id.categ_id.property_account_income_categ_id.id
					else:
						raise UserError(_('No account defined for product "%s".') % fee.product_id.name)

					invoice_line = InvoiceLine.create({
						'invoice_id': invoice.id,
						'name': name,
						'origin': repair.name,
						'account_id': account_id,
						'quantity': fee.product_uom_qty,
						'invoice_line_tax_ids': [(6, 0, [x.id for x in fee.tax_id])],
						'uom_id': fee.product_uom.id,
						'product_id': fee.product_id and fee.product_id.id or False,
						'price_unit': fee.price_unit,
						'price_subtotal': fee.product_uom_qty * fee.price_unit
					})
					fee.write({'invoiced': True, 'invoice_line_id': invoice_line.id})
				invoice.compute_taxes()
				res[repair.id] = invoice.id
		return res



class product_product(models.Model):
	_inherit = "product.template"
	type = fields.Selection([
		('consu', _('Consumable')),
		('service', _('Service')),('product', 'Stockable Product')], string='Product Type', default='product', required=True,
		help='A stockable product is a product for which you manage stock. The "Inventory" app has to be installed.\n'
			 'A consumable product, on the other hand, is a product for which stock is not managed.\n'
			 'A service is a non-material product you provide.\n'
			 'A digital content is a non-material product you sell online. The files attached to the products are the one that are sold on '
			 'the e-commerce such as e-books, music, pictures,... The "Digital Product" module has to be installed.')

	#@api.onchange('intereses')
	#def recalculo_monto(self):
	#    saldo=self.intereses+self.amount
	#    self.amount=saldo
	#raise UserError(_("Hola vamos cerca!!!"))

			#def _interes(self):
			#    facturas=self._get_invoicescreate_stock_moves
			#    i=0.0
			#    for f in facturas:
			#        i+=f.intereses
			#    self.intereses=1.111

			#@api.model







#     _name = 'especifico_stihl.especifico_stihl'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100
