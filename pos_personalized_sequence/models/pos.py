# -*- coding: utf-8 -*-

from odoo import fields, models

class PosConfig(models.Model):
    _inherit = 'pos.config'

    personalized_sequence_id = fields.Many2one('ir.sequence', 'personalized Order IDs Sequence')


class PosOrder(models.Model):
    _inherit = 'pos.order'

    def pos_personalized_sequence(self):
        return self.env['ir.sequence'].next_by_code('pos.order.seq')
