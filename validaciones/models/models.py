# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import requests
import json

class validacion(models.Model):
    _inherit = 'res.partner'
    @api.onchange('vat')
    def validacion(self):
            if bool(self.vat) == True:
                cedula = self.vat
                url = 'https://ssoqa.hacienda.go.cr/atv/api/ae?identificacion=' + str(cedula)
                print(url)
                response = requests.get(url)
                print(response)
                if response.status_code == 200:
                    response_content = json.loads(response.text)
                    list = response_content['actividades']

                    if len(list) == 0:
                        raise UserError("No es contribuyente")
                    else:
                        self.name = response_content.get('nombre')
                        print(response_content)
                else:
                    raise UserError("La identificacion no existe o el sistema no responde ")
            else:
                print("esta vacio")








# class validaciones(models.Model):
#     _name = 'validaciones.validaciones'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100