# -*- encoding: utf-8 -*-
###########################################################################
#    Copyright (C) 2016 - Turkesh Patel. <http://www.almightycs.com>
#
#    @author Turkesh Patel <info@almightycs.com>
###########################################################################

{
    'name' : 'Invoice Refund',
    'category' : 'Accounting',
    'depends' : ['account'],
    'author': 'Almighty Consulting Services',
    'website': 'http://www.almightycs.com',
    'summary': """Customer Invoice and Supplier Invoice Refund in Seprate Menu and Sequence.""",
    'description': """ Customer Invoice and Supplier Invoice Refund in Seprate Menu and Sequence
    Refund Sequence
    Invoice refund
    payment refund
    Refund Menu
    Invoice refund sequence
    """,
    'data': [
        'views/invoice_views.xml',
    ],
    'images': [
        'static/description/odoo_invoice_refund_almightycs_cover.jpg',
    ],
    'application': False,
    'sequence': 1,
    'price': 15,
    'currency': 'EUR',
}
