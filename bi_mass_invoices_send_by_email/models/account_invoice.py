# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) BrowseInfo (http://browseinfo.in)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import SUPERUSER_ID
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools.safe_eval import safe_eval

class wiz_mass_invoice(models.TransientModel):
    _name = 'wiz.mass.invoice'

    @api.multi
    def mass_invoice_email_send(self):
        context = self._context
        active_ids = context.get('active_ids')
        super_user = self.env['res.users'].browse(SUPERUSER_ID)
        for a_id in active_ids:
            account_invoice_brw = self.env['account.invoice'].browse(a_id)
            for partner in account_invoice_brw.partner_id:
                partner_email = partner.email
                if not partner_email:
                    raise UserError(_('%s customer has no email id please enter email address')
                            % (account_invoice_brw.partner_id.name))
                else:
                    template_id = self.env['ir.model.data'].get_object_reference(
                                                                      'account',
                                                                      'email_template_edi_invoice')[1]
                    email_template_obj = self.env['mail.template'].browse(template_id)
                    if template_id:
                        values = email_template_obj.generate_email(a_id, fields=None)
                        #values['email_from'] = super_user.email
                        values['email_to'] = partner.email
                        values['res_id'] = a_id
                        ir_attachment_obj = self.env['ir.attachment']
                        vals = {
                                'name' : account_invoice_brw.number+'.pdf' or "Draft",
                                'type' : 'binary',
                                'datas' : values['attachments'][0][1],
                                'res_id' : a_id,
                                'res_model' : 'account.invoice',
                                'datas_fname' : account_invoice_brw.number+'.pdf' or "Draft",
                        }
                        xml={
                                'name' : account_invoice_brw.number+'.xml' or "Draft",
                                'type' : 'binary',
                                'datas' : account_invoice_brw.xml_comprobante,
                                'res_id' : a_id,
                                'res_model' : 'account.invoice',
                                'datas_fname' : account_invoice_brw.number+'.xml' or "Draft",
                        }
                        xml_respuesta={
                                'name' : 'Respuesta'+account_invoice_brw.number+'.xml' or "Draft",
                                'type' : 'binary',
                                'datas' : account_invoice_brw.xml_respuesta_tributacion,
                                'res_id' : a_id,
                                'res_model' : 'account.invoice',
                                'datas_fname' : 'Respuesta'+account_invoice_brw.number+'.xml' or "Draft",
                        }
                        attachment_id = ir_attachment_obj.create(vals)
                        attachment_id_xml = ir_attachment_obj.create(xml)
                        attachment_id_xml_repuesta = ir_attachment_obj.create(xml_respuesta)
                        # Set boolean field true after mass invoice email sent
                        account_invoice_brw.write({
                                                    'is_invoice_sent' : True
                                                 })
                        mail_mail_obj = self.env['mail.mail']
                        msg_id = mail_mail_obj.create(values)
                        msg_id.attachment_ids=[(6,0,[attachment_id.id,attachment_id_xml.id,attachment_id_xml_repuesta.id])]
                        #msg_id.attachment_ids=[(6,0,[attachment_id_xml.id])]
                        #msg_id.attachment_ids=[(6,0,[attachment_id_xml_repuesta.id])]

                        if msg_id:
                            mail_mail_obj.send([msg_id])

        return True

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    is_invoice_sent = fields.Boolean('Is Invoice Sent',default=False)

class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail(self, auto_commit=False):
        for wizard in self:
            if wizard.attachment_ids and wizard.composition_mode != 'mass_mail' and wizard.template_id:
                new_attachment_ids = []
                for attachment in wizard.attachment_ids:
                    if attachment in wizard.template_id.attachment_ids:
                        new_attachment_ids.append(attachment.copy({'res_model': 'mail.compose.message', 'res_id': wizard.id}).id)
                    else:
                        new_attachment_ids.append(attachment.id)
                    wizard.write({'attachment_ids': [(6, 0, new_attachment_ids)]})

            # Mass Mailing
            mass_mode = wizard.composition_mode in ('mass_mail', 'mass_post')

            Mail = self.env['mail.mail']
            ActiveModel = self.env[wizard.model if wizard.model else 'mail.thread']
            if wizard.template_id:
                # template user_signature is added when generating body_html
                # mass mailing: use template auto_delete value -> note, for emails mass mailing only
                Mail = Mail.with_context(mail_notify_user_signature=False)
                ActiveModel = ActiveModel.with_context(mail_notify_user_signature=False, mail_auto_delete=wizard.template_id.auto_delete)
            if not hasattr(ActiveModel, 'message_post'):
                ActiveModel = self.env['mail.thread'].with_context(thread_model=wizard.model)
            if wizard.composition_mode == 'mass_post':
                # do not send emails directly but use the queue instead
                # add context key to avoid subscribing the author
                ActiveModel = ActiveModel.with_context(mail_notify_force_send=False, mail_create_nosubscribe=True)
            # wizard works in batch mode: [res_id] or active_ids or active_domain
            if mass_mode and wizard.use_active_domain and wizard.model:
                res_ids = self.env[wizard.model].search(safe_eval(wizard.active_domain)).ids
            elif mass_mode and wizard.model and self._context.get('active_ids'):
                res_ids = self._context['active_ids']
            else:
                res_ids = [wizard.res_id]

            batch_size = int(self.env['ir.config_parameter'].sudo().get_param('mail.batch_size')) or self._batch_size
            sliced_res_ids = [res_ids[i:i + batch_size] for i in range(0, len(res_ids), batch_size)]

            if wizard.composition_mode == 'mass_mail' or wizard.is_log or (wizard.composition_mode == 'mass_post' and not wizard.notify):  # log a note: subtype is False
                subtype_id = False
            elif wizard.subtype_id:
                subtype_id = wizard.subtype_id.id
            else:
                subtype_id = self.sudo().env.ref('mail.mt_comment', raise_if_not_found=False).id

            for res_ids in sliced_res_ids:
                batch_mails = Mail
                account_invoice_brw = self.env['account.invoice'].browse(res_ids)
                all_mail_values = wizard.get_mail_values(res_ids)
                # Set boolean field true after mass invoice email sent
                account_invoice_brw.write({
                                            'is_invoice_sent' : True
                                         })
                for res_id, mail_values in all_mail_values.iteritems():
                    if wizard.composition_mode == 'mass_mail':
                        batch_mails |= Mail.create(mail_values)
                    else:
                        ActiveModel.browse(res_id).message_post(
                            message_type=wizard.message_type,
                            subtype_id=subtype_id,
                            **mail_values)

                if wizard.composition_mode == 'mass_mail':
                    batch_mails.send(auto_commit=auto_commit)

        return {'type': 'ir.actions.act_window_close'}


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
