# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)

#models.TransientModel
class WizardCierreCaja(models.TransientModel):
    _name="cierre.wizard"
    _description="Wizard Cierre"

    fecha_cierre = fields.Date("Fecha")

    @api.multi
    def action_report(self):
        """Metodo que llama la lógica que genera el reporte"""
        datas={'ids': self.env.context.get('active_ids', [])}
        res = self.read(['fecha_cierre'])
        res = res and res[0] or {}
        datas['form'] = res
        domain=[]
        domain_detalle_cierre=[]
        if self.fecha_cierre:
            domain=[('date_invoice','=',self.fecha_cierre)
            ,('type','in',['out_refund','out_invoice']),('state','!=','draft')]
            domain_recibos=[('partner_type','=','customer'),('payment_date','=',self.fecha_cierre),('invoice_ids','!=',False),('state','!=','draft')]
            domain_recibos_sin_factura=[('payment_date','=',self.fecha_cierre),('state','!=','draft'),('invoice_ids','=',False),('payment_type','in',['inbound'])]
        fields=['number','date_invoice','amount_total','payment_term_id','type']
        facturas_ob = self.env['account.invoice'].search_read(domain,fields)
        facturas_recorset = self.env['account.invoice'].search(domain)
        cierre_recorset = self.env['cierre.caja'].search([('fecha_inicio', '=', self.fecha_cierre)])
        detalle_cierre={}
        saldo_caja={"efectivo":0.0,"saldo":0.0}
        for cierre in cierre_recorset:
            saldo_caja["saldo"]=cierre.monto_efectivo
            for cierre_detalle in cierre.cierre_ids:
                if cierre_detalle.tipo == 'ingreso':
                    detalle_cierre[str(cierre_detalle.tipo)+' '+str(cierre_detalle.descripcion)]=cierre_detalle.monto
                    saldo_caja["efectivo"]+=cierre_detalle.monto
                else:
                    detalle_cierre[str(cierre_detalle.tipo)+' '+str(cierre_detalle.descripcion)]=cierre_detalle.monto*-1
                    saldo_caja["efectivo"]-=cierre_detalle.monto
        Ventas={"Contado":0.0,"Credito":0.0}
        suma_dev=0.0
        for factura in facturas_recorset:
            es_devolucion = True
            es_nc = False

            for linea in factura.invoice_line_ids:
                if (linea.product_id.name == False):
                    #print factura.number+'linea: '+linea.name+'\n'
                    suma_dev+=factura.amount_total
                    es_devolucion = False
                    es_nc = True
            if factura.payment_term_id.line_ids.days==0:
                if factura.type=="out_refund" and es_devolucion:
                    Ventas["Contado"]+=(factura.amount_total)*-1
                elif  not es_nc:
                    Ventas["Contado"]+=factura.amount_total
            else:
                if factura.type=="out_refund" and es_devolucion:
                    Ventas["Credito"]+=(factura.amount_total)*-1
                elif not es_nc:
                    Ventas["Credito"]+=factura.amount_total
        #print 'Suma Dev: '+str(suma_dev)
        datas['ventas']=Ventas
        #print "Ventas Contado"+"{:,}".format(Ventas["Contado"])
        #print "Ventas Credito"+"{:,}".format(Ventas["Contado"])
        #print datas

        forma_pagos = {}
        forma_pago_recorset = self.env['account.journal'].search([('code','!=','')])
        for fp_r in forma_pago_recorset:
            forma_pagos[fp_r.name] = 0.0
        recibos = {"Credito/Recuperacion":0.0,"Contado":0.0,"Abierto":0.0}
        recibos_recorset = self.env['account.payment'].search(domain_recibos)
        recibos_recorset_sin_factura = self.env['account.payment'].search(domain_recibos_sin_factura)

        for recibo_sin_factura in recibos_recorset_sin_factura:
            if recibo_sin_factura.payment_type=="inbound":
                recibos["Abierto"] += recibo_sin_factura.amount
            else:
                recibos["Abierto"] -= recibo_sin_factura.amount
            if recibo_sin_factura.payment_type=="inbound":
                forma_pagos[recibo_sin_factura.journal_id.name]+=recibo_sin_factura.amount
            else:
                forma_pagos[recibo_sin_factura.journal_id.name]-=recibo_sin_factura.amount

        for recibo in recibos_recorset:
            if recibo.payment_type=="inbound":
                forma_pagos[recibo.journal_id.name]+=recibo.amount
            else:
                forma_pagos[recibo.journal_id.name]-=recibo.amount
            for rf in recibo.invoice_ids:
                #print 'id: '+str(rf.id)+' Factura:'+rf.number+'Tipo: '+ rf.type
                #print 'id: '+str(rf.id)+' Factura:'+rf.number
                if rf.type=="out_refund":
                    #print 'id: '+str(rf.id)+' Devolucion Dinero:'+rf.number
                    recibo.devolucion_dinero = True

                if rf.payment_term_id.line_ids.days==0:
                    recibo.cobro_credito = False
                else:
                    recibo.cobro_credito = True
            if recibo.cobro_credito:
                recibos["Credito/Recuperacion"]+=recibo.amount
            else:
                if recibo.payment_type=="inbound":
                    #print  'recibo'+str(recibo.id)
                    recibos["Contado"]+=recibo.amount
                else:
                    recibos["Contado"]-=recibo.amount

        d = dict.fromkeys(forma_pagos)
        for key in d:
            if forma_pagos[key]==0.0:
                del forma_pagos[key]

        for fp in forma_pagos:
            if fp in ('Efectivo','Devoluciones de Dinero'):
                saldo_caja['efectivo']+=forma_pagos[fp]

        #for p in forma_pagos:
        #    print p, ":", forma_pagos[p]

        datas['forma_pagos']=forma_pagos
        datas['recibos']=recibos
        datas['detalle_cierre']=detalle_cierre
        datas['saldo_caja']=saldo_caja
        datas['saldo_efectivo']=saldo_caja['saldo']-saldo_caja["efectivo"]
        s = datas['form']['fecha_cierre']
        f = "%Y-%m-%d"
        fecha = datetime.strptime(s, f)
        titulo_reporte = 'Cierre caja del '+fecha.strftime("%d/%m/%Y")
        datas['titulo_reporte']=titulo_reporte

        #for f in facturas_ob:
        #    print str(f)+'\n'
        datas['facturas'] = facturas_ob
        return self.env['report'].get_action([], 'cierre_diario', data=datas)
