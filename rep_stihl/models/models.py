# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, RedirectWarning, ValidationError
from datetime import date
import logging
import dateutil.parser
from odoo import tools


class AccountPayment(models.Model):
    _inherit = 'account.invoice'
    nombre_cliente = fields.Char(readonly=False,store=True,compute="_nombre_cliente",string="Nombre")

    @api.onchange('partner_id')
    def _nombre_cliente(self):
        if (not self.partner_id==False):
                self.nombre_cliente=self.partner_id.name


class AccountPayment(models.Model):
    _inherit = 'account.payment'
    cobro_credito = fields.Boolean(readonly=False,store=True,compute="_factura_credito",string="Cobra facturas credito")
    devolucion_dinero = fields.Boolean(readonly=False,store=True,compute="_dev_dinero",string="Devolvio dinero al cliente")

    @api.one
    def _factura_credito(self):
        for f in self.invoice_ids:
            if f.rf.payment_term_id.line_ids.days>0:
                self.cobro_credito=True
                break
            else:
                self.cobro_credito=False
    @api.one
    def _dev_dinero(self):
        for f in self.invoice_ids:
            if f.type=='out_invoice':
                devolucion_dinero = True

class CierreDetalle(models.Model):
    _name = 'cierre.caja.detalle'
    _cierre = 'Detalle Cierre de Caja'
    tipo = fields.Selection([('ingreso','Ingreso'),('egreso','Egreso')],'Tipo')
    descripcion = fields.Char('Descripcion');
    monto = fields.Float(string='Monto')
    cierre_id = fields.Many2one('cierre.caja', string='Referencia cierre',
        ondelete='cascade', index=True)

    #@api.model
    #def create(self, vals):
    #    vals['consecutivo'] = self.env['ir.sequence'].next_by_code('caja.cierre')
    #    return super(Cierre, self).create(vals)

class Cierre(models.Model):
    _name='cierre.caja'
    _cierre='Cierre de caja'
    state = fields.Selection([
            ('draft','Borrador'),
            ('open', 'Aplicado'),
        ], string='Estado', index=True, readonly=True, default='draft',
        track_visibility='onchange', copy=False,
        help="")
    consecutivo = fields.Char("Cierre");
    fecha_inicio = fields.Date("Fecha Inicio")
    fecha_cierre = fields.Date("Fecha Cierre")
    cierre_ids=fields.One2many('cierre.caja.detalle', 'cierre_id', string='Detalle cierre')
    monto_efectivo=fields.Float(string='Efectivo caja')

    @api.onchange('fecha_inicio')
    def _fecha(self):
        self.fecha_cierre=self.fecha_inicio


    @api.model
    def create(self, vals):
        vals['consecutivo'] = self.env['ir.sequence'].next_by_code('caja.cierre')
        return super(Cierre, self).create(vals)



class ProductTemplate(models.Model):
    _inherit = 'product.template'
    margen_costo = fields.Float(readonly=False,store=True,string="% Utilidad")
    precio_ivi = fields.Float(readonly=False,store=True,string="Precio IVI")

    @api.onchange('list_price', 'standard_price')
    def _margen(self):
        if(self.standard_price==0.0):
            pass
        else:
            utilidad=(self.list_price-self.standard_price)/self.standard_price
            self.margen_costo=(utilidad*100)
    @api.onchange('margen_costo')
    def _venta(self):
        if(self.standard_price==0.0):
            pass
        else:
            venta=0.0
            venta=(self.margen_costo/100)*self.standard_price
            venta+=self.standard_price
            self.list_price=venta
            self.precio_ivi=((self.taxes_id.amount/100)*self.list_price)+self.list_price
    @api.onchange('precio_ivi')
    def _precio_ivi(self):
        if(self.standard_price==0.0):
            pass
        else:
            venta=self.precio_ivi/(1+(self.taxes_id.amount/100))
            self.list_price=venta
            self.margen_costo=((venta-self.standard_price)/self.standard_price)*100

class RepRecibos(models.Model):
    _name = "recibos.report"
    _description = "Reporte Recibos"
    _auto = False
    _table = "recibos_diarios"
    fecha = fields.Date("Fecha",readonly=True)
    numero = fields.Char("Numero",readonly=True)
    cliente = fields.Char("Cliente",readonly=True)
    monto = fields.Float('Monto', readonly=True)
    formapago = fields.Char("Forma Pago",readonly=True)
    def _select(self):
        select_str="""
            select
	ROW_NUMBER() OVER (ORDER BY p.payment_date desc) id
	,p.payment_date fecha
	,p.name numero
    ,(select name from res_partner where res_partner.id=p.partner_id) cliente
	,p.amount monto
	,(
	select
		MAX(coalesce((
		select
			'Dias '||(pl.days)
		from
			account_payment_term p inner join account_payment_term_line pl
			on p.id=pl.payment_id
		where
			p.id=f.payment_term_id
		),'SN')) formapago
	from
	account_invoice_payment_rel fr
		inner join account_invoice f
			on f.id=fr.invoice_id
	where
		fr.payment_id=p.id
	)formapago
        """
        return select_str

    def _from(self):
        from_str="""
            account_payment p
        """
        return from_str

    def _where(self):
        where_str="""
            where
            p.partner_type='customer'
	        and p.state<>'draft'
	        and p.payment_date>='2017-12-18'
        """
        return where_str

    @api.model_cr
    def init(self):
        # self._table = sale_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM  %s
            %s
            )""" % (self._table, self._select(), self._from(), self._where()))

class RepVentas(models.Model):
    _name = "ventas.report"
    _description = "Reporte Ventas Diarias"
    _auto = False
    _table = "ventas_diarias"
    #_rec_name = 'date'
    #_order = 'date desc'
    tipo_documento=fields.Char("Tipo Documento",readonly=True)
    fecha = fields.Date("Fecha",readonly=True)
    formapago = fields.Char("Forma Pago",readonly=True)
    vendedor = fields.Char("Vendedor",readonly=True)
    subtotalexento = fields.Float('SubTotal Exento', readonly=True)
    montodescuentoexento = fields.Float('Descuento Exento', readonly=True)
    subtotalgravado = fields.Float('SubTotal Gravado', readonly=True)
    montodescuentogravado = fields.Float('Descuento Gravado', readonly=True)
    subtotal = fields.Float('Sub Venta', readonly=True)
    impuesto = fields.Float('Monto Impuesto', readonly=True)
    total = fields.Float('Total', readonly=True)

    def _select(self):
        select_str="""
            select
                ROW_NUMBER() OVER (ORDER BY fecha desc,formapago asc) id
                ,v.fecha
                ,v.tipo_documento
                ,v.vendedor
            	,coalesce(v.formapago,'SN') formapago
            	,sum(v.subtotalexento) subtotalexento
            	,sum(v.MontoDescuentoExento) montodescuentoexento
            	,sum(v.subtotalgravado) subtotalgravado
            	,sum(v.MontoDescuentoGravado) montodescuentogravado
                ,sum(v.subtotalexento)+sum(v.subtotalgravado) subtotal
            	,sum(v.montoimpuesto) impuesto
                ,sum(v.subtotalexento+v.subtotalgravado+v.montoimpuesto) total
        """
        return select_str
    def _from(self):
        from_str="""
            (
            	select
            		d.nombre
            		,d.numero
                    ,d.tipo_documento
            		,d.fecha
            		,d.reference
            		,CASE
            			WHEN imp=0 then subtotal
            			ELSE 0
            		END SubTotalExento
            		,CASE
            			WHEN imp=0 then montodescuento
            			ELSE 0
            		END MontoDescuentoExento
            		,CASE
            			WHEN imp>0 then subtotal
            			ELSE 0
            		END SubTotalGravado
            		,CASE
            			WHEN IMP>0 THEN montodescuento
            			else 0
            		END MontoDescuentoGravado
            		,d.imp
            		,CASE
            			WHEN IMP>0 THEN (d.imp/100)*subtotal
            			else 0
            		END montoimpuesto

            		,d.tipo
            		,d.formapago
                    ,d.vendedor
            	from(
            		select
            			c.name nombre
                        ,(select p.name from res_users u inner join res_partner p on u.partner_id=p.id where u.id=f.user_id) vendedor
            			,f.number numero
            			,f.reference
            			--,fl.price_unit
            			--,fl.quantity
            			,f.date_invoice fecha
            			,case
            				when f.type in ('out_refund','in_refund') then fl.price_subtotal*-1
            				else fl.price_subtotal
            			end subtotal
            			--,fl.discount
            			--,fl.price_subtotal_signed
            			,coalesce((select
            				sum(t.amount)
            			from
            				account_invoice_line_tax flt
            					inner join account_tax t
            				on flt.tax_id=t.id
            			where
            				fl.id=flt.invoice_line_id),0) imp
            			,CASE WHEN f.type in ('in_invoice','in_refund') THEN 'Compra'
            				ELSE 'Venta'
            			END tipo
            			,(
            			select
            				'Dias '||pl.days
            			from
            				account_payment_term p inner join account_payment_term_line pl
            				on p.id=pl.payment_id
            			where
            				p.id=f.payment_term_id
            			) formapago
            			,case
            				when f.type in ('out_refund','in_refund') then ((fl.price_unit*fl.quantity)*(fl.discount/100))*-1
            				else (fl.price_unit*fl.quantity)*(fl.discount/100)
            			end MontoDescuento
                        ,case
        					when f.type in ('out_refund','in_refund') then 'Nota'
        					else 'factura'
        				end tipo_documento
            		from
            			account_invoice_line fl inner join account_invoice f
            				on f.id=fl.invoice_id
            			inner join res_partner c
            				on f.commercial_partner_id=c.id
            		where
            			state not in ('draft')
                        and f.date_invoice >='2017-11-01'
            		) d
                    where
			                 tipo='Venta'
            	) v
        """
        return from_str
    def _group_by(self):
        group_by_str = """
            GROUP BY
                fecha
        	    ,formapago
                ,v.tipo_documento
                ,v.vendedor
            order by
            	fecha desc
            	,formapago

        """
        return group_by_str
    @api.model_cr
    def init(self):
        # self._table = sale_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM  %s
            %s
            )""" % (self._table, self._select(), self._from(), self._group_by()))

class RepUtilidad(models.Model):
    _name = "utilidad.report"
    _description = "Reporte de Utilidad"
    _auto = False
    _table = "utilidad"
    #_rec_name = 'date'
    #_order = 'date desc'
    numero = fields.Char("Numero",readonly=True)
    tipo_producto = fields.Char("Tipo Producto",readonly=True)
    codigo = fields.Char("Codigo",readonly=True)
    nombre = fields.Char("Nombre",readonly=True)
    fecha = fields.Date("Fecha",readonly=True)
    formapago = fields.Char("Forma Pago",readonly=True)
    tipo_documento = fields.Char("Tipo Documento",readonly=True)
    vendedor = fields.Char("Vendedor",readonly=True)
    cantidad_vendida = fields.Float('Cant Vendida', readonly=True)
    venta = fields.Float('Venta', readonly=True)
    costo = fields.Float('Costo', readonly=True)
    monto_utilidad = fields.Float('Monto Utilidad', readonly=True)
    por_utilidad = fields.Float('% Utilidad',group_operator="avg",readonly=True)
    por_util = fields.Float('Utilidad', readonly=True,compute="_por_utilidad")

    @api.one
    @api.depends('tipo_producto')
    def _por_utilidad(self):
        if self.costo>0.0:
            self.por_util=(self.monto_utilidad/self.costo)*100

    def _select(self):
        str_select = """
            select
            	ROW_NUMBER() OVER (ORDER BY utilidad.fecha desc) id
                ,Utilidad.numero
                ,Utilidad.tipo_producto
            	,Utilidad.default_code Codigo
            	,Utilidad.name nombre
            	,Utilidad.fecha
            	,Utilidad.formapago
                ,Utilidad.tipo_documento
                ,Utilidad.Vendedor
            	,Utilidad.cantidad cantidad_vendida
            	,Utilidad.subtotal venta
            	,Utilidad.costo costo
            	,Utilidad.subtotal-Utilidad.costo monto_utilidad
                ,ROUND((CASE
            		WHEN (Utilidad.subtotal-Utilidad.costo)=0 THEN 0
            		WHEN Utilidad.costo=0 then 1
            		ELSE (Utilidad.subtotal-Utilidad.costo)/Utilidad.costo
            	END)*100,2) por_utilidad
        """
        return str_select

    def _from(self):
        from_str="""
                (
        	select
        		--c.name nombre
        		f.number numero
        		,p.default_code
        		,p.name
        		,case
        			when p.type='consu' then 'Consumible'
        			when p.type='service' then 'Servicio'
        			when p.type='product' then 'Producto'
        			else p.type
        		end tipo_producto
                ,(select c.name from res_users u inner join res_partner c on u.partner_id=c.id where f.user_id=u.id) Vendedor
        		--,f.reference
        		--,fl.price_unit
        		--,fl.quantity
        		,f.date_invoice fecha
        		,fl.product_id
        			,CASE WHEN f.type in ('in_invoice','in_refund') THEN 'Compra'
        			ELSE 'Venta'
        		END tipo
        		,(
        		select
        			'Dias '||pl.days
        		from
        			account_payment_term p inner join account_payment_term_line pl
        			on p.id=pl.payment_id
        		where
        			p.id=f.payment_term_id
        		) formapago
        		,case
        			when f.type in ('out_refund','in_refund') then fl.quantity*-1
        			else fl.quantity
        		end cantidad
        		,case
        			when f.type in ('out_refund','in_refund') then fl.price_subtotal*-1
        			else fl.price_subtotal
        		end subtotal
        		/*
        		,fl.discount
        		,fl.price_subtotal_signed
        		,coalesce((select
        			sum(t.amount)
        		from
        			account_invoice_line_tax flt
        				inner join account_tax t
        			on flt.tax_id=t.id
        		where
        			fl.id=flt.invoice_line_id),0) imp


        		,case
        			when f.type in ('out_refund','in_refund') then ((fl.price_unit*fl.quantity)*(fl.discount/100))*-1
        			else (fl.price_unit*fl.quantity)*(fl.discount/100)
        		end MontoDescuento
        		*/
        		,case
        			when  f.type in ('out_refund','in_refund') then coalesce((
        				select
        					/*case
        						when f.type in ('out_refund','in_refund') then al.debit*-1
        						else al.debit
        					end Costo
        					*/
        					sum(al.debit)

        				from
        					account_move_line al inner join account_move a
        						on a.id=al.move_id
        					inner join account_account c
        						on c.id=al.account_id
        				where
        					c.name=' COSTO DE VENTAS GUAPILES'
        					--and
        					and a.id=f.move_id
        				and al.product_id=fl.product_id
        			),0)*-1
        		else (
        			coalesce((
        				select
        					/*case
        						when f.type in ('out_refund','in_refund') then al.debit*-1
        						else al.debit
        					end Costo
        					*/
        					sum(al.debit)

        				from
        					account_move_line al inner join account_move a
        						on a.id=al.move_id
        					inner join account_account c
        						on c.id=al.account_id
        				where
        					c.name=' COSTO DE VENTAS GUAPILES'
        					--and
        					and a.id=f.move_id
        				and al.product_id=fl.product_id
        			),0)
        		)
        		end costo
        		,f.move_id
                ,case
        			when f.type in ('out_refund','in_refund') then 'Nota'
        			else 'factura'
        		end tipo_documento

        	from
        		account_invoice_line fl inner join account_invoice f
        			on f.id=fl.invoice_id
        		inner join res_partner c
        			on f.commercial_partner_id=c.id
        		inner join product_template p
        			on p.id=fl.product_id
        	where
        		state not in ('draft')
        		and f.type not in ('in_invoice','in_refund')
        		and f.date_invoice >='2017-11-01'
        	--and f.number='FV-00144686'
        	)Utilidad
        """
        return from_str

    @api.model_cr
    def init(self):
        # self._table = sale_report
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
            %s
            FROM  %s
            )""" % (self._table, self._select(), self._from()))


# class rep_stihl(models.Model):
#     _name = 'rep_stihl.rep_stihl'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100
