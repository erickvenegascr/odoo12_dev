odoo.define('invoice_barcode.FieldBarcodeScannable', function(require) {
"use strict";
 
var AbstractField = require('web.AbstractField');
var basicFields = require('web.basic_fields');
var fieldRegistry = require('web.field_registry');
var rpc = require('web.rpc');


var FieldBarcodeScannable = basicFields.FieldChar.extend({
    events: {
        'keypress': 'simulateKeypress',
    },

    simulateKeypress: function (e) {
        if (e.keyCode ==13)
        {   
            var self = this;
            console.log('this--->>>>',this);
            rpc.query({
                model: this.model,
                method: 'onchange_clean_ean',
                args: [[this.res_id], [self.$el[0].value]],
            })
            .then(function (nomenclatures){
                //this.$el[0].trigger('input');
                //this.$el[0].trigger("change");
                //this.trigger("change");
                e.preventDefault();
                console.log("aaa--22----",this);
                //self.$el[0].value = '';
            });
        }
    },
});

fieldRegistry.add('field_barcode_scannable', FieldBarcodeScannable);

return {
    FieldBarcodeScannable: FieldBarcodeScannable,
};

});



$(document).ready(function () {
    console.log("iniiiiii",$('.get_product_ean'));
    /*if ($(".get_product_ean").length) {
        var tmp;
        console.log("iniiiiii");
        //tmp = setTimeout(function(){ 
        //    document.getElementById("auto_click").click(); 
        //}, 5000);
    }*/
    /*$('.get_product_ean').keypress(function (e) {
        console.log($(this).attr('id'));
       if(e.which ==13)
        console.log($(this).attr('id'));
    });*/
    $('.get_product_ean').on('change',function(){
        console.log("2222222222");
       // do your logic....
    });

});