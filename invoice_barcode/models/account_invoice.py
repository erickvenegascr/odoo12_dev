# -*- encoding: utf-8 -*-

from odoo import api, fields, models, SUPERUSER_ID, _


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    get_product_ean = fields.Char(string='Add Product', size=13, help="Read a product barcode to add it as new Line.")

    @api.onchange('get_product_ean')
    def onchange_product_ean(self):
        if not self.get_product_ean:
            return

        ProductObj = self.env['product.product']
        InvLine = self.env['account.invoice.line']
        product = ProductObj.search([('barcode','=',self.get_product_ean)], limit=1)
        if not product:
            product = ProductObj.search([('default_code','=',self.get_product_ean)], limit=1)
        if not product:
            warning = {'title': _('Warning!'),
                'message': _("There is no product with Barcode or Reference: %s") % (self.get_product_ean),
            }
            self.get_product_ean = False
            return {'warning': warning}

        flag = 1
        order_line = []
        for o_line in self.invoice_line_ids:
            if o_line.product_id == product:
                flag = 0
                order_line.append((0,0,{
                    'product_id': o_line.product_id.id,
                    'name': o_line.product_id.name,
                    'invoice_line_tax_ids': o_line.invoice_line_tax_ids,
                    'quantity' : (o_line.quantity + 1),
                    'price_unit': o_line.price_unit,
                    'uom_id': o_line.uom_id and o_line.uom_id.id,
                    'state' : 'draft',
                    'account_id': o_line.account_id.id,
                }))
            else:
                order_line.append((0,0,{
                    'product_id': o_line.product_id.id,
                    'name': o_line.product_id.name,
                    'invoice_line_tax_ids': o_line.invoice_line_tax_ids,
                    'quantity' : o_line.quantity,
                    'price_unit': o_line.price_unit,
                    'uom_id': o_line.uom_id and o_line.uom_id.id,
                    'account_id': o_line.account_id.id,
                    'state' : 'draft',
                }))

        if flag:
            account_id = InvLine.get_invoice_line_account(self.type, product, self.fiscal_position_id, self.company_id)
            order_line.append((0,0, {
                'product_id': product.id,
                'name':product.name,
                'invoice_line_tax_ids': [(4, product.taxes_id.id, 0)],
                'quantity' : 1,
                'price_unit': product.lst_price,
                'uom_id': product.uom_id.id,
                'account_id': account_id.id,
                'state' : 'draft',
            }))
        self.get_product_ean = ''
        self.write({'get_product_ean':''})

        print("self.get_product_ean",self.get_product_ean)
        self.invoice_line_ids = order_line

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: