odoo.define('pos_sync_pos_restaurant_orders.sync_pos_restaurant_orders', function(require){
    
    var core = require('web.core');
    var Gui = require('point_of_sale.gui');
    var Screens = require('point_of_sale.screens');
    var Models = require('point_of_sale.models');
    var SyncSession = require('pos_sync_pos_restaurant_orders.sync_pos_orders');
    var _t = core._t;

    var _super_posmodel = Models.PosModel.prototype;
    Models.PosModel = Models.PosModel.extend({
        initialize: function (session, attributes) {
            var floor_model = _.find(this.models, function(model){ return model.model === 'restaurant.floor'; });
            floor_model.domain = function(self){ return [['id','in',self.config.floor_ids]]; };
            return _super_posmodel.initialize.call(this, session, attributes);
        }
    });

    _.each(Gui.Gui.prototype.screen_classes, function(screen){
        if (screen.name == 'floors'){
            var FloorScreenWidget = screen.widget;
            FloorScreenWidget.include({
                start: function () {
                    var self = this;
                    this._super();
                    this.pos.bind('change:orders-count-on-floor-screen', function () {
                        self.renderElement();
                    });
                }
            });
            return false;
        }
    });

    Screens.OrderWidget.include({
        remove_orderline: function(order_line){
            if (this.pos.get_order() && this.pos.get_order().get_orderlines().length === 0)
                this._super(order_line);
            else {
                if(this.pos.get_order())
                    order_line.node.parentNode.removeChild(order_line.node);
            }
        },
        update_summary: function(){
            if (!this.pos.get('selectedOrder'))
                return;
            this._super();
        },
    });

    var PosModelSuper = Models.PosModel;
    Models.PosModel = Models.PosModel.extend({
        initialize: function(){
            var self = this;
            PosModelSuper.prototype.initialize.apply(this, arguments);
            this.sync_session.remove_order = function(data) {
                if (data.transfer) {
                    data.transfer = false;
                    return;
                } else {
                    this.send({
                        action: 'remove_order',
                        data: data
                    });
                }
            };
        },
        sync_create_order: function(options){
            var self = this;
            var order = PosModelSuper.prototype.sync_create_order.apply(this, arguments);
            if (options.data.table_id) {
                order.customer_count = options.data.customer_count;
                order.table = self.tables_by_id[options.data.table_id];
                order.save_to_db();
            }
            return order;
        },
        sync_on_add_order: function(current_order){
            if (current_order)
                PosModelSuper.prototype.sync_on_add_order.apply(this, arguments);
            else
                this.trigger('change:orders-count-on-floor-screen');
        },
        sync_on_update: function(message, sync_all){
            var self = this;
            var order = false;
            var data = message.data || {};
            var old_order = this.get_order();

            if (data.uid){
                order = this.get('orders').find(function(order){
                    return order.uid == data.uid;
                });
            }
            if (order && order.table.id != data.table_id) {
                order.transfer = true;
                order.destroy({'reason': 'abandon'});
            }
            PosModelSuper.prototype.sync_on_update.apply(this, arguments);
            if ((order && old_order && old_order.uid != order.uid) || (old_order == null))
                this.set('selectedOrder',old_order);
        },
        add_new_order: function(){
            var self = this;
            PosModelSuper.prototype.add_new_order.apply(this, arguments);
            if (this.sync_session){
                var current_order = this.get_order();
                current_order.sync_update();
            }
        },
        set_table: function(table) {
            var self = this;
            if (table && this.order_to_transfer_to_different_table) {
                this.order_to_transfer_to_different_table.table = table;
                this.order_to_transfer_to_different_table.sync_update();
                this.order_to_transfer_to_different_table = null;
                this.set_table(table);
            } else {
                PosModelSuper.prototype.set_table.apply(this, arguments);
            }
        },
        on_removed_order: function(removed_order, index, reason){
            PosModelSuper.prototype.on_removed_order.apply(this, arguments);
            this.trigger('change:orders-count-on-floor-screen');
        },
        sync_do_update: function(order, data){
            PosModelSuper.prototype.sync_do_update.apply(this, arguments);
            if (order) {
                order.set_customer_count(data.customer_count, true);
                order.saved_resume = data.multiprint_resume;
                order.trigger('change');
                this.gui.screen_instances.floors.renderElement();
            }
        },
    });

    var OrderSuper = Models.Order;
    Models.Order = Models.Order.extend({
        set_customer_count: function (count, skip_sync_update) {
            OrderSuper.prototype.set_customer_count.apply(this, arguments);
            if (!skip_sync_update)
                this.sync_update();
        },
        do_sync_remove_order: function(){
            if (this.transfer)
                this.pos.sync_session.remove_order({
                    'uid': this.uid,
                    'revision_ID': this.revision_ID,
                    'transfer': this.transfer
                });
            else
                OrderSuper.prototype.do_sync_remove_order.apply(this, arguments);
        }
    });

    var OrderlineSuper = Models.Orderline;
    Models.Orderline = Models.Orderline.extend({
        get_line_diff_hash: function(){
            if (this.get_note()) {
                return this.uid + '|' + this.get_note();
            } else {
                return '' + this.uid;
            }
        },
    });

    Gui.Gui.prototype.screen_classes.filter(function(el) {
        return el.name == 'splitbill'
    })[0].widget.include({
        pay: function(order,neworder,splitlines){
            this._super(order,neworder,splitlines);
            neworder.save_to_db();
        }
    });
});