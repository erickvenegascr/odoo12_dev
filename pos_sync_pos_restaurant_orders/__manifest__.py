# -*- coding: utf-8 -*-
{
    "name": """POS Sync restaurant orders across multiple sessions""",
    "summary": """Use multiple POS for handling restaurant orders""",
    "category": "Point of Sale",

    'version': '10.0.0.1.0',
    'author': 'iPredict IT Solutions Pvt. Ltd.',
    'website': 'http://ipredictitsolutions.com',
    "support": "ipredictitsolutions@gmail.com",

    "depends": [
        "bus",
        "pos_restaurant",
    ],
    "data": [
        "security/ir.model.access.csv",
        "views/templates.xml",
        "views/pos_config_views.xml",
    ],
    "qweb": [
        "static/src/xml/pos_sync_connection.xml",
        "static/src/xml/sync_pos_restaurant_order.xml",
    ],
    "demo": ["demo/demo.xml"],
    'license': "OPL-1",
    'images': ['static/description/banner.png'],
    'live_test_url': 'https://youtu.be/jmD5gJX2tfE',
    'price': 149,
    'currency': "EUR",
    "auto_install": False,
    "installable": True,
}
