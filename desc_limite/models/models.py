# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError, RedirectWarning, ValidationError
import logging

_logger = logging.getLogger(__name__)

# class desc_limite(models.Model):
#     _name = 'desc_limite.desc_limite'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class descuento_limite(models.Model):
	_inherit = 'sale.order.line'


	@api.onchange('discount')
	@api.constrains('discount')
	def verif_descuento(self):
		for line in self:
			descuento=line.discount
			if descuento>12.0:
				#self.discount=
				#raise UserError(_('Descunto no autorizado'))
				line.discount=0.0
				raise  ValidationError('El articulo: '+line.display_name+'\n tiene un descuento no permitido')

				#self.discount=0.0
				#raise exceptions.except_orm(('Descuento'),('Descuento NO autorizado'))