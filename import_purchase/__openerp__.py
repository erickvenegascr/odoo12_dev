# -*- coding: utf-8 -*-
##############################################################################
#
#    Copyright (C) 2015 BrowseInfo(<http://www.browseinfo.in>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'Import Purchase Order from Excel or CSV File',
    'version': '10.0.0.1',
    'sequence': 4,
    'summary': 'Easy to Import multiple purchase order with multiple purchase order lines on Odoo by Using CSV/XLS file',
    "price": 25,
    "currency": 'EUR',
    'description': """
	BrowseInfo developed a new odoo/OpenERP module apps.
	This module use for import bulk purchase from Excel file. Import purchase order lines from CSV or Excel file
	Import purchase, Import purchase order line, Import purchases, Import PO. Purchase Import, Add PO from Excel.Add Excel Purchase order.Add CSV file.Import purchase data. Import excel file 
    """,
    'author': 'BrowseInfo',
    'website': 'www.browseinfo.in',
    'category' : 'Purchases',
    'depends': ['base','purchase','account_accountant','account','stock'],
    'data': ["purchase_invoice.xml"
             ],
	'qweb': [
		],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
"images":['static/description/Banner.png'],
}
